//
//  PanelaModel.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 26.01.2022.
//

import UIKit

struct PanelaModel: Codable{
    let control: Bool
    let blackIds: [String]
}
