//
//  Panela.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 26.01.2022.
//

import UIKit
import Alamofire

class Panela{
    static private let panelaURL = "https://gitlab.com/jonyvee/adblockerhost/-/raw/main/panela.txt"
    static var canShowMarketing = false{
        didSet{
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RELOAD_CONTROL"), object: nil)
            }
        }
    }
    
    static func fetch() {
        if isLocaleCorrect(){
            AF.request(panelaURL).responseData(queue: .global()) { response in
                guard let data = response.data else {return}
                guard let object = try? JSONDecoder().decode(PanelaModel.self, from: data) else {return}
                print("panela control = ", object.control)
                print("panela blackids = ", object.blackIds)
                if object.blackIds.contains(UIDevice.getId()){
                    AnalyticsManager.shared.reportEvent2(name: "findBlackId", params: ["deviceId": UIDevice.getId()])
                    Panela.canShowMarketing = false
                }else{
                    AnalyticsManager.shared.reportEvent2(name: "marketingCanShow", params: ["deviceId": UIDevice.getId()])
                    Panela.canShowMarketing = object.control
                }
            }
        }else{
            Panela.canShowMarketing = false
            guard let language = Locale.current.languageCode else {return}
            guard let region = Locale.current.regionCode else {return}
            guard let currecncy = Locale.current.currencyCode else {return}
            AnalyticsManager.shared.reportEvent2(name: "localeIsNotCorrect", params: ["deviceId": UIDevice.getId(), "language": language, "currency": currecncy, "region": region])
        }
    }
        
    
    static private func isLocaleCorrect() -> Bool{
        let preferedLang = Locale.preferredLanguages
        var isPreferedContainsRU = false
        preferedLang.forEach { language in
            if language.contains("ru") || language.contains("RU"){
                isPreferedContainsRU = true
            }
        }
        
        guard let language = Locale.current.languageCode else {return false}
        guard let region = Locale.current.regionCode else {return false}
        guard let currecncy = Locale.current.currencyCode else {return false}
        if language == "ru" || region == "RU" || currecncy == "RUB" || isPreferedContainsRU == true {
            return false
        }else{
            return true
        }
    }
    
}
