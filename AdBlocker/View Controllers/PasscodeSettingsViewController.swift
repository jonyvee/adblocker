//
//  PasscodeSettingsViewController.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 08.04.2022.
//

import UIKit
import SnapKit

class PasscodeSettingsViewController: UIViewController, JonyMessage{
    
    // MARK: - Properties
    
    private var backView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        view.layer.cornerCurve = .continuous
        view.layer.cornerRadius = 24
        return view
    }()
    
    private var topLineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(rgb: 0xF6F6F6)
        view.layer.cornerRadius = 2.5
        return view
    }()
    
    private var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Montserrat-Bold", size: 24)
        label.textAlignment = .center
        label.textColor = UIColor(rgb: 0x23262F)
        label.text = "Passcode settings"
        return label
    }()
    
    private var usePasscodeLabel: UILabel = {
        let label = UILabel()
        label.text = "Use passcode"
        label.textColor = UIColor(red: 0.102, green: 0.125, blue: 0.173, alpha: 1)
        label.font = UIFont(name: "Montserrat-Medium", size: 18)
        return label
    }()
    
    private var useFaceLabel: UILabel = {
        let label = UILabel()
        label.text = "Face Id"
        label.textColor = UIColor(red: 0.102, green: 0.125, blue: 0.173, alpha: 1)
        label.font = UIFont(name: "Montserrat-Medium", size: 18)
        return label
    }()
    
    private var changeLabel: UILabel = {
        let label = UILabel()
        label.text = "Change passcode"
        label.textColor = UIColor(red: 0.102, green: 0.125, blue: 0.173, alpha: 1)
        label.font = UIFont(name: "Montserrat-Medium", size: 18)
        return label
    }()
    
    private lazy var changePasscodeBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(changeAction), for: .touchUpInside)
        return button
    }()
    
    private let imageViewYYYY: UIImageView = UIImageView(image: "yyyy".img())
    
    private lazy var passcodeSwitcher: UISwitch = {
        let switcher = UISwitch()
        switcher.tag = 0
        switcher.tintColor = UIColor(rgb: 0xE3FDF7)
        switcher.onTintColor = UIColor(rgb: 0x00A469)
        switcher.addTarget(self, action: #selector(switcherAction(_:)), for: .valueChanged)
        switcher.isOn = true
        return switcher
    }()
    
    private lazy var faceSwitcher: UISwitch = {
        let switcher = UISwitch()
        switcher.tag = 0
        switcher.tintColor = UIColor(rgb: 0xE3FDF7)
        switcher.onTintColor = UIColor(rgb: 0x00A469)
        switcher.addTarget(self, action: #selector(switcherAction2(_:)), for: .valueChanged)
        switcher.isOn = true
        return switcher
    }()
    
    
    
    private let bottomSafeAreaView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    // MARK: -
    
    var didClose: (() -> ())?
    var didOpenChange: (() -> ())?
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        setViews()
        setConstraints()
        
        passcodeSwitcher.isOn = User.shared.hasPasscode!
        faceSwitcher.isOn = User.shared.hasFaceid!
    }
    
    // MARK: - Actions
    
    @objc private func changeAction(){
        Vibration.soft.vibrate()
        didOpenChange?()
    }
    
    @objc private func closeAction(){
        Vibration.soft.vibrate()
        didClose?()
    }
    
    @objc private func switcherAction(_ sender: UISwitch){
        Vibration.soft.vibrate()
        
        if User.shared.passcode == ""{
            self.message(text: "First create passcode by tapping button Change passcode below")
            passcodeSwitcher.isOn = false
        }else{
            User.shared.hasPasscode = sender.isOn
        }
        
    }
    
    @objc private func switcherAction2(_ sender: UISwitch){
        Vibration.soft.vibrate()
        User.shared.hasFaceid = sender.isOn
    }
    
    // MARK: - UI
    
    private func setViews(){
        self.view.addSubview(backView)
        self.view.addSubview(bottomSafeAreaView)
        backView.addSubview(topLineView)
        backView.addSubview(titleLabel)
        backView.addSubview(usePasscodeLabel)
        backView.addSubview(passcodeSwitcher)
        backView.addSubview(useFaceLabel)
        backView.addSubview(faceSwitcher)
        backView.addSubview(changeLabel)
        backView.addSubview(imageViewYYYY)
        backView.addSubview(changePasscodeBtn)
    }
    
    private func setConstraints(){
        backView.snp.makeConstraints { make in
            make.height.equalTo(260)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide)
        }
        
        topLineView.snp.makeConstraints { make in
            make.height.equalTo(5)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(8)
            make.width.equalTo(47)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.height.equalTo(30)
            make.top.equalToSuperview().inset(28)
        }
        
        usePasscodeLabel.snp.makeConstraints { make in
            make.height.equalTo(25)
            make.leading.equalToSuperview().inset(16)
            make.top.equalTo(titleLabel.snp.bottom).inset(-30)
        }
        
        passcodeSwitcher.snp.makeConstraints { make in
            make.centerY.equalTo(usePasscodeLabel)
            make.trailing.equalToSuperview().inset(32)
        }
        
        useFaceLabel.snp.makeConstraints { make in
            make.height.equalTo(25)
            make.leading.equalToSuperview().inset(16)
            make.top.equalTo(usePasscodeLabel.snp.bottom).inset(-19)
        }
        
        faceSwitcher.snp.makeConstraints { make in
            make.centerY.equalTo(useFaceLabel)
            make.trailing.equalToSuperview().inset(32)
        }
        
        changeLabel.snp.makeConstraints { make in
            make.height.equalTo(25)
            make.leading.equalToSuperview().inset(16)
            make.top.equalTo(useFaceLabel.snp.bottom).inset(-26)
        }
        
        imageViewYYYY.snp.makeConstraints { make in
            make.height.width.equalTo(30)
            make.trailing.equalToSuperview().inset(32)
            make.centerY.equalTo(changeLabel)
        }
        
        changePasscodeBtn.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(35)
            make.centerY.equalTo(imageViewYYYY)
        }
        
        bottomSafeAreaView.snp.makeConstraints { make in
            make.leading.bottom.trailing.equalToSuperview()
            make.top.equalTo(backView.snp.bottom)
        }
    }


    // MARK: - Touch
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first{
            let location = touch.location(in: self.view)
            if self.backView.frame.contains(location) == false{
                closeAction()
            }
        }
    }
    
}
