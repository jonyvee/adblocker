//
//  EnterViewController.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 20.02.2022.
//

import UIKit
import SnapKit

class EnterViewController: UIViewController, JonyMessage, LocalAuthenticationManager{
    
    // MARK: - Properties
    
    private let codeStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .fill
        stackView.axis = .horizontal
        stackView.spacing = 32
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private let code1View: UIView = {
        return UIView().setCodeView()
    }()
    
    private let code2View: UIView = {
        return UIView().setCodeView()
    }()
    
    private let code3View: UIView = {
        return UIView().setCodeView()
    }()
    
    private let code4View: UIView = {
        return UIView().setCodeView()
    }()
    
    private let codeCenteringView = UIView()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Create passcode"
        label.textColor = UIColor(rgb: 0x2A3038)
        label.textAlignment = .center
        label.font = UIFont(name: "Montserrat-Bold", size: 28)
        return label
    }()
    
    private let subtitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Please, create passcode"
        label.textColor = UIColor(rgb: 0x848C9A)
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        label.numberOfLines = 1
        label.font = UIFont(name: "Montserrat-Regular", size: 16)
        return label
    }()
    
    private let buttonsVStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.spacing = 24
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private lazy var faceIdImageView: UIImageView = {
        let imageView = UIImageView(image: "faceid".img())
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(faceIdAction))
        imageView.addGestureRecognizer(tapGesture)
        return imageView
    }()
    
    private lazy var closeBtn: UIButton = {
        let button = UIButton()
        button.setImage("bakc".img(), for: .normal)
        button.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
//        button.isHidden = !viewModel.isCloseBtnShowed
        return button
    }()
        
    // MARK: -
    
    var isCreatePasscode = false
    var isBackBtnHidden = true
    var didClose: (() -> ())?
    var didBack: (() -> ())?
    
    // MARK: -
    
    private var passcode: String = ""{
        didSet{
            changeCodeView(codes: self.passcode.count)
        }
    }
    
    private var registrationPasscode = ""
        
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupViews()
        setConstraints()
        setButtonsHStackView()
        
        closeBtn.isHidden = isBackBtnHidden
        
        if isCreatePasscode{
            titleLabel.text = "Create passcode"
            subtitleLabel.text = "Please, create passcode"
            faceIdImageView.image = "".img()
        }else{
            titleLabel.text = "Enter passcode"
            subtitleLabel.text = "Enter passcode to login"
            faceIdImageView.image = User.shared.hasFaceid! ? "faceid".img() : "".img()
        }
    }
    
    // MARK: - Actions
    
    @objc private func buttonAction(_ sender: UIButton){
        Vibration.soft.vibrate()
        
        if passcode.count < 4{
            passcode.append(sender.tag.description)
        }
        
        if passcode.count == 4{
            
            if isCreatePasscode{
                
                if registrationPasscode == ""{
                    registrationPasscode = passcode
                    passcode = ""
                    titleLabel.text = "Repeate passcode"
                    subtitleLabel.text = "Please, repeat passcode"
                }else{
                    
                    if passcode == registrationPasscode{
                        Vibration.success.vibrate()
                        User.shared.passcode = passcode
                        User.shared.isFinishedOnboard = true
                        passcode = ""
                        registrationPasscode = ""
                        didClose?()
                    }else{
                        titleLabel.text = "Create passcode"
                        subtitleLabel.text = "Please, create passcode"
                        Vibration.error.vibrate()
                        self.message(text: "Incorrect repeat, please try again")
                        self.passcode = ""
                        self.registrationPasscode = ""
                    }
                }
                
            }else{
                if passcode == User.shared.passcode{
                    Vibration.success.vibrate()
                    didClose?()
                }else{
                    self.message(text: "incorrect password")
                    Vibration.error.vibrate()
                    self.passcode = ""
                }
            }
        }
    }
    
    @objc private func faceIdAction(){
        if User.shared.hasFaceid == false || isCreatePasscode == true {return}
        Vibration.soft.vibrate()
        loginWithFaceId { success in
            if success{
                DispatchQueue.main.async {
                    self.didClose?()
                }
            }
        }
    }
    
    @objc private func removeAction(){
        Vibration.soft.vibrate()
        if passcode.isEmpty == false{
            passcode.removeLast()
        }
    }
    
    @objc private func closeAction(){
        Vibration.soft.vibrate()
        self.passcode = ""
        self.registrationPasscode = ""
        didBack?()
    }
    
    // MARK: - UI
    
    private func setupViews(){
        view.addSubview(buttonsVStackView)
        view.addSubview(titleLabel)
        view.addSubview(subtitleLabel)
        view.addSubview(codeCenteringView)
        view.addSubview(closeBtn)
        codeCenteringView.addSubview(codeStackView)
        
        codeStackView.addArrangedSubview(code1View)
        codeStackView.addArrangedSubview(code2View)
        codeStackView.addArrangedSubview(code3View)
        codeStackView.addArrangedSubview(code4View)
    }
    
    private func setConstraints(){
        
        closeBtn.snp.makeConstraints { make in
            make.width.height.equalTo(48)
            make.leading.equalToSuperview().inset(16)
            make.top.equalTo(view.safeAreaLayoutGuide).inset(12)
        }
        
        buttonsVStackView.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeAreaLayoutGuide).inset(40)
            make.height.equalTo(376)
            make.width.equalTo(286)
            make.centerX.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { make in
            make.height.equalTo(36)
            make.leading.trailing.equalToSuperview().inset(24)
            make.top.equalTo(closeBtn.snp.bottom).inset(-9)
        }
        
        subtitleLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(24)
            make.top.equalTo(titleLabel.snp.bottom).inset(-16)
        }
        
        codeCenteringView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(subtitleLabel.snp.bottom)
            make.bottom.equalTo(buttonsVStackView.snp.top)
        }
        
        codeStackView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
            make.width.equalTo(160)
            make.height.equalTo(16)
        }
    }
    
    private func setButtonsHStackView(){
        // 1
        let hStackView1 = UIStackView()
        hStackView1.spacing = 29
        hStackView1.setHStackView(to: buttonsVStackView)
        setButtons(to: hStackView1, numbers: [1,2,3])
        // 2
        let hStackView2 = UIStackView()
        hStackView2.spacing = 29
        hStackView2.setHStackView(to: buttonsVStackView)
        setButtons(to: hStackView2, numbers: [4,5,6])
        // 3
        let hStackView3 = UIStackView()
        hStackView3.spacing = 29
        hStackView3.setHStackView(to: buttonsVStackView)
        setButtons(to: hStackView3, numbers: [7,8,9])
        // 4
        let hStackView4 = UIStackView()
        hStackView4.spacing = 29
        hStackView4.setHStackView(to: buttonsVStackView)
        hStackView4.addArrangedSubview(faceIdImageView)
        setButton(to: hStackView4, number: 0)
        setDeleteButton(to: hStackView4)
    }
    
    // MARK: -
    
    private func changeCodeView(codes: Int){
        code1View.setPassiveState()
        code2View.setPassiveState()
        code3View.setPassiveState()
        code4View.setPassiveState()
        
        switch codes{
        case 0:
            break;
        case 1:
            code1View.setActiveState()
        case 2:
            code1View.setActiveState()
            code2View.setActiveState()
        case 3:
            code1View.setActiveState()
            code2View.setActiveState()
            code3View.setActiveState()
        default:
            code1View.setActiveState()
            code2View.setActiveState()
            code3View.setActiveState()
            code4View.setActiveState()
        }
    }
    
    // MARK: -
    
//    private func changeEntering(isFirstEntering: Bool, enterType: EnterViewModel.EnterType){
//        if enterType == .registration || enterType == .registrationWithClose{
//            if isFirstEntering{
//                self.titleLabel.text = "Create passcode"
//                self.subtitleLabel.text = "Please, create strong passcode\nand remember him"
//            }else{
//                self.titleLabel.text = "Repeat passcode"
//                self.subtitleLabel.text = "Please, repeat strong passcode\nand remember him"
//            }
//        }else if enterType == .login{
//            self.titleLabel.text = "Enter passcode"
//            self.subtitleLabel.text = "Please, enter passcode to login"
//        }else if enterType == .fakePasscode{
//            if isFirstEntering{
//                self.titleLabel.text = "Create fake passcode"
//                self.subtitleLabel.text = "Please, create fake passcode\nand remember him"
//            }else{
//                self.titleLabel.text = "Repeat fake passcode"
//                self.subtitleLabel.text = "Please, repeat fake passcode\nand remember him"
//            }
//        }
//    }
    
    // MARK: - Helper
    
    private func setButtons(to stackView: UIStackView, numbers: [Int]){
        numbers.forEach { number in
            setButton(to: stackView, number: number)
        }
    }
    
    private func setButton(to stackView: UIStackView, number: Int){
        let button = UIButton()
        button.tag = number
        button.backgroundColor = UIColor(rgb: 0xF7F8FA)
        button.layer.cornerRadius = 38
        button.setTitle(number.description, for: .normal)
        button.setTitleColor(UIColor(rgb: 0x243B54), for: .normal)
        button.titleLabel?.font = UIFont(name: "SFProDisplay-Bold", size: 20)
        button.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        stackView.addArrangedSubview(button)
    }
    
    private func setDeleteButton(to stackView: UIStackView){
        let button = UIButton()
        button.setTitle("Delete", for: .normal)
        button.setTitleColor(UIColor(red: 0.141, green: 0.231, blue: 0.329, alpha: 1), for: .normal)
        button.titleLabel?.font = UIFont(name: "SFProDisplay-Regular", size: 20)
        button.addTarget(self, action: #selector(removeAction), for: .touchUpInside)
        stackView.addArrangedSubview(button)
    }
}

// MARK: - Helper

fileprivate extension UIStackView{
    func setHStackView(to stackView: UIStackView){
        stackView.addArrangedSubview(self)
        self.alignment = .fill
        self.axis = .horizontal
        self.distribution = .fillEqually
    }
}

fileprivate extension UILabel{
    func setCodeLabel() -> UILabel{
        self.textColor = UIColor(rgb: 0x000000)
        self.textAlignment = .center
        return self
    }
}

fileprivate extension UIView{
    func setCodeView() -> UIView{
        self.layer.cornerRadius = 8
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor(red: 0, green: 0.642, blue: 0.412, alpha: 1).cgColor
        return self
    }
    

    func setActiveState(){
        self.layer.cornerRadius = 8
        self.layer.borderWidth = 0
        self.backgroundColor = UIColor(red: 0, green: 0.642, blue: 0.412, alpha: 1)
    }
    
    func setPassiveState(){
        self.layer.cornerRadius = 8
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor(red: 0, green: 0.642, blue: 0.412, alpha: 1).cgColor
        self.backgroundColor = .clear
    }
}
