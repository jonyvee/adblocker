//
//  PasswordViewController.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 01.04.2022.
//

import SnapKit
import UIKit
import RealmSwift

class PasswordViewController: UIViewController, JonyMessage{
    
    // MARK: - Properties
    
    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.contentSize = CGSize(width: UIDevice.width, height: 660)
        return scrollView
    }()
    
    private lazy var backBtn: UIButton = {
        let button = UIButton()
        button.setImage("bakc".img(), for: .normal)
        button.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var saveBtn: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(rgb: 0x718096)
        button.setTitleColor(.white, for: .normal)
        button.setTitle("Save", for: .normal)
        button.backgroundColor = UIColor(rgb: 0x1ED694)
        button.layer.cornerRadius = 12
        button.addTarget(self, action: #selector(saveAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var heartBtn: UIButton = {
        let button = UIButton()
        button.setImage("favorite".img(), for: .normal)
        button.addTarget(self, action: #selector(heartAction), for: .touchUpInside)
        return button
    }()
    
    private let titleTextLabel: UILabel = {
        let label = UILabel()
        label.text = "Name"
        label.textColor = UIColor(rgb: 0x718096)
        label.font = UIFont(name: "Montserrat-Bold", size: 12)
        return label
    }()
    
    private lazy var titleTextField: UITextField = {
        let textField = UITextField()
        textField.addTarget(self, action: #selector(textFieldAction), for: .editingChanged)
        textField.layer.cornerRadius = 16
        textField.backgroundColor = UIColor(rgb: 0x1ED694).withAlphaComponent(0.2)
        let leftView = UIView()
        leftView.frame = CGRect(x: 0, y: 0, width: 16.78, height: 56)
        textField.leftView = leftView
        textField.leftViewMode = .always
        textField.placeholder = "Google"
        textField.autocorrectionType = .no
        textField.delegate = self
        return textField
    }()
    
    private let loginTextLabel: UILabel = {
        let label = UILabel()
        label.text = "Login"
        label.textColor = UIColor(rgb: 0x718096)
        label.font = UIFont(name: "Montserrat-Bold", size: 12)
        return label
    }()
    
    private lazy var loginTextField: UITextField = {
        let textField = UITextField()
        textField.addTarget(self, action: #selector(textFieldAction), for: .editingChanged)
        textField.layer.cornerRadius = 16
        textField.backgroundColor = UIColor(rgb: 0x1ED694).withAlphaComponent(0.2)
        let leftView = UIView()
        leftView.frame = CGRect(x: 0, y: 0, width: 16.78, height: 56)
        textField.leftView = leftView
        textField.leftViewMode = .always
        textField.placeholder = "example@gmail.com"
        textField.autocorrectionType = .no
        textField.delegate = self
        return textField
    }()
    
    private let passwordTextLabel: UILabel = {
        let label = UILabel()
        label.text = "Password"
        label.textColor = UIColor(rgb: 0x718096)
        label.font = UIFont(name: "Montserrat-Bold", size: 12)
        return label
    }()
    
    private lazy var passwordTextField: UITextField = {
        let textField = UITextField()
        textField.addTarget(self, action: #selector(textFieldAction), for: .editingChanged)
        textField.layer.cornerRadius = 16
        textField.backgroundColor = UIColor(rgb: 0x1ED694).withAlphaComponent(0.2)
        textField.delegate = self
        textField.placeholder = "password"
        textField.autocorrectionType = .no
        let leftView = UIView()
        leftView.frame = CGRect(x: 0, y: 0, width: 16.78, height: 56)
        textField.leftView = leftView
        textField.leftViewMode = .always
        return textField
    }()
    
    private lazy var deleteBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(deleteAction), for: .touchUpInside)
        button.setTitle("Delete", for: .normal)
        button.setTitleColor(UIColor(red: 0.165, green: 0.188, blue: 0.22, alpha: 1), for: .normal)
        button.titleLabel?.font = UIFont(name: "Montserrat-Regular", size: 15)
        return button
    }()
    
    // MARK: -
    
    var didBack: (() -> ())?
    
    // MARK: -
    
    private var password: PasswordModel?
    
    // MARK: - Initialization
    
    init(data: PasswordModel?){
        self.password = data
        
        if password == nil{
            password = PasswordModel()
        }
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupViews()
        setConstraints()
        setData()
    }
    
    // MARK: - Actions
    
    @objc private func backAction(){
        didBack?()
    }
    
    @objc private func saveAction(){
        guard let password = password else {
            return
        }
        
        if self.titleTextField.text == "" || self.passwordTextField.text == ""{
            self.message(text: "Name and Password fields can't be empty")
        }else{
            try! realm.write {
                realm.add(password, update: .modified)
            }
            didBack?()
        }
    }
    
    @objc private func deleteAction(){
        guard let password = password else {
            return
        }
        
        let controller = UIAlertController(title: "Delete", message: "Do you want to delete password?", preferredStyle: .alert)
        let action = UIAlertAction(title: "Delete", style: .default) { _ in

            try? realm.write{
                realm.delete(realm.objects(PasswordModel.self).filter({$0.id == password.id}))
            }
            
            self.didBack?()
        }
        let action2 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        controller.addAction(action)
        controller.addAction(action2)
        UIApplication.shared.topVC?.present(controller, animated: true, completion: nil)
    }
    
    @objc private func heartAction(){
        guard let password = password else {
            return
        }
        
        // realm write
        self.password?.isFavorite.toggle()
        
        self.heartBtn.setImage(password.isFavorite ? "favorite2".img() : "favorite".img(), for: .normal)
    }
    
    @objc private func textFieldAction(){
        password?.password = passwordTextField.text ?? ""
        password?.login = loginTextField.text ?? ""
        password?.title = titleTextField.text ?? ""
    }
    
    // MARK: - UI
    
    private func setupViews(){
        view.addSubview(backBtn)
        view.addSubview(saveBtn)
        view.addSubview(heartBtn)
        view.addSubview(scrollView)
        scrollView.addSubview(titleTextLabel)
        scrollView.addSubview(titleTextField)
        scrollView.addSubview(loginTextLabel)
        scrollView.addSubview(loginTextField)
        scrollView.addSubview(passwordTextLabel)
        scrollView.addSubview(passwordTextField)
        scrollView.addSubview(deleteBtn)
    }
    
    private func setConstraints(){
        backBtn.snp.makeConstraints { make in
            make.height.width.equalTo(48)
            make.top.equalTo(view.safeAreaLayoutGuide).inset(12)
            make.leading.equalToSuperview().inset(16)
        }
        
        saveBtn.snp.makeConstraints { make in
            make.height.equalTo(48)
            make.trailing.equalToSuperview().inset(16)
            make.width.equalTo(90)
            make.centerY.equalTo(backBtn)
        }
        
        heartBtn.snp.makeConstraints { make in
            make.height.width.equalTo(48)
            make.centerY.equalTo(backBtn)
            make.trailing.equalTo(saveBtn.snp.leading).inset(-8)
        }
        
        scrollView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(backBtn.snp.bottom).inset(-5)
        }
        
        titleTextLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(16)
            make.width.equalTo(UIDevice.width-32)
            make.height.equalTo(20)
            make.top.equalToSuperview().inset(9)
        }
        
        titleTextField.snp.makeConstraints { make in
            make.leading.trailing.equalTo(titleTextLabel)
            make.height.equalTo(56)
            make.top.equalTo(titleTextLabel.snp.bottom).inset(-8)
        }
        
        loginTextLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(titleTextLabel)
            make.width.equalTo(UIDevice.width-32)
            make.height.equalTo(20)
            make.top.equalTo(titleTextField.snp.bottom).inset(-16)
        }
        
        loginTextField.snp.makeConstraints { make in
            make.leading.trailing.equalTo(titleTextLabel)
            make.height.equalTo(56)
            make.top.equalTo(loginTextLabel.snp.bottom).inset(-8)
        }
        
        passwordTextLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(titleTextLabel)
            make.width.equalTo(UIDevice.width-32)
            make.height.equalTo(20)
            make.top.equalTo(loginTextField.snp.bottom).inset(-16)
        }
        
        passwordTextField.snp.makeConstraints { make in
            make.leading.trailing.equalTo(titleTextLabel)
            make.height.equalTo(56)
            make.top.equalTo(passwordTextLabel.snp.bottom).inset(-8)
        }
        
        deleteBtn.snp.makeConstraints { make in
            make.height.equalTo(35)
            make.width.equalTo(200)
            make.centerX.equalTo(titleTextField)
            make.top.equalTo(passwordTextField.snp.bottom).inset(-8)
        }
    }
    
    // MARK: -
    
    private func setData(){
        guard let password = self.password else {return}
        self.titleTextField.text = password.title
        self.loginTextField.text = password.login
        self.passwordTextField.text = password.password
        self.heartBtn.setImage(password.isFavorite ? "favorite2".img() : "favorite".img(), for: .normal)
    }
    
}


// MARK: - UITextFieldDelegate

extension PasswordViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
