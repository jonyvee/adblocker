//
//  TutorialVC.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 21.01.2022.
//

import UIKit
import GoogleMobileAds

class TutorialVC: UIViewController, AdBlocker{
    
    private var banner: GADBannerView?
    private let titleLabel = UILabel()
    private let closeBtn = UIButton()
    private var collectionView: UICollectionView?
    private let circleLabel = UILabel()
    private let title2Label = UILabel()
    private let descriptionLabel = UILabel()
    private let nextBtn = UIButton()
    private let stackView = UIStackView()
    private var segmentsRef: [UIView] = []
    private var segmentsConstraintsRef: [NSLayoutConstraint] = []
    
    private var choosedTutorial: Int = 0{
        didSet{
            updateAfterScroll()
        }
    }
    
    private var isAdEnabled = false
    private var isAdultEnabled = false
    private var isMalvareEnabled = false
    private var isScriptEnabled = false
    private var isCookieEnabled = false
    
    var didClose: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(updateGADUI), name: NSNotification.Name.init(rawValue: "RELOAD_CONTROL"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        self.view.backgroundColor = .white
        setCloseBtn()
        setTitleLabel()
        setCollectionView()
        setCircleLabel()
        setTitle2Label()
        setDescriptionLabel()
        setStackView()
        setNextBtn()
        setGAD()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        applicationDidBecomeActive()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    
    // MARK: - Actions
    @objc private func closeAction(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "tutorialCloseAction", params: [:])
        didClose?()
    }
    
    @objc private func nextAction(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "tutorialNext\(choosedTutorial)Action", params: [:])
        if choosedTutorial < 4{
            choosedTutorial+=1
        }else{
            UIApplication.shared.open(URL.init(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
        }
    }
    
    @objc func applicationDidBecomeActive() {
        getState(host: .ad, completion: { isEnabled in
            self.isAdEnabled = isEnabled
            DispatchQueue.main.async {self.checkEnabling()}
        })
        getState(host: .malvare, completion: { isEnabled in
            self.isMalvareEnabled = isEnabled
            DispatchQueue.main.async {self.checkEnabling()}
        })
        getState(host: .script, completion: { isEnabled in
            self.isScriptEnabled = isEnabled
            DispatchQueue.main.async {self.checkEnabling()}
        })
        getState(host: .adult, completion: { isEnabled in
            self.isAdultEnabled = isEnabled
            DispatchQueue.main.async {self.checkEnabling()}
        })
        getState(host: .cookie, completion: { isEnabled in
            self.isCookieEnabled = isEnabled
            DispatchQueue.main.async {self.checkEnabling()}
        })
    }
    
    
    @objc private func updateGADUI(){
        if Panela.canShowMarketing && User.shared.isPurchased == false && UIDevice.hasNotch == true{
            if banner == nil{
                setGAD()
            }else{
                banner?.isHidden = false
            }
        }else{
            banner?.isHidden = true
        }
    }
    
    private func setGAD(){
        if Panela.canShowMarketing && User.shared.isPurchased == false && UIDevice.hasNotch == true{
            self.banner = GADBannerView(adSize: GADAdSizeBanner)
            banner?.translatesAutoresizingMaskIntoConstraints = false
            banner?.adUnitID = AdManager.Ids.banner2.rawValue
            banner?.rootViewController = self
            banner?.isAutoloadEnabled = true
            self.view.addSubview(banner!)
            banner?.heightAnchor.constraint(equalToConstant: 50).isActive = true
            banner?.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
            banner?.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
            banner?.bottomAnchor.constraint(equalTo: nextBtn.topAnchor, constant: -8).isActive = true
        }
    }
    
    
    
    // MARK: - Funcionality
    private func updateAfterScroll(){
        let tutorial = TutorialData.tutorials[choosedTutorial]
        
        self.collectionView?.scrollToItem(at: IndexPath(row: choosedTutorial, section: 0), at: .centeredHorizontally, animated: true)
        circleLabel.text = (choosedTutorial+1).description
        title2Label.text = tutorial.title
        descriptionLabel.text = tutorial.description
        
        if choosedTutorial == 4{
            nextBtn.setTitle("Open the settings", for: .normal)
        }else{
            nextBtn.setTitle("Next", for: .normal)
        }
        
        segmentsRef.enumerated().forEach { element in
            let isChoosed = choosedTutorial == element.offset
            segmentsConstraintsRef[element.offset].constant = isChoosed ? 48 : 16
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn) {
                self.stackView.layoutIfNeeded()
                element.element.backgroundColor = isChoosed ? UIColor(red: 0.118, green: 0.839, blue: 0.579, alpha: 1) : UIColor(red: 0.165, green: 0.188, blue: 0.22, alpha: 0.6)
            } completion: { completed in }
        }
    }
    
    private func checkEnabling(){
        if isAdEnabled && isScriptEnabled && isMalvareEnabled && isAdultEnabled && isCookieEnabled{
            AnalyticsManager.shared.reportEvent(vc: self, name: "tutorialPermissionsAgreed", params: [:])
            closeBtn.isHidden = false
        }else{
            closeBtn.isHidden = true
        }
    }
    
    
    
    // MARK: - UI
    private func setCloseBtn(){
        view.addSubview(closeBtn)
        closeBtn.translatesAutoresizingMaskIntoConstraints = false
        closeBtn.isHidden = true
        closeBtn.setImage("close".img(), for: .normal)
        closeBtn.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        closeBtn.heightAnchor.constraint(equalToConstant: 44).isActive = true
        closeBtn.widthAnchor.constraint(equalToConstant: 44).isActive = true
        closeBtn.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16).isActive = true
        closeBtn.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
    }
    
    private func setTitleLabel(){
        view.addSubview(titleLabel)
        titleLabel.modifyTitle(text: "Active application")
        titleLabel.heightAnchor.constraint(equalToConstant: 29).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 18).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: closeBtn.centerYAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: closeBtn.leadingAnchor, constant: -8).isActive = true
    }
    
    private func setCollectionView(){
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView?.translatesAutoresizingMaskIntoConstraints = false
        collectionView?.backgroundColor = .clear
        collectionView?.showsVerticalScrollIndicator = false
        collectionView?.showsHorizontalScrollIndicator = false
        collectionView?.isPagingEnabled = true
        
        let padding: CGFloat = UIDevice.hasNotch ? 36 : 82
        
        let width = UIDevice.width-padding
        let height = (UIDevice.width-padding)*1.106194690265487
    
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets.init(top: 0, left: padding/2, bottom: 0, right: padding/2)
        layout.itemSize = CGSize(width: width, height: height)
        layout.minimumLineSpacing = padding
        collectionView?.setCollectionViewLayout(layout, animated: true)
        collectionView?.dataSource = self
        collectionView?.delegate = self
        
        self.view.addSubview(collectionView!)
        collectionView?.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        collectionView?.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        collectionView?.topAnchor.constraint(equalTo: closeBtn.bottomAnchor, constant: 16).isActive = true
        collectionView?.heightAnchor.constraint(equalToConstant: height+1).isActive = true
        
        // register cell
        collectionView?.register(TutorialCell.self, forCellWithReuseIdentifier: TutorialCell.key)
    }
    
    private func setCircleLabel(){
        let circleView = UIView()
        circleView.translatesAutoresizingMaskIntoConstraints = false
        circleView.layer.cornerRadius = 12
        circleView.backgroundColor = UIColor(red: 0, green: 0.642, blue: 0.412, alpha: 1)
        view.addSubview(circleView)
        circleView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        circleView.widthAnchor.constraint(equalToConstant: 32).isActive = true
        circleView.topAnchor.constraint(equalTo: collectionView!.bottomAnchor, constant: 32).isActive = true
        circleView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 18).isActive = true
        
        circleLabel.text = "1"
        circleLabel.translatesAutoresizingMaskIntoConstraints = false
        circleLabel.textColor = .white
        circleLabel.font = Font.bold.font(size: 16)
        circleLabel.textAlignment = .center
        circleView.addSubview(circleLabel)
        circleLabel.pinToSuperViewEdges()
    }
    
    private func setTitle2Label(){
        title2Label.translatesAutoresizingMaskIntoConstraints = false
        title2Label.textColor = Color.title.ui
        title2Label.font = Font.bold.font(size: 24)
        title2Label.adjustsFontSizeToFitWidth = true
        title2Label.text = "Open the settings"
        view.addSubview(title2Label)
        title2Label.heightAnchor.constraint(equalToConstant: 29).isActive = true
        title2Label.leadingAnchor.constraint(equalTo: circleLabel.trailingAnchor, constant: 16).isActive = true
        title2Label.centerYAnchor.constraint(equalTo: circleLabel.centerYAnchor).isActive = true
        title2Label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -18).isActive = true
    }
    
    private func setDescriptionLabel(){
        descriptionLabel.text = "Exit the app and go to your\ndevice settings"
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.textColor = Color.subtitle.ui
        descriptionLabel.numberOfLines = 2
        descriptionLabel.font = Font.regular.font(size: 16)
        descriptionLabel.adjustsFontSizeToFitWidth = true
        view.addSubview(descriptionLabel)
        descriptionLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        descriptionLabel.leadingAnchor.constraint(equalTo: title2Label.leadingAnchor).isActive = true
        descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -18).isActive = true
        descriptionLabel.topAnchor.constraint(equalTo: circleLabel.bottomAnchor, constant: 8).isActive = true
    }
    
    private func setStackView(){
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.spacing = 8
        view.addSubview(stackView)
        stackView.heightAnchor.constraint(equalToConstant: 4).isActive = true
        stackView.leadingAnchor.constraint(equalTo: title2Label.leadingAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 10).isActive = true
        
        for i in 0..<5{
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.layer.cornerRadius = 2
            stackView.addArrangedSubview(view)
            segmentsRef.append(view)
            
            if i == 0{
                let constraint = view.widthAnchor.constraint(equalToConstant: 48)
                segmentsConstraintsRef.append(constraint)
                constraint.isActive = true
                view.backgroundColor = UIColor(red: 0.118, green: 0.839, blue: 0.579, alpha: 1)
            }else{
                let constraint = view.widthAnchor.constraint(equalToConstant: 16)
                segmentsConstraintsRef.append(constraint)
                constraint.isActive = true
                view.backgroundColor = UIColor(red: 0.165, green: 0.188, blue: 0.22, alpha: 0.6)
            }
        }
    }
    
    private func setNextBtn(){
        nextBtn.translatesAutoresizingMaskIntoConstraints = false
        nextBtn.setTitle("Next", for: .normal)
        nextBtn.titleLabel?.font = Font.bold.font(size: 16)
        nextBtn.setTitleColor(.white, for: .normal)
        nextBtn.backgroundColor = Color.mainGreen2.ui
        nextBtn.layer.cornerRadius = 16
        nextBtn.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        view.addSubview(nextBtn)
        nextBtn.heightAnchor.constraint(equalToConstant: 52).isActive = true
        nextBtn.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 18).isActive = true
        nextBtn.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -18).isActive = true
        nextBtn.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -8).isActive = true
    }
}


extension TutorialVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        choosedTutorial = Int(scrollView.contentOffset.x)/Int(scrollView.frame.width)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return TutorialData.tutorials.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TutorialCell.key, for: indexPath as IndexPath) as! TutorialCell
        let image = TutorialData.tutorials[indexPath.row].image
        cell.configure(image: image)
        return cell
    }
}
