//
//  PhotoViewController.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 26.03.2022.
//

import UIKit
import SwiftSpinner
import SnapKit
import RealmSwift

class PhotoViewController: UIViewController, ImageSaverManager{
    
    // MARK: - Properties
    
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var backBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        button.setImage("back".img(), for: .normal)
        return button
    }()
    
    private lazy var favoriteBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(favoriteAction), for: .touchUpInside)
        return button
    }()
    
    private lazy var shareBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(shareAction), for: .touchUpInside)
        button.setImage("share".img(), for: .normal)
        return button
    }()
    
    private lazy var deleteBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(deleteAction), for: .touchUpInside)
        button.setImage("delete".img(), for: .normal)
        return button
    }()
    
    // MARK: -
    
    var didBack: (() -> ())?
    
    // MARK: -
    
    private let model: PictureModel
    
    // MARK: - Initialization
    
    init(model: PictureModel) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        setViews()
        setConstraints()
        updateFavoriteUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let photo = self.retrieveImage(forKey: model.id){
            self.imageView.image = photo
        }
    }

    
    // MARK: - Actions
    
    @objc private func backAction(){
        didBack?()
    }
    
    @objc private func favoriteAction(){
        try! realm.write{
            model.isFavorite.toggle()
        }
        updateFavoriteUI()
    }
    
    @objc private func shareAction(){
        if let image = self.retrieveImage(forKey: model.id){
            ShareManager.share(items: [image])
        }
    }
    
    @objc private func deleteAction(){
        let controller = UIAlertController(title: "Are you sure?", message: "This photo will be deleted", preferredStyle: .alert)
        let actionOk = UIAlertAction(title: "Ok", style: .default) { action in
            try! realm.write{
                realm.delete(self.model)
            }
            self.didBack?()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        controller.addAction(cancelAction)
        controller.addAction(actionOk)
        self.present(controller, animated: true)
    }
    
    // MARK: - UI
    
    private func setViews(){
        view.addSubview(imageView)
        view.addSubview(backBtn)
        view.addSubview(shareBtn)
        view.addSubview(favoriteBtn)
        view.addSubview(deleteBtn)
    }
    
    private func setConstraints(){
        imageView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        backBtn.snp.makeConstraints { make in
            make.width.height.equalTo(48)
            make.top.equalTo(view.safeAreaLayoutGuide).inset(8)
            make.leading.equalToSuperview().inset(12)
        }
        
        favoriteBtn.snp.makeConstraints { make in
            make.width.height.equalTo(48)
            make.centerX.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide).inset(16)
        }
        
        shareBtn.snp.makeConstraints { make in
            make.width.height.equalTo(48)
            make.trailing.equalTo(favoriteBtn.snp.leading).inset(-24)
            make.bottom.equalTo(view.safeAreaLayoutGuide).inset(16)
        }
        
        deleteBtn.snp.makeConstraints { make in
            make.width.height.equalTo(48)
            make.leading.equalTo(favoriteBtn.snp.trailing).inset(-24)
            make.bottom.equalTo(view.safeAreaLayoutGuide).inset(16)
        }
    }
    
    private func updateFavoriteUI(){
        favoriteBtn.setImage(model.isFavorite ? "favorite2".img() : "favorite".img(), for: .normal)
    }
}
