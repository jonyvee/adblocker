//
//  MyBoardVC.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 25.01.2022.
//

import UIKit
import Lottie


class MyBoardVC: UIViewController{
    
    private let titleLabel = UILabel()
    private let subtitleLabel = UILabel()
    private var lottieView: AnimationView?
    private var timer: Timer?
    
    var didShowBoard2: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        timer = Timer.scheduledTimer(timeInterval: TimeInterval(User.shared.myboard!.duration), target: self, selector: #selector(showBoard2Action), userInfo: nil, repeats: false)
        
        view.backgroundColor = .white
        setTitleLabel()
        setLottieView()
        setSubtitleLabel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        playLottieView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
    }
    
    
    @objc private func showBoard2Action(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "showMyBoard2", params: [:])
        didShowBoard2?()
    }
    
    
    // MARK: - Funcionality
    private func playLottieView(){
        lottieView?.play(fromProgress: 0, toProgress: 1, loopMode: .loop) { isFinished in}
    }
    
    // MARK: - UI
    private func setTitleLabel(){
        titleLabel.modifyTitle(text: User.shared.myboard!.title)
        titleLabel.text = "Scanning a smartphone"
        titleLabel.textAlignment = .center
        view.addSubview(titleLabel)
        titleLabel.heightAnchor.constraint(equalToConstant: 29).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 18).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -18).isActive = true
        titleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 24).isActive = true
    }
    
    private func setLottieView(){
        lottieView = .init(name: "animation")
        lottieView?.contentMode = .scaleAspectFit
        lottieView?.animationSpeed = 1
        lottieView?.isUserInteractionEnabled = false
        lottieView?.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lottieView!)
        lottieView?.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        lottieView?.heightAnchor.constraint(equalTo: lottieView!.widthAnchor).isActive = true
        lottieView?.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        lottieView?.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    private func setSubtitleLabel(){
        subtitleLabel.translatesAutoresizingMaskIntoConstraints = false
        subtitleLabel.numberOfLines = 2
        subtitleLabel.text = "Don't close the app, we're\nscanning your smartphone!"
        subtitleLabel.textColor = UIColor(red: 0.518, green: 0.549, blue: 0.604, alpha: 1)
        subtitleLabel.textAlignment = .center
        subtitleLabel.font = Font.regular.font(size: 16)
        subtitleLabel.adjustsFontSizeToFitWidth = true
        view.addSubview(subtitleLabel)
        subtitleLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        subtitleLabel.topAnchor.constraint(equalTo: lottieView!.bottomAnchor, constant: 24).isActive = true
        subtitleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 18).isActive = true
        subtitleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -18).isActive = true
    }
    
}
