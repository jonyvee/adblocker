//
//  MyBoardVC2.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 25.01.2022.
//

import UIKit
import Adapty
import SwiftSpinner

class MyBoardVC2: UIViewController, JonyMessage{
    
    private let titleLabel = UILabel()
    private var bannerView1 = UIView()
    private var bannerView2 = UIView()
    private var bannerView3 = UIView()
    private let nextBtn = UIButton()
    
    private let BANNER_WIDTH: CGFloat = (UIDevice.width-45)/2
    
    var didShowPurchase: (() -> ())?
    var didShowOnboard: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ui
        self.view.backgroundColor = .white
        setTitleLabel()
        setBannerView1()
        setBannerView2()
        setBannerView3()
        setNextBtn()
        
        // load purchases
        loadPurchases()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        bannerView1.applyGradient(colours: [UIColor(red: 1, green: 0.337, blue: 0.337, alpha: 1),
                                            UIColor(red: 0.792, green: 0.228, blue: 0.228, alpha: 1)], locations: [0,1], startPoint: .init(x: 0.5, y: 0), endPoint: .init(x: 1, y: 0.5), cornerRadius: 16)
        bannerView2.applyGradient(colours: [UIColor(red: 1, green: 0.337, blue: 0.337, alpha: 1),
                                            UIColor(red: 0.792, green: 0.228, blue: 0.228, alpha: 1)], locations: [0,1], startPoint: .init(x: 0.5, y: 0), endPoint: .init(x: 1, y: 0.5), cornerRadius: 16)
        bannerView3.applyGradient(colours: [UIColor(red: 1, green: 0.337, blue: 0.337, alpha: 1),
                                            UIColor(red: 0.792, green: 0.228, blue: 0.228, alpha: 1)], locations: [0,1], startPoint: .init(x: 0.5, y: 0), endPoint: .init(x: 1, y: 0.5), cornerRadius: 16)
    }
    
    
    // MARK: - Actions
    @objc private func nextAction(){
        if User.shared.myboard?.showPaywall == true{
            AnalyticsManager.shared.reportEvent(vc: self, name: "myBoard2ShowPurchaseAction", params: [:])
            didShowPurchase?()
        }else{
            AnalyticsManager.shared.reportEvent(vc: self, name: "myBoard2PayAction", params: [:])
            guard let product = PayManager.shared.getProduct(.week) else {return}
            
            SwiftSpinner.show("Just a second")
            
            Adapty.makePurchase(product: product) { purchaserInfo, receipt, appleValidationResult, product, error in
                DispatchQueue.main.async {
                    SwiftSpinner.hide()
                }
                
                if error != nil{
                    DispatchQueue.main.async {self.message(text: "Something went wrong, try again")}
                    AnalyticsManager.shared.reportEvent(vc: self, name: "purchaseCanceled", params: [:])
                    return
                }
                
                AnalyticsManager.shared.logSubscribeEvent(orderId: UIDevice.getId(), currency: product?.currencyCode ?? "", price: product?.skProduct?.price.doubleValue ?? 0, contentId: product?.vendorProductId ?? "", duration: "week")
                User.shared.isPurchased = true

                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RELOAD_CONTROL"), object: nil)
                    self.didShowOnboard?()
                }
            }
        }
    }
    
    
    
    // MARK: - Funcionality
    private func loadPurchases(){
        PayManager.shared.loadProducts { _ in}
    }
    
    private func createBanner(image: UIImage?, count: Int, text: String) -> UIView{
        let bannerView = UIView()
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        
        let imageView = UIImageView(image: image)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        bannerView.addSubview(imageView)
        imageView.leadingAnchor.constraint(equalTo: bannerView.leadingAnchor, constant: 9).isActive = true
        imageView.topAnchor.constraint(equalTo: bannerView.topAnchor, constant: 12).isActive = true
        imageView.widthAnchor.constraint(equalTo: bannerView.widthAnchor, multiplier: 0.303030303030303).isActive = true
        imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor).isActive = true
        
        let numberLabel = UILabel()
        numberLabel.translatesAutoresizingMaskIntoConstraints = false
        numberLabel.textColor = .white
        numberLabel.font = Font.bold.font(size: 32)
        numberLabel.text = count.description
        numberLabel.adjustsFontSizeToFitWidth = true
        bannerView.addSubview(numberLabel)
        numberLabel.heightAnchor.constraint(equalTo: bannerView.heightAnchor, multiplier: 0.238372093023256).isActive = true
        numberLabel.leadingAnchor.constraint(equalTo: bannerView.leadingAnchor, constant: 13).isActive = true
        numberLabel.trailingAnchor.constraint(equalTo: bannerView.trailingAnchor, constant: -13).isActive = true
        numberLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 9).isActive = true
        
        let textLabel = UILabel()
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.text = text
        textLabel.textColor = .white
        textLabel.font = Font.medium.font(size: 14)
        textLabel.numberOfLines = 2
        textLabel.adjustsFontSizeToFitWidth = true
        bannerView.addSubview(textLabel)
        textLabel.leadingAnchor.constraint(equalTo: numberLabel.leadingAnchor).isActive = true
        textLabel.trailingAnchor.constraint(equalTo: numberLabel.trailingAnchor).isActive = true
        textLabel.topAnchor.constraint(equalTo: numberLabel.bottomAnchor, constant: 4).isActive = true
        textLabel.bottomAnchor.constraint(equalTo: bannerView.bottomAnchor, constant: -8).isActive = true
        
        return bannerView
    }
    
    
    
    
    // MARK: - UI
    private func setTitleLabel(){
        titleLabel.modifyTitle(text: User.shared.myboard!.title2)
        titleLabel.textAlignment = .left
        view.addSubview(titleLabel)
        titleLabel.heightAnchor.constraint(equalToConstant: 29).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 18).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -18).isActive = true
        titleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 24).isActive = true
    }
    
    private func setNextBtn(){
        nextBtn.modify(title: User.shared.myboard!.button)
        nextBtn.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        view.addSubview(nextBtn)
        nextBtn.heightAnchor.constraint(equalToConstant: 52).isActive = true
        nextBtn.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 18).isActive = true
        nextBtn.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -18).isActive = true
        nextBtn.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -8).isActive = true
    }
    
    private func setBannerView1(){
        let text = User.shared.myboard!.banner1
        bannerView1 = createBanner(image: "myboard1".img(), count: 112, text: text)
        bannerView1.widthAnchor.constraint(equalToConstant: BANNER_WIDTH).isActive = true
        bannerView1.heightAnchor.constraint(equalTo: bannerView1.widthAnchor, multiplier: 1.042424242424242).isActive = true
        bannerView1.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 18).isActive = true
        bannerView1.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 24).isActive = true
    }
    
    private func setBannerView2(){
        let text = User.shared.myboard!.banner2
        bannerView2 = createBanner(image: "myboard2".img(), count: 82, text: text)
        bannerView2.widthAnchor.constraint(equalToConstant: BANNER_WIDTH).isActive = true
        bannerView2.heightAnchor.constraint(equalTo: bannerView1.widthAnchor, multiplier: 1.042424242424242).isActive = true
        bannerView2.leadingAnchor.constraint(equalTo: bannerView1.trailingAnchor, constant: 9).isActive = true
        bannerView2.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 24).isActive = true
    }
    
    private func setBannerView3(){
        let text = User.shared.myboard!.banner3
        bannerView3 = createBanner(image: "myboard3".img(), count: 32, text: text)
        bannerView3.widthAnchor.constraint(equalToConstant: BANNER_WIDTH).isActive = true
        bannerView3.heightAnchor.constraint(equalTo: bannerView1.widthAnchor, multiplier: 1.042424242424242).isActive = true
        bannerView3.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 18).isActive = true
        bannerView3.topAnchor.constraint(equalTo: bannerView1.bottomAnchor, constant: 8).isActive = true
    }
    
}
