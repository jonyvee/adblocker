//
//  SettingsVC.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 20.01.2022.
//

import UIKit
import SafariServices
import MessageUI
import SwiftSpinner
import GoogleMobileAds


class SettingsVC: UIViewController, JonyMessage, MFMailComposeViewControllerDelegate{
    
    private let closeBtn = UIButton()
    private let titleLabel = UILabel()
    private var collectionView: UICollectionView?
    private var banner: GADBannerView?
    
    var didClose: (() -> ())?
    var didShowPurchase: (() -> ())?
    var didShowTutorial: (() -> ())?
    var didShowPassSettings: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        setCloseBtn()
        setTitleLabel()
        setCollectionView()
        setGAD()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionView?.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(updateGADUI), name: NSNotification.Name.init(rawValue: "RELOAD_CONTROL"), object: nil)
        
        
        
        print("Поменять на метод PurchaseDelegate")
        self.collectionView?.reloadData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Actions
    @objc private func closeAction(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "closeSettingsAction", params: [:])
        didClose?()
    }
    
    @objc private func updateGADUI(){
        if Panela.canShowMarketing && User.shared.isPurchased == false{
            if banner == nil{
                setGAD()
            }else{
                banner?.isHidden = false
            }
        }else{
            banner?.isHidden = true
        }
    }
    
    private func setGAD(){
        if Panela.canShowMarketing && User.shared.isPurchased == false{
            self.banner = GADBannerView(adSize: GADAdSizeBanner)
            banner?.translatesAutoresizingMaskIntoConstraints = false
            banner?.adUnitID = AdManager.Ids.banner4.rawValue
            banner?.rootViewController = self
            banner?.isAutoloadEnabled = true
            self.view.addSubview(banner!)
            banner?.heightAnchor.constraint(equalToConstant: 50).isActive = true
            banner?.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
            banner?.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
            banner?.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        }
    }
        
    // MARK: - Funcionality
    private func purchaseAction(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "settingsShowPurchaseAction", params: [:])
        didShowPurchase?()
    }
    
    private func howToUseAction(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "howToUseAction", params: [:])
        didShowTutorial?()
    }
    
    private func supportAction(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "supportAction", params: [:])
    }
    
    private func changePasscodeSettings(){
        didShowPassSettings?()
    }
    
    private func shareAction(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "shareAction", params: [:])
        let text = "This is the greate AdBlocker, can block all your ads https://apps.apple.com/us/app/ad-blocker-max/id1606941362"
        guard let image = "board1".img() else {return}
        let items = [text, image] as [Any]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        let fakeViewController = UIViewController()
        fakeViewController.modalPresentationStyle = .overFullScreen
        
        ac.completionWithItemsHandler = { [weak fakeViewController] _, _, _, _ in
            if let presentingViewController = fakeViewController?.presentingViewController {
                presentingViewController.dismiss(animated: false, completion: nil)
            } else {
                fakeViewController?.dismiss(animated: false, completion: nil)
            }
        }
        
        if let popOver = ac.popoverPresentationController {
            popOver.sourceView = self.view
        }
                
        SwiftSpinner.show("Just a second")
        present(fakeViewController, animated: true) { [weak fakeViewController] in
            fakeViewController?.present(ac, animated: true, completion: {
                SwiftSpinner.hide()
            })
        }
    }
    
    private func rateAction(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "rateAction", params: [:])
        if let url = URL(string: "itms-apps://apple.com/app/id1606941362"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
    
    private func privacyAction(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "privacyAction", params: [:])
        if let url = URL(string: "https://clck.ru/amufY") {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
    }
    
    private func termsAction(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "termsAction", params: [:])
        if let url = URL(string: "https://clck.ru/amuez") {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
    }

    func moveTo(index: Int){
        switch index{
        case 0:
            howToUseAction()
        case 1:
            changePasscodeSettings()
        case 2:
            supportAction()
        case 3:
            shareAction()
        case 4:
            rateAction()
        case 5:
            privacyAction()
        default:
            termsAction()
        }
    }
    
    
    // MARK: - UI
    private func setCloseBtn(){
        view.addSubview(closeBtn)
        closeBtn.translatesAutoresizingMaskIntoConstraints = false
        closeBtn.setImage("close".img(), for: .normal)
        closeBtn.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        closeBtn.heightAnchor.constraint(equalToConstant: 44).isActive = true
        closeBtn.widthAnchor.constraint(equalToConstant: 44).isActive = true
        closeBtn.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16).isActive = true
        closeBtn.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
    }
    
    private func setTitleLabel(){
        view.addSubview(titleLabel)
        titleLabel.modifyTitle(text: "Settings")
        titleLabel.heightAnchor.constraint(equalToConstant: 29).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 18).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: closeBtn.centerYAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: closeBtn.leadingAnchor, constant: -8).isActive = true
    }
    
    private func setCollectionView(){
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView?.translatesAutoresizingMaskIntoConstraints = false
        collectionView?.backgroundColor = .clear
        collectionView?.showsVerticalScrollIndicator = false
        collectionView?.showsHorizontalScrollIndicator = false
        
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets.init(top: 19, left: 18, bottom: 0, right: 18)
        collectionView?.setCollectionViewLayout(layout, animated: true)
        collectionView?.dataSource = self
        collectionView?.delegate = self
        
        self.view.addSubview(collectionView!)
        collectionView?.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        collectionView?.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        collectionView?.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        collectionView?.topAnchor.constraint(equalTo: closeBtn.bottomAnchor).isActive = true
        
        // register cell
        collectionView?.register(SettingCell.self, forCellWithReuseIdentifier: SettingCell.key)
        collectionView?.register(SettingPurchaseCell.self, forCellWithReuseIdentifier: SettingPurchaseCell.key)
    }
}

extension SettingsVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return User.shared.isPurchased! ? 1 : 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return User.shared.isPurchased! ? SettingsData.titles.count : (section == 0 ? 1 : SettingsData.titles.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0{
            if User.shared.isPurchased == false{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SettingPurchaseCell.key, for: indexPath as IndexPath) as! SettingPurchaseCell
                cell.configure()
                
                cell.layoutIfNeeded()
                
                return cell
            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SettingCell.key, for: indexPath as IndexPath) as! SettingCell
                cell.configure(index: indexPath.row)
                
                
                
                return cell
            }
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SettingCell.key, for: indexPath as IndexPath) as! SettingCell
            cell.configure(index: indexPath.row)
            
            
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0{
            if User.shared.isPurchased == true{
                moveTo(index: indexPath.row)
            }else{
                purchaseAction()
            }
            
        }else{
            moveTo(index: indexPath.row)
        }
    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.view.frame.size.width - 36

        if indexPath.section == 0{
            if User.shared.isPurchased == false{
                return CGSize(width: width, height: 83)
            }else{
                return CGSize(width: width, height: 64)
            }
        }else{
            return CGSize(width: width, height: 64)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        if section == 0{
            if User.shared.isPurchased == false{
                return 20
            }else{
                return 8
            }
        }else{
            return 8
        }
    }
}
