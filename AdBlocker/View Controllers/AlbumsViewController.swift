//
//  AlbumsViewController.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 26.03.2022.
//

import UIKit
import SnapKit
import RealmSwift
import BSImagePicker
import SwiftSpinner
import Photos

class AlbumsViewController: UIViewController, ImageSaverManager{
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Photo storage"
        label.textColor = UIColor(rgb: 0x2A3038)
        label.font = UIFont(name: "Montserrat-Bold", size: 24)
        return label
    }()
    
    private lazy var editBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(editAction), for: .touchUpInside)
        button.setTitle("Edit", for: .normal)
        button.setTitleColor(UIColor(rgb: 0x2A3038), for: .normal)
        button.titleLabel?.font = UIFont(name: "Montserrat-Medium", size: 15)
        return button
    }()
    
    private let selectView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 0.888, green: 0.992, blue: 0.967, alpha: 1)
        view.layer.cornerRadius = 16
        return view
    }()
    
    private lazy var allButton: UIButton = {
        let button = UIButton()
        button.setTitle("All", for: .normal)
        button.addTarget(self, action: #selector(allAction), for: .touchUpInside)
        button.titleLabel?.font = UIFont(name: "Montserrat-Bold", size: 16)
        button.layer.cornerRadius = 16
        return button
    }()
    
    private lazy var favoriteButton: UIButton = {
        let button = UIButton()
        button.setTitle("Favorite", for: .normal)
        button.addTarget(self, action: #selector(favoriteAction), for: .touchUpInside)
        button.titleLabel?.font = UIFont(name: "Montserrat-Bold", size: 16)
        button.layer.cornerRadius = 16
        return button
    }()
    
    private let emptyCentringView: UIView =  UIView()
    private let emptyImageView = UIImageView(image: "photo_empty".img())
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets.init(top: 0, left: 1, bottom: 20, right: 1)
        
        collectionView.setCollectionViewLayout(layout, animated: true)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(PhotoCell.self, forCellWithReuseIdentifier: PhotoCell.key)
        
        return collectionView
    }()
    
    private lazy var addBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(addAction), for: .touchUpInside)
        button.setImage("addBtn".img(), for: .normal)
        return button
    }()
    
    // MARK: -
    
    var didSelect: ((_ model: PictureModel) -> ())?
    var didShowPurchase: (() -> ())?
    
    // MARK: -
    
    private var imagePicker: ImagePicker?
    
    // MARK: -
    
    private var isFavorite = false{
        didSet{
            if isFavorite {
                images = realm.objects(PictureModel.self).where({$0.isFavorite == true})
            }else{
                images = realm.objects(PictureModel.self)
            }
            
            self.selectedIndex.removeAll()
            self.isEditMode = false
            self.updateEditModeUI()
            self.updateEmptyViewUI()
            self.collectionView.reloadData()
        }
    }
    
    private var isEditMode = false
    private var isViewDidAppear = false
    private var selectedIndex: [Int] = []{
        didSet{
            if self.selectedIndex.isEmpty{
                self.addBtn.setImage("addBtn".img(), for: .normal)
            }else{
                self.addBtn.setImage("deletegallery".img(), for: .normal)
            }
        }
    }
    
    // MARK: -
    
    private var images = realm.objects(PictureModel.self)
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker = ImagePicker(presentationController: self, delegate: self)
        
        self.view.backgroundColor = .white
        setViews()
        setConstraints()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if isViewDidAppear == false{
            updateSelectionUI()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionView.reloadData()
        updateEmptyViewUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isViewDidAppear = true
    }
    
    // MARK: - Actions
    
    @objc private func allAction(){
        callCNVibration(style: .soft)
        isFavorite = false
        updateSelectionUI()
    }
    
    @objc private func favoriteAction(){
        callCNVibration(style: .soft)
        isFavorite = true
        updateSelectionUI()
    }
    
    @objc private func editAction(){
        Vibration.soft.vibrate()
        isEditMode.toggle()
        updateEditModeUI()
        
        if isEditMode == false{
            self.selectedIndex = []
            self.collectionView.reloadData()
        }
    }
    
    @objc private func addAction(){
        if selectedIndex.isEmpty{
            
            if self.images.count >= 10 && User.shared.isPurchased == false{
                didShowPurchase?()
            }else{
                showAlertViewController()
                
                if User.shared.isPurchased == false {
                    if Panela.canShowMarketing == true && AdManager.shared.isReady(ad: .interstitial){
                        AdManager.shared.present(ad: .interstitial, root: self, callback: self)
                    }
                }
            }

        }else{
            showDeleteAlertController()
        }
    }
    
    // MARK: - UI
    
    private func setViews(){
        view.addSubview(titleLabel)
        view.addSubview(editBtn)
        view.addSubview(selectView)
        selectView.addSubview(allButton)
        selectView.addSubview(favoriteButton)
        view.addSubview(emptyCentringView)
        emptyCentringView.addSubview(emptyImageView)
        view.addSubview(collectionView)
        view.addSubview(addBtn)
    }
    
    private func setConstraints(){
        titleLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(18)
            make.height.equalTo(29)
            make.top.equalTo(view.safeAreaLayoutGuide).inset(20)
        }
        
        editBtn.snp.makeConstraints { make in
            make.height.equalTo(35)
            make.width.equalTo(60)
            make.trailing.equalToSuperview().inset(18)
            make.centerY.equalTo(titleLabel)
        }
        
        selectView.snp.makeConstraints { make in
            make.height.equalTo(44)
            make.leading.trailing.equalToSuperview().inset(18)
            make.top.equalTo(titleLabel.snp.bottom).inset(-24)
        }

        allButton.snp.makeConstraints { make in
            make.leading.bottom.top.equalToSuperview().inset(2)
            make.width.equalTo(selectView).dividedBy(2.1)
        }

        favoriteButton.snp.makeConstraints { make in
            make.trailing.bottom.top.equalToSuperview().inset(2)
            make.width.equalTo(selectView).dividedBy(2.1)
        }

        emptyCentringView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide)
            make.top.equalTo(selectView.snp.bottom)
        }

        emptyImageView.snp.makeConstraints { make in
            make.width.equalTo(222)
            make.height.equalTo(267)
            make.center.equalToSuperview()
        }
        
        collectionView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(selectView.snp.bottom).inset(-12)
        }
        
        addBtn.snp.makeConstraints { make in
            make.height.width.equalTo(52)
            make.trailing.equalToSuperview().inset(18)
            make.bottom.equalTo(view.safeAreaLayoutGuide).inset(24)
        }
    }
    
    
    
    // MARK: - Helper
    
    private func updateSelectionUI(){
        if isFavorite{
            allButton.removeGradient()
            favoriteButton.setTitleColor(.white, for: .normal)
            favoriteButton.applyGradient(colours: [UIColor(rgb: 0x1ED694),UIColor(rgb: 0x00A469)], locations: [0,1], startPoint: CGPoint(x: 0.5, y: 0), endPoint: CGPoint(x: 0.5, y: 1), cornerRadius: 16)
            allButton.backgroundColor = .clear
            allButton.setTitleColor(UIColor(rgb: 0x2A3038), for: .normal)
        }else{
            favoriteButton.removeGradient()
            allButton.setTitleColor(.white, for: .normal)
            allButton.applyGradient(colours: [UIColor(rgb: 0x1ED694),UIColor(rgb: 0x00A469)], locations: [0,1], startPoint: CGPoint(x: 0.5, y: 0), endPoint: CGPoint(x: 0.5, y: 1), cornerRadius: 16)
            favoriteButton.backgroundColor = .clear
            favoriteButton.setTitleColor(UIColor(rgb: 0x2A3038), for: .normal)
        }
    }
    
    private func updateEditModeUI(){
        editBtn.setTitle(isEditMode ? "Cancel" : "Edit", for: .normal)
    }
    
    private func updateEmptyViewUI(){
        emptyCentringView.isHidden = !images.isEmpty
    }
    
    private func showAlertViewController(){
        let alertVC = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let takeAction = UIAlertAction(title: "Take a photo", style: .default) { action in
            self.imagePicker?.present(type: .camera)
        }
        let importAction = UIAlertAction(title: "Select from gallery", style: .default) { action in
            self.presentImagePicker()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertVC.addAction(takeAction)
        alertVC.addAction(importAction)
        alertVC.addAction(cancelAction)
        self.present(alertVC, animated: true)
    }
    
    // MARK: -
    
    private func presentImagePicker(){
        self.presentImagePicker(ImagePickerController(),
                                                       select: { (asset) in},
                                                       deselect: { asset in},
                                                       cancel: { asset in},
                                                       finish: { asset in
            
            SwiftSpinner.show("", animated: true)
            
            if User.shared.isPurchased == false{
                let enoughtCount = 10 - self.images.count
                var myAsset: [PHAsset] = []
                
                var i = 0
                
                for ass in asset{
                    if i == enoughtCount{
                        break
                    }else{
                        myAsset.append(ass)
                    }
                    i += 1
                }
                
                self.loadImage(assets: myAsset)
            }else{
                self.loadImage(assets: asset)
            }
        })
    }
    
    private func loadImage(assets: [PHAsset]) {
                
        if assets.isEmpty {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
                self.updateEmptyViewUI()
                SwiftSpinner.hide()
            }
            return
        }
        
        var assets = assets
        let asset = assets.removeFirst()
        
        let options = PHImageRequestOptions()
        options.isSynchronous = true
        PHImageManager.default().requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: options) { (image, info) in
            guard let info = info, let image = image, let result = info["PHImageResultIsDegradedKey"] as? Int, result == 0 else {
                return
            }
        
            DispatchQueue.main.async {
                self.storeImage(image: image)
            }
            
            self.loadImage(assets: assets)
        }
    }

    // MARK: -
    
    private func storeImage(image: UIImage){
        let model = PictureModel()
        self.store(image: image, forKey: model.id)

        try! realm.write{
            realm.add(model)
        }
    }
    
    // MARK: -
    
    private func updateSelectedIndex(index: Int){
        if let index = self.selectedIndex.firstIndex(where: {$0 == index}){
            self.selectedIndex.remove(at: index)
        }else{
            self.selectedIndex.append(index)
        }
    }
    
    private func showDeleteAlertController(){
        let controller = UIAlertController(title: "Delete", message: "Do you want to delete \(selectedIndex.count) photos", preferredStyle: .alert)
        let actionOk = UIAlertAction(title: "Ok", style: .default) { action in
            
            var photosForDelete: [PictureModel] = []
            
            self.selectedIndex.forEach { index in
                let model = self.images[index]
                photosForDelete.append(model)
            }
            
            try! realm.write{
                realm.delete(photosForDelete)
            }
            
            self.collectionView.reloadData()
            self.selectedIndex.removeAll()
            self.isEditMode = false
            self.updateEditModeUI()
            self.updateEmptyViewUI()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        controller.addAction(cancelAction)
        controller.addAction(actionOk)
        self.present(controller, animated: true)
    }
}

// MARK: - UICollectionViewDataSource

extension AlbumsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCell.key, for: indexPath as IndexPath) as! PhotoCell
        
        let imageData = self.images[indexPath.row]
        
        if let image = self.retrievePreviewImage(forKey: imageData.id){
            let presentable = PhotoCellPresentable(image: image, isSelected: selectedIndex.contains(indexPath.row))
            cell.configure(presentable: presentable)
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        Vibration.soft.vibrate()
        
        let imageData = self.images[indexPath.row]
        
        if isEditMode{
            updateSelectedIndex(index: indexPath.row)
            self.collectionView.reloadItems(at: [indexPath])
        }else{
            didSelect?(imageData)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let item_width = (UIDevice.width-7)/3
        return CGSize(width: item_width, height: item_width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
}


// MARK: - ImagePickerDelegate
extension AlbumsViewController: ImagePickerDelegate {
    func didSelect(image: UIImage?) {
        if let image = image{
            self.storeImage(image: image)
            self.collectionView.reloadData()
            self.updateEmptyViewUI()
        }else{
            print("image is nil")
        }
    }
}


extension AlbumsViewController: AdManagerDelegate{
    func adDidDismissFullScreenContent(isEarn: Bool) {
    }
    
    func adDidPresentFullScreenContent() {
    }
    
    func didFailToPresentFullScreenContentWithError() {
    }
}
