//
//  MethodViewController.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 05.04.2022.
//

import UIKit
import SnapKit

class MethodViewController: UIViewController, JonyMessage{
    
    // MARK: - Properties
    
    private lazy var skipBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        button.setTitle("Skip", for: .normal)
        button.titleLabel?.font = UIFont(name: "Montserrat-Regular", size: 14)
        button.setTitleColor(UIColor(red: 0.518, green: 0.549, blue: 0.604, alpha: 1), for: .normal)
        return button
    }()
    
    private lazy var withoutPassBtn: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 16
        button.backgroundColor = UIColor(red: 0, green: 0.642, blue: 0.412, alpha: 1)
        button.titleLabel?.font = UIFont(name: "Montserrat-Bold", size: 16)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        button.setTitle("Without password", for: .normal)
        return button
    }()
    
    private lazy var passBtn: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 16
        button.layer.borderWidth = 2
        button.layer.borderColor = UIColor(red: 0, green: 0.642, blue: 0.412, alpha: 1).cgColor
        button.titleLabel?.font = UIFont(name: "Montserrat-Bold", size: 16)
        button.setTitleColor(UIColor(red: 0, green: 0.642, blue: 0.412, alpha: 1), for: .normal)
        button.addTarget(self, action: #selector(withPassAction), for: .touchUpInside)
        button.setTitle("Use password", for: .normal)
        return button
    }()
    
    private let subtitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Optional password protection\nhelps protect your private photos\nand passwords. You can change\naccess options in settings"
        label.numberOfLines = 0
        label.font = UIFont(name: "Montserrat-Regular", size: 16)
        label.textColor = UIColor(red: 0.518, green: 0.549, blue: 0.604, alpha: 1)
        label.textAlignment = .center
        return label
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Choose a login method"
        label.numberOfLines = 0
        label.font = UIFont(name: "Montserrat-Bold", size: 24)
        label.textColor = UIColor(red: 0.165, green: 0.188, blue: 0.22, alpha: 1)
        label.textAlignment = .center
        return label
    }()
    
    private let imageView: UIImageView = {
        let imageView = UIImageView(image: "method".img())
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    // MARK: -
    
    var didClose: (() -> ())?
    var didUserPassword: (() -> ())?
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        
        setViews()
        setConstraints()
    }
    
    // MARK: - Actions
    
    @objc private func withPassAction(){
        Vibration.soft.vibrate()
        didUserPassword?()        
    }
    
    @objc private func closeAction(){
        Vibration.soft.vibrate()
        User.shared.isFinishedOnboard = true
        didClose?()
    }
    
    // MARK: - UI

    private func setViews(){
        view.addSubview(skipBtn)
        view.addSubview(withoutPassBtn)
        view.addSubview(passBtn)
        view.addSubview(subtitleLabel)
        view.addSubview(titleLabel)
        view.addSubview(imageView)
    }
    
    private func setConstraints(){
        skipBtn.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(24)
            make.bottom.equalTo(view.safeAreaLayoutGuide).inset(10)
            make.height.equalTo(26)
        }
        
        withoutPassBtn.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(18)
            make.height.equalTo(52)
            make.bottom.equalTo(skipBtn.snp.top).inset(-20)
        }
        
        passBtn.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(18)
            make.height.equalTo(52)
            make.bottom.equalTo(withoutPassBtn.snp.top).inset(-12)
        }
        
        subtitleLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(18)
            make.bottom.equalTo(passBtn.snp.top).inset(-24)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(18)
            make.height.equalTo(29)
            make.bottom.equalTo(subtitleLabel.snp.top).inset(-10)
        }
        
        imageView.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
            make.bottom.equalTo(titleLabel.snp.top)
        }
    }
    
}

