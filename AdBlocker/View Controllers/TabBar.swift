//
//  TabBar.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 26.03.2022.
//

import UIKit
import RealmSwift
import Realm

var realm: Realm = try! Realm()

class TabBarController: UITabBarController{
    
    
    private var isViewWillAppeared = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        // здесь можем поменять иконки для таббаров ...
        self.tabBar.backgroundColor = .white        
        self.tabBar.layer.shadowOpacity = 0.2
        self.tabBar.layer.shadowColor = UIColor(rgb: 0xA0AEC0).cgColor
        self.tabBar.layer.shadowRadius = 24
        self.tabBar.layer.shadowOffset = .init(width: 0, height: -10)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.isViewWillAppeared == false{
            isViewWillAppeared.toggle()
            self.selectedIndex = 1
        }
    }
    
}
