//
//  ViewController.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 20.01.2022.
//

import UIKit
import SwiftSpinner
import Appodeal
import SafariServices
import GoogleMobileAds

class ViewController: UIViewController, AdBlocker, Storyboardable, JonyMessage {

    private var bannerViewTopConstraint: NSLayoutConstraint?
    private var bannerViewHeihgtConstraint: NSLayoutConstraint?
    private let bannerView = UIView()
    private var banner: GADBannerView?
    private let settingBtn = UIButton()
    private let titleLabel = UILabel()
    private let scrollView = UIScrollView()
    private let blockBtn = UIButton()
    private let deviceInfoLabel = UILabel()
    private let statView1 = UIView()
    private let statLabel1 = UILabel()
    private let statView2 = UIView()
    private let statLabel2 = UILabel()
    private let additionalLabel = UILabel()
    private let additView1 = UIView()
    private let additView2 = UIView()
    private let additView3 = UIView()
    private let additView4 = UIView()
    private let addit1ImageView = UIImageView()
    private let addit2ImageView = UIImageView()
    private let addit3ImageView = UIImageView()
    private let addit4ImageView = UIImageView()
    private let addit1Label = UILabel()
    private let addit2Label = UILabel()
    private let addit3Label = UILabel()
    private let addit4Label = UILabel()
    private let addit1SubLabel = UILabel()
    private let addit2SubLabel = UILabel()
    private let addit3SubLabel = UILabel()
    private let addit4SubLabel = UILabel()
    
    private var viewDidAppeared = false
    
    
    var didShowSettings: (() -> ())?
    var didShowPurchase: (() -> ())?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(updateGADUI), name: NSNotification.Name.init(rawValue: "RELOAD_CONTROL"), object: nil)
        
        setSettingBtn()
        setTitleLabel()
        setScrollView()
        setBlockBtn()
        setDeviceInfoLabel()
        setBannerView()
        setStatView1()
        setStatView2()
        setAdditionalLabel()
        setAdditView1()
        setAdditView2()
        setAdditView3()
        setAdditView4()
        setAppodealBanner()
        setGAD()
        
        Panela.fetch()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if viewDidAppeared == false{
            statView1.applyGradient(colours: [Color.red1.ui, Color.red2.ui], locations: [0,1], startPoint: CGPoint(x: 0.5, y: 0), endPoint: CGPoint(x: 0.5, y: 1), cornerRadius: 16)
            statView2.applyGradient(colours: [Color.green2.ui, Color.mainGreen2.ui], locations: [0,1], startPoint: CGPoint(x: 0.5, y: 0), endPoint: CGPoint(x: 0.5, y: 1), cornerRadius: 16)
            
            setGlobalUI()
            setMalvaretUI()
            setScriptUI()
            setAdultUI()
            setCookiesUI()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        countSummTime()
        updateTimeUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.checkAppodeal()
    }
    
    // MARK: - Actions
    @objc private func settingsAction(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "settingsAction", params: [:])
        didShowSettings?()
    }
    
    @objc private func blockAction(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "blockAdAction", params: [:])
        callCNVibration(style: .light)
        
        if User.shared.isPurchased == false {
            if Panela.canShowMarketing == true && AdManager.shared.isReady(ad: .interstitial){
                AdManager.shared.present(ad: .interstitial, root: self, callback: self)
            }else{
                didShowPurchase?()
            }
            return
        }
        
        if User.shared.status?.ad == false{
            featchData(host: .ad)
        }else{
            self.write(host: .ad, object: nil)
            self.reload(host: .ad)
            User.shared.status?.ad.toggle()
            setGlobalUI()
        }
    }
    
    @objc private func addit1Action(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "blockAdultAction", params: [:])
        callCNVibration(style: .light)
        if User.shared.isPurchased == false {
            if Panela.canShowMarketing == true && AdManager.shared.isReady(ad: .interstitial){
                AdManager.shared.present(ad: .interstitial, root: self, callback: self)
            }else{
                didShowPurchase?()
            }
            return
        }
        
        if User.shared.status?.adult == false{
            featchData(host: .adult)
        }else{
            self.write(host: .adult, object: nil)
            self.reload(host: .adult)
            User.shared.status?.adult.toggle()
            setAdultUI()
        }
    }
    
    @objc private func addit2Action(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "blockScriptAction", params: [:])
        callCNVibration(style: .light)
        if User.shared.isPurchased == false {
            if Panela.canShowMarketing == true && AdManager.shared.isReady(ad: .interstitial){
                AdManager.shared.present(ad: .interstitial, root: self, callback: self)
            }else{
                didShowPurchase?()
            }
            return
        }
        
        if User.shared.status?.script == false{
            featchData(host: .script)
        }else{
            self.write(host: .script, object: nil)
            self.reload(host: .script)
            User.shared.status?.script.toggle()
            setScriptUI()
        }
    }
    
    @objc private func addit3Action(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "blockMalvareAction", params: [:])
        callCNVibration(style: .light)
        if User.shared.isPurchased == false {
            if Panela.canShowMarketing == true && AdManager.shared.isReady(ad: .interstitial){
                AdManager.shared.present(ad: .interstitial, root: self, callback: self)
            }else{
                didShowPurchase?()
            }
            return
        }
        
        if User.shared.status?.malvare == false{
            featchData(host: .malvare)
        }else{
            self.write(host: .malvare, object: nil)
            self.reload(host: .malvare)
            User.shared.status?.malvare.toggle()
            setMalvaretUI()
        }
    }
    
    @objc private func addit4Action(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "blockCookieAction", params: [:])
        callCNVibration(style: .light)
        if User.shared.isPurchased == false {
            if Panela.canShowMarketing == true && AdManager.shared.isReady(ad: .interstitial){
                AdManager.shared.present(ad: .interstitial, root: self, callback: self)
            }else{
                didShowPurchase?()
            }
            return
        }
        
        if User.shared.status?.cookies == false{
            featchData(host: .cookie)
        }else{
            self.write(host: .cookie, object: nil)
            self.reload(host: .cookie)
            User.shared.status?.cookies.toggle()
            setCookiesUI()
        }
    }
    
    @objc private func updateGADUI(){
        
        if Panela.canShowMarketing && User.shared.isPurchased == false{
            if banner == nil{
                setGAD()
            }else{
                banner?.isHidden = false
                bannerViewHeihgtConstraint?.constant = 50
                scrollView.contentSize.height = UIDevice.width + 560 + 58
                bannerViewTopConstraint?.constant = 8
            }
        }else{
            banner?.isHidden = true
            bannerViewHeihgtConstraint?.constant = 1
            scrollView.contentSize.height = UIDevice.width + 560
            bannerViewTopConstraint?.constant = 0
        }
    }
    
    private func setGAD(){
        if Panela.canShowMarketing && User.shared.isPurchased == false{
            bannerViewHeihgtConstraint?.constant = 50
            scrollView.contentSize.height = UIDevice.width + 560 + 58
            bannerViewTopConstraint?.constant = 8
            self.banner = GADBannerView(adSize: GADAdSizeBanner)
            banner?.translatesAutoresizingMaskIntoConstraints = false
            banner?.adUnitID = AdManager.Ids.banner3.rawValue
            banner?.rootViewController = self
            banner?.isAutoloadEnabled = true
            self.bannerView.addSubview(banner!)
            banner?.pinToSuperViewEdges()
        }
    }
    
    
    // MARK: - Funcionality
    private func featchData(host: Constants.Host){
        SwiftSpinner.show("Blocking \(host.path)...")
        TrackerListParser.fetchAndParseTrackerList(host: host) { trackers in            
            SwiftSpinner.hide()
            self.write(host: host, object: trackers)
            self.reload(host: host)
    
            DispatchQueue.main.async {
                callCNVibration(style: .light)
                
                switch host{
                case .ad:
                    User.shared.status?.ad.toggle()
                    self.setGlobalUI()
                case .adult:
                    User.shared.status?.adult.toggle()
                    self.setAdultUI()
                case .script:
                    User.shared.status?.script.toggle()
                    self.setScriptUI()
                case .malvare:
                    User.shared.status?.malvare.toggle()
                    self.setMalvaretUI()
                case .cookie:
                    User.shared.status?.cookies.toggle()
                    self.setCookiesUI()
                }
                
                self.updateDateUsingApp()
            }
        }
    }
    
    private func updateDateUsingApp(){
        let st = User.shared.status!
        if st.ad || st.adult || st.cookies || st.malvare || st.script{
            User.shared.lastWorkingDate = Date()
        }
    }
    
    private func countSummTime(){
        let st = User.shared.status!
        let isTurnedOn = st.ad || st.adult || st.cookies || st.malvare || st.script
        
        if isTurnedOn{
            let time = Date().timeIntervalSince(User.shared.lastWorkingDate!)
            let ti = Int(time)
            let minutes = (ti / 60) % 60
            if minutes != 0{
                print("jonyvee minutes = ", minutes)
                User.shared.summMinutes! += minutes
                print("jonyvee all minutes = ", User.shared.summMinutes!)
                User.shared.lastWorkingDate = Date()
            }
        }
    }
    
    private func updateTimeUI(){
        let hours = Int(User.shared.summMinutes!/60)
        let minutes = User.shared.summMinutes! % 60
        
        if hours == 0{
            statLabel2.text = "\(minutes) min"
        }else{
            statLabel2.text = "\(hours) h \(minutes) min"
        }
        
        let random = minutes == 0 ? 0 : Int.random(in: 1...10)
        statLabel1.text = "\(User.shared.summMinutes! + random) threats"
    }
    
    private func setGlobalUI(){
        blockBtn.setImage(User.shared.status!.ad ? "btnOn".img() : "btnOff".img(), for: .normal)
    }
    
    private func setAdultUI(){
        additView1.removeGradient()
        
        if User.shared.status!.adult{
            addit1Label.textColor = .white
            addit1SubLabel.textColor = .white
            addit1ImageView.image = "addit1White".img()
            additView1.applyGradient(colours: [Color.green2.ui, Color.mainGreen2.ui], locations: [0,1], startPoint: CGPoint(x: 1, y: 0), endPoint: CGPoint(x: 0, y: 1), cornerRadius: 16)
        }else{
            addit1Label.textColor = Color.title.ui
            addit1SubLabel.textColor = Color.title.ui
            addit1ImageView.image = "addit1Green".img()
            additView1.applyGradient(colours: [Color.lightGreen1.ui, Color.lightGreen2.ui], locations: [0,1], startPoint: CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 1, y: 1), cornerRadius: 16)
            
        }
    }
    
    private func setScriptUI(){
        additView2.removeGradient()
        
        if User.shared.status!.script{
            addit2Label.textColor = .white
            addit2SubLabel.textColor = .white
            addit2ImageView.image = "addit2White".img()
            additView2.applyGradient(colours: [Color.green2.ui, Color.mainGreen2.ui], locations: [0,1], startPoint: CGPoint(x: 1, y: 0), endPoint: CGPoint(x: 0, y: 1), cornerRadius: 16)
        }else{
            addit2Label.textColor = Color.title.ui
            addit2SubLabel.textColor = Color.title.ui
            addit2ImageView.image = "addit2Green".img()
            additView2.applyGradient(colours: [Color.lightGreen1.ui, Color.lightGreen2.ui], locations: [0,1], startPoint: CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 1, y: 1), cornerRadius: 16)
            
        }
    }
    
    private func setMalvaretUI(){
        additView3.removeGradient()
        
        if User.shared.status!.malvare{
            addit3Label.textColor = .white
            addit3SubLabel.textColor = .white
            addit3ImageView.image = "addit3White".img()
            additView3.applyGradient(colours: [Color.green2.ui, Color.mainGreen2.ui], locations: [0,1], startPoint: CGPoint(x: 1, y: 0), endPoint: CGPoint(x: 0, y: 1), cornerRadius: 16)
        }else{
            addit3Label.textColor = Color.title.ui
            addit3SubLabel.textColor = Color.title.ui
            addit3ImageView.image = "addit3Green".img()
            additView3.applyGradient(colours: [Color.lightGreen1.ui, Color.lightGreen2.ui], locations: [0,1], startPoint: CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 1, y: 1), cornerRadius: 16)
            
        }
    }
    
    private func setCookiesUI(){
        additView4.removeGradient()
        
        if User.shared.status!.cookies{
            addit4Label.textColor = .white
            addit4SubLabel.textColor = .white
            addit4ImageView.image = "addit4White".img()
            additView4.applyGradient(colours: [Color.green2.ui, Color.mainGreen2.ui], locations: [0,1], startPoint: CGPoint(x: 1, y: 0), endPoint: CGPoint(x: 0, y: 1), cornerRadius: 16)
        }else{
            addit4Label.textColor = Color.title.ui
            addit4SubLabel.textColor = Color.title.ui
            addit4ImageView.image = "addit4Green".img()
            additView4.applyGradient(colours: [Color.lightGreen1.ui, Color.lightGreen2.ui], locations: [0,1], startPoint: CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 1, y: 1), cornerRadius: 16)
            
        }
    }
    
    private func checkAppodeal(){
        if User.shared.isPurchased == true{
            Appodeal.hideBanner()
            scrollView.contentSize.height = UIDevice.width + 480
        }
    }
    
    
    // MARK: - UI
    private func setAppodealBanner(){
        if User.shared.isPurchased == false{
            Appodeal.showAd(.bannerBottom, rootViewController: self)
        }
    }
    
    private func setSettingBtn(){
        view.addSubview(settingBtn)
        settingBtn.translatesAutoresizingMaskIntoConstraints = false
        settingBtn.setImage("settings".img(), for: .normal)
        settingBtn.addTarget(self, action: #selector(settingsAction), for: .touchUpInside)
        settingBtn.heightAnchor.constraint(equalToConstant: 44).isActive = true
        settingBtn.widthAnchor.constraint(equalToConstant: 44).isActive = true
        settingBtn.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16).isActive = true
        settingBtn.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
    }
    
    private func setTitleLabel(){
        view.addSubview(titleLabel)
        titleLabel.modifyTitle(text: "Ad Blocker Max")
        titleLabel.heightAnchor.constraint(equalToConstant: 29).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 18).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: settingBtn.centerYAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: settingBtn.leadingAnchor, constant: -8).isActive = true
    }
    
    private func setScrollView(){
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        let height = User.shared.isPurchased! ? (UIDevice.width + 480) : (UIDevice.width + 560)
        scrollView.contentSize = CGSize(width: UIDevice.width, height: height)
        view.addSubview(scrollView)
        scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: settingBtn.bottomAnchor).isActive = true
    }
    
    private func setBlockBtn(){
        blockBtn.translatesAutoresizingMaskIntoConstraints = false
        blockBtn.setImage("btnOff".img(), for: .normal)
        blockBtn.addTarget(self, action: #selector(blockAction), for: .touchUpInside)
        blockBtn.layer.cornerRadius = (UIDevice.width-104)/2
        blockBtn.clipsToBounds = true
        scrollView.addSubview(blockBtn)
        blockBtn.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 22).isActive = true
        blockBtn.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -104).isActive = true
        blockBtn.heightAnchor.constraint(equalTo: blockBtn.widthAnchor).isActive = true
        blockBtn.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 52).isActive = true
    }
    
    private func setDeviceInfoLabel(){
        deviceInfoLabel.translatesAutoresizingMaskIntoConstraints = false
        deviceInfoLabel.font = Font.bold.font(size: 18)
        deviceInfoLabel.textColor = Color.littleTitle.ui
        deviceInfoLabel.text = "Device information"
        scrollView.addSubview(deviceInfoLabel)
        deviceInfoLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 18).isActive = true
        deviceInfoLabel.heightAnchor.constraint(equalToConstant: 22).isActive = true
        deviceInfoLabel.widthAnchor.constraint(equalToConstant: UIDevice.width-36).isActive = true
        deviceInfoLabel.topAnchor.constraint(equalTo: blockBtn.bottomAnchor, constant: 32).isActive = true
    }
    
    private func setBannerView(){
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(bannerView)
        bannerViewHeihgtConstraint = bannerView.heightAnchor.constraint(equalToConstant: 1)
        bannerViewHeihgtConstraint?.isActive = true
        bannerViewTopConstraint = bannerView.topAnchor.constraint(equalTo: deviceInfoLabel.bottomAnchor, constant: 0)
        bannerViewTopConstraint?.isActive = true
        bannerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        bannerView.widthAnchor.constraint(equalToConstant: UIDevice.width).isActive = true
    }
    
    private func setStatView1(){
        statView1.translatesAutoresizingMaskIntoConstraints = false
        statView1.layer.cornerRadius = 16
        scrollView.addSubview(statView1)
        statView1.heightAnchor.constraint(equalToConstant: 106).isActive = true
        statView1.leadingAnchor.constraint(equalTo: deviceInfoLabel.leadingAnchor).isActive = true
        statView1.trailingAnchor.constraint(equalTo: deviceInfoLabel.trailingAnchor).isActive = true
        statView1.topAnchor.constraint(equalTo: bannerView.bottomAnchor, constant: 11).isActive = true
        
        let topLabel = UILabel()
        topLabel.translatesAutoresizingMaskIntoConstraints = false
        topLabel.text = "Threats blocked on this device:"
        topLabel.adjustsFontSizeToFitWidth = true
        topLabel.textColor = .white
        topLabel.font = Font.bold.font(size: 16)
        statView1.addSubview(topLabel)
        topLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        topLabel.leadingAnchor.constraint(equalTo: statView1.leadingAnchor, constant: 12).isActive = true
        topLabel.trailingAnchor.constraint(equalTo: statView1.trailingAnchor, constant: -12).isActive = true
        topLabel.topAnchor.constraint(equalTo: statView1.topAnchor, constant: 12).isActive = true
        
        statLabel1.translatesAutoresizingMaskIntoConstraints = false
        statLabel1.textColor = .white
        statLabel1.font = Font.bold.font(size: 44)
        statLabel1.adjustsFontSizeToFitWidth = true
        statLabel1.text = "0 threats"
        statView1.addSubview(statLabel1)
        statLabel1.topAnchor.constraint(equalTo: topLabel.bottomAnchor, constant: 12).isActive = true
        statLabel1.leadingAnchor.constraint(equalTo: statView1.leadingAnchor, constant: 12).isActive = true
        statLabel1.trailingAnchor.constraint(equalTo: statView1.trailingAnchor, constant: -12).isActive = true
        statLabel1.heightAnchor.constraint(equalToConstant: 54).isActive = true
    }
    
    private func setStatView2(){
        statView2.translatesAutoresizingMaskIntoConstraints = false
        statView2.layer.cornerRadius = 16
        scrollView.addSubview(statView2)
        statView2.heightAnchor.constraint(equalToConstant: 106).isActive = true
        statView2.leadingAnchor.constraint(equalTo: deviceInfoLabel.leadingAnchor).isActive = true
        statView2.trailingAnchor.constraint(equalTo: deviceInfoLabel.trailingAnchor).isActive = true
        statView2.topAnchor.constraint(equalTo: statView1.bottomAnchor, constant: 6).isActive = true
        
        let topLabel = UILabel()
        topLabel.translatesAutoresizingMaskIntoConstraints = false
        topLabel.text = "Summary session:"
        topLabel.adjustsFontSizeToFitWidth = true
        topLabel.textColor = .white
        topLabel.font = Font.bold.font(size: 16)
        statView2.addSubview(topLabel)
        topLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        topLabel.leadingAnchor.constraint(equalTo: statView2.leadingAnchor, constant: 12).isActive = true
        topLabel.trailingAnchor.constraint(equalTo: statView2.trailingAnchor, constant: -12).isActive = true
        topLabel.topAnchor.constraint(equalTo: statView2.topAnchor, constant: 12).isActive = true
        
        statLabel2.translatesAutoresizingMaskIntoConstraints = false
        statLabel2.textColor = .white
        statLabel2.font = Font.bold.font(size: 44)
        statLabel2.adjustsFontSizeToFitWidth = true
        statLabel2.text = "0 min"
        statView2.addSubview(statLabel2)
        statLabel2.topAnchor.constraint(equalTo: topLabel.bottomAnchor, constant: 12).isActive = true
        statLabel2.leadingAnchor.constraint(equalTo: statView2.leadingAnchor, constant: 12).isActive = true
        statLabel2.trailingAnchor.constraint(equalTo: statView2.trailingAnchor, constant: -12).isActive = true
        statLabel2.heightAnchor.constraint(equalToConstant: 54).isActive = true
    }
    
    private func setAdditionalLabel(){
        additionalLabel.translatesAutoresizingMaskIntoConstraints = false
        additionalLabel.font = Font.bold.font(size: 18)
        additionalLabel.textColor = Color.littleTitle.ui
        additionalLabel.text = "Additional extension"
        scrollView.addSubview(additionalLabel)
        additionalLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 18).isActive = true
        additionalLabel.heightAnchor.constraint(equalToConstant: 22).isActive = true
        additionalLabel.widthAnchor.constraint(equalToConstant: UIDevice.width-36).isActive = true
        additionalLabel.topAnchor.constraint(equalTo: statView2.bottomAnchor, constant: 24).isActive = true
    }
    
    private func setAdditView1(){
        additView1.translatesAutoresizingMaskIntoConstraints = false
        additView1.isUserInteractionEnabled = true
        additView1.layer.cornerRadius = 16
        let gesture = UITapGestureRecognizer(target: self, action: #selector(addit1Action))
        additView1.addGestureRecognizer(gesture)
        scrollView.addSubview(additView1)
        additView1.heightAnchor.constraint(equalToConstant: 111).isActive = true
        additView1.widthAnchor.constraint(equalToConstant: (UIDevice.width-45)/2).isActive = true
        additView1.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 18).isActive = true
        additView1.topAnchor.constraint(equalTo: additionalLabel.bottomAnchor, constant: 12).isActive = true
        
        addit1ImageView.image = "addit1Green".img()
        addit1ImageView.translatesAutoresizingMaskIntoConstraints = false
        additView1.addSubview(addit1ImageView)
        addit1ImageView.heightAnchor.constraint(equalToConstant: 43).isActive = true
        addit1ImageView.widthAnchor.constraint(equalToConstant: 43).isActive = true
        addit1ImageView.topAnchor.constraint(equalTo: additView1.topAnchor, constant: 12).isActive = true
        addit1ImageView.trailingAnchor.constraint(equalTo: additView1.trailingAnchor, constant: -10).isActive = true
        
        addit1Label.text = "Block\nAdult Sites"
        addit1Label.translatesAutoresizingMaskIntoConstraints = false
        addit1Label.textColor = Color.title.ui
        addit1Label.font = Font.bold.font(size: 14)
        addit1Label.numberOfLines = 3
        additView1.addSubview(addit1Label)
        addit1Label.heightAnchor.constraint(equalToConstant: 51).isActive = true
        addit1Label.leadingAnchor.constraint(equalTo: additView1.leadingAnchor, constant: 12).isActive = true
        addit1Label.topAnchor.constraint(equalTo: additView1.topAnchor, constant: 10).isActive = true
        addit1Label.trailingAnchor.constraint(equalTo: addit1ImageView.leadingAnchor, constant: -5).isActive = true
        
        addit1SubLabel.text = "Make browsing safe\nfor children"
        addit1SubLabel.translatesAutoresizingMaskIntoConstraints = false
        addit1SubLabel.textColor = Color.title.ui
        addit1SubLabel.font = Font.medium.font(size: 14)
        addit1SubLabel.numberOfLines = 2
        additView1.addSubview(addit1SubLabel)
        addit1SubLabel.leadingAnchor.constraint(equalTo: addit1Label.leadingAnchor).isActive = true
        addit1SubLabel.topAnchor.constraint(equalTo: addit1Label.bottomAnchor, constant: 2).isActive = true
        addit1SubLabel.trailingAnchor.constraint(equalTo: additView1.trailingAnchor, constant: -10).isActive = true
        addit1SubLabel.bottomAnchor.constraint(equalTo: additView1.bottomAnchor, constant: -12).isActive = true
    }
    
    private func setAdditView2(){
        additView2.translatesAutoresizingMaskIntoConstraints = false
        additView2.isUserInteractionEnabled = true
        additView2.layer.cornerRadius = 16
        let gesture = UITapGestureRecognizer(target: self, action: #selector(addit2Action))
        additView2.addGestureRecognizer(gesture)
        scrollView.addSubview(additView2)
        additView2.heightAnchor.constraint(equalToConstant: 111).isActive = true
        additView2.widthAnchor.constraint(equalToConstant: (UIDevice.width-45)/2).isActive = true
        additView2.leadingAnchor.constraint(equalTo: additView1.trailingAnchor, constant: 9).isActive = true
        additView2.topAnchor.constraint(equalTo: additionalLabel.bottomAnchor, constant: 12).isActive = true
        
        addit2ImageView.image = "addit2Green".img()
        addit2ImageView.translatesAutoresizingMaskIntoConstraints = false
        additView2.addSubview(addit2ImageView)
        addit2ImageView.heightAnchor.constraint(equalToConstant: 43).isActive = true
        addit2ImageView.widthAnchor.constraint(equalToConstant: 43).isActive = true
        addit2ImageView.topAnchor.constraint(equalTo: additView2.topAnchor, constant: 12).isActive = true
        addit2ImageView.trailingAnchor.constraint(equalTo: additView2.trailingAnchor, constant: -10).isActive = true
        
        addit2Label.text = "Stop\nTracking\nScripts"
        addit2Label.translatesAutoresizingMaskIntoConstraints = false
        addit2Label.textColor = Color.title.ui
        addit2Label.font = Font.bold.font(size: 14)
        addit2Label.numberOfLines = 3
        addit2Label.adjustsFontSizeToFitWidth = true
        additView2.addSubview(addit2Label)
        addit2Label.heightAnchor.constraint(equalToConstant: 51).isActive = true
        addit2Label.leadingAnchor.constraint(equalTo: additView2.leadingAnchor, constant: 12).isActive = true
        addit2Label.topAnchor.constraint(equalTo: additView2.topAnchor, constant: 10).isActive = true
        addit2Label.trailingAnchor.constraint(equalTo: addit2ImageView.leadingAnchor, constant: -5).isActive = true
        
        addit2SubLabel.text = "Make your search\nprivate"
        addit2SubLabel.translatesAutoresizingMaskIntoConstraints = false
        addit2SubLabel.textColor = Color.title.ui
        addit2SubLabel.font = Font.medium.font(size: 14)
        addit2SubLabel.numberOfLines = 2
        additView2.addSubview(addit2SubLabel)
        addit2SubLabel.leadingAnchor.constraint(equalTo: addit2Label.leadingAnchor).isActive = true
        addit2SubLabel.topAnchor.constraint(equalTo: addit2Label.bottomAnchor, constant: 2).isActive = true
        addit2SubLabel.trailingAnchor.constraint(equalTo: additView2.trailingAnchor, constant: -10).isActive = true
        addit2SubLabel.bottomAnchor.constraint(equalTo: additView2.bottomAnchor, constant: -12).isActive = true
    }
    
    private func setAdditView3(){
        additView3.translatesAutoresizingMaskIntoConstraints = false
        additView3.isUserInteractionEnabled = true
        additView3.layer.cornerRadius = 16
        let gesture = UITapGestureRecognizer(target: self, action: #selector(addit3Action))
        additView3.addGestureRecognizer(gesture)
        scrollView.addSubview(additView3)
        additView3.heightAnchor.constraint(equalToConstant: 111).isActive = true
        additView3.widthAnchor.constraint(equalToConstant: (UIDevice.width-45)/2).isActive = true
        additView3.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 18).isActive = true
        additView3.topAnchor.constraint(equalTo: additView1.bottomAnchor, constant: 9).isActive = true
        
        addit3ImageView.image = "addit3Green".img()
        addit3ImageView.translatesAutoresizingMaskIntoConstraints = false
        additView3.addSubview(addit3ImageView)
        addit3ImageView.heightAnchor.constraint(equalToConstant: 43).isActive = true
        addit3ImageView.widthAnchor.constraint(equalToConstant: 43).isActive = true
        addit3ImageView.topAnchor.constraint(equalTo: additView3.topAnchor, constant: 12).isActive = true
        addit3ImageView.trailingAnchor.constraint(equalTo: additView3.trailingAnchor, constant: -10).isActive = true
        
        addit3Label.text = "Block\nMalvare"
        addit3Label.translatesAutoresizingMaskIntoConstraints = false
        addit3Label.textColor = Color.title.ui
        addit3Label.font = Font.bold.font(size: 14)
        addit3Label.numberOfLines = 3
        additView3.addSubview(addit3Label)
        addit3Label.heightAnchor.constraint(equalToConstant: 51).isActive = true
        addit3Label.leadingAnchor.constraint(equalTo: additView3.leadingAnchor, constant: 12).isActive = true
        addit3Label.topAnchor.constraint(equalTo: additView3.topAnchor, constant: 10).isActive = true
        addit3Label.trailingAnchor.constraint(equalTo: addit3ImageView.leadingAnchor, constant: -5).isActive = true
        
        addit3SubLabel.text = "Block pagas that\ncause harm"
        addit3SubLabel.translatesAutoresizingMaskIntoConstraints = false
        addit3SubLabel.textColor = Color.title.ui
        addit3SubLabel.font = Font.medium.font(size: 14)
        addit3SubLabel.numberOfLines = 2
        additView3.addSubview(addit3SubLabel)
        addit3SubLabel.leadingAnchor.constraint(equalTo: addit3Label.leadingAnchor).isActive = true
        addit3SubLabel.topAnchor.constraint(equalTo: addit3Label.bottomAnchor, constant: 2).isActive = true
        addit3SubLabel.trailingAnchor.constraint(equalTo: additView3.trailingAnchor, constant: -10).isActive = true
        addit3SubLabel.bottomAnchor.constraint(equalTo: additView3.bottomAnchor, constant: -12).isActive = true
    }
    
    private func setAdditView4(){
        additView4.translatesAutoresizingMaskIntoConstraints = false
        additView4.isUserInteractionEnabled = true
        additView4.layer.cornerRadius = 16
        let gesture = UITapGestureRecognizer(target: self, action: #selector(addit4Action))
        additView4.addGestureRecognizer(gesture)
        scrollView.addSubview(additView4)
        additView4.heightAnchor.constraint(equalToConstant: 111).isActive = true
        additView4.widthAnchor.constraint(equalToConstant: (UIDevice.width-45)/2).isActive = true
        additView4.leadingAnchor.constraint(equalTo: additView3.trailingAnchor, constant: 9).isActive = true
        additView4.centerYAnchor.constraint(equalTo: additView3.centerYAnchor).isActive = true
        
        addit4ImageView.image = "addit4Green".img()
        addit4ImageView.translatesAutoresizingMaskIntoConstraints = false
        additView4.addSubview(addit4ImageView)
        addit4ImageView.heightAnchor.constraint(equalToConstant: 43).isActive = true
        addit4ImageView.widthAnchor.constraint(equalToConstant: 43).isActive = true
        addit4ImageView.topAnchor.constraint(equalTo: additView4.topAnchor, constant: 12).isActive = true
        addit4ImageView.trailingAnchor.constraint(equalTo: additView4.trailingAnchor, constant: -10).isActive = true
        
        addit4Label.text = "Hide\nCookies\nPrompts"
        addit4Label.translatesAutoresizingMaskIntoConstraints = false
        addit4Label.textColor = Color.title.ui
        addit4Label.font = Font.bold.font(size: 14)
        addit4Label.numberOfLines = 3
        addit4Label.adjustsFontSizeToFitWidth = true
        additView4.addSubview(addit4Label)
        addit4Label.heightAnchor.constraint(equalToConstant: 51).isActive = true
        addit4Label.leadingAnchor.constraint(equalTo: additView4.leadingAnchor, constant: 12).isActive = true
        addit4Label.topAnchor.constraint(equalTo: additView4.topAnchor, constant: 10).isActive = true
        addit4Label.trailingAnchor.constraint(equalTo: addit4ImageView.leadingAnchor, constant: -5).isActive = true
        
        addit4SubLabel.text = "Remove annoying\nbanners"
        addit4SubLabel.translatesAutoresizingMaskIntoConstraints = false
        addit4SubLabel.textColor = Color.title.ui
        addit4SubLabel.font = Font.medium.font(size: 14)
        addit4SubLabel.numberOfLines = 2
        additView4.addSubview(addit4SubLabel)
        addit4SubLabel.leadingAnchor.constraint(equalTo: addit4Label.leadingAnchor).isActive = true
        addit4SubLabel.topAnchor.constraint(equalTo: addit4Label.bottomAnchor, constant: 2).isActive = true
        addit4SubLabel.trailingAnchor.constraint(equalTo: additView4.trailingAnchor, constant: -10).isActive = true
        addit4SubLabel.bottomAnchor.constraint(equalTo: additView4.bottomAnchor, constant: -12).isActive = true
    }
}


extension ViewController: AdManagerDelegate{
    func adDidDismissFullScreenContent(isEarn: Bool) {
        didShowPurchase?()
    }
    
    func adDidPresentFullScreenContent() {}
    
    func didFailToPresentFullScreenContentWithError() {
        didShowPurchase?()
    }
}
