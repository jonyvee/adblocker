//
//  PurchaseVC.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 20.01.2022.
//

import UIKit
import Adapty
import SwiftSpinner
import SafariServices
import SnapKit

class PurchaseVC: UIViewController, JonyMessage{
    
    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        let height = UIDevice.width * (0.664) + 700
        scrollView.contentSize = CGSize(width: UIDevice.width, height: height)
        return scrollView
    }()
    
    private let titleLabel = UILabel()
    private let closeBtn = UIButton()
    private var collectionView: UICollectionView?
    private let stackView = UIStackView()
    private var segmentsRef: [UIView] = []
    private var segmentsConstraintsRef: [NSLayoutConstraint] = []
    private let itemsCenteringView = UIView()
    
    private let itemsImageView: UIImageView = {
        let imageView = UIImageView(image: "premiumFeatures".img())
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private let priceView = UIView()
    private let totalPriceLabel = UILabel()
    private let priceLabel = UILabel()
    private let currencyLabel = UILabel()
    private let weakLabel = UILabel()
    
    private let priceView2 = UIView()
    private let totalPriceLabel2 = UILabel()
    private let priceLabel2 = UILabel()
    private let currencyLabel2 = UILabel()
    private let weakLabel2 = UILabel()
    
    private let bottomStackView = UIStackView()

    private let bottomLabel: UILabel = {
        let label = UILabel()
        label.text = "Information about products not manufactured by Apple, or independent websites not controlled or tested by Apple, is provided without recommendation or endorsement. Apple assumes no responsibility with regard to the selection, performance, or use of thirdparty websites or products. Apple makes no representations regarding thirdparty website accuracy or reliability. Risks are inherent in the use of the Internet. Contact the vendor for additional information. Other company and product names may be trademarks of their respective owners."
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = UIFont(name: "Montserrat-Regular", size: 12)
        label.textColor = UIColor(red: 0.165, green: 0.188, blue: 0.22, alpha: 1)
        return label
    }()

    // MARK: -

    private var isViewDidAppeared = false
    
    var didClose: (() -> ())?
    var didPurchase: (() -> ())?
    
    private var choosedBoard: Int = 0{
        didSet{
            updateAfterScroll()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // ui
        self.view.backgroundColor = .white
        setScrollView()
        setCloseBtn()
        setTitleLabel()
        setCollectionView()
        setStackView()
        setItemsView()
        setPriceView()
        setPriceView2()
        setBottomStackView()
        setBottomLabel()
        
        totalPriceLabel2.textColor = .white
        priceLabel2.textColor = .white
        currencyLabel2.textColor = .white
        weakLabel2.textColor = .white
        totalPriceLabel.textColor = UIColor(rgb: 0x2A3038)
        priceLabel.textColor = UIColor(rgb: 0x2A3038)
        currencyLabel.textColor = UIColor(rgb: 0x2A3038)
        weakLabel.textColor = UIColor(rgb: 0x2A3038)
        
        // purchases loading if needed
        loadPurchases()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if isViewDidAppeared == false{
            let _ = titleLabel.applyGradientWith(startColor: UIColor(rgb: 0x67DD7F), endColor: UIColor(rgb: 0xFFBC63))
            
            priceView.gradientBorder(width: 2, colors: [Color.green2.ui,Color.mainGreen2.ui], startPoint: .init(x: 0, y: 0.5), endPoint: .init(x: 1, y: 0.5), andRoundCornersWithRadius: 16)
            
            priceView2.applyGradient(colours: [UIColor(rgb: 0x00A469), UIColor(rgb: 0x67DD7F)], locations: [0,1], startPoint: CGPoint(x: 0, y: 0.5), endPoint: CGPoint(x: 1, y: 0.5), cornerRadius: 16)
            
            let height = bottomLabel.frame.origin.y + bottomLabel.frame.size.height + 20
            scrollView.contentSize = CGSize(width: UIDevice.width, height: height)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        isViewDidAppeared = true
    }
    
    
    // MARK: - Actions
    @objc private func closeAction(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "closePurchaseAction", params: [:])
        didClose?()
    }
    
    @objc private func priceView1Action(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "payPurchaseAction", params: [:])
        guard let product = PayManager.shared.getProduct(.week) else {return}
        
        SwiftSpinner.show("Just a second")
        callCNVibration(style: .soft)
        
        priceView.removeGradient()
        priceView.removeBorderGradient()
        priceView2.removeGradient()
        priceView2.removeBorderGradient()
        
        priceView.applyGradient(colours: [UIColor(rgb: 0x00A469), UIColor(rgb: 0x67DD7F)], locations: [0,1], startPoint: CGPoint(x: 0, y: 0.5), endPoint: CGPoint(x: 1, y: 0.5), cornerRadius: 16)
        
        priceView2.gradientBorder(width: 2, colors: [Color.green2.ui,Color.mainGreen2.ui], startPoint: .init(x: 0, y: 0.5), endPoint: .init(x: 1, y: 0.5), andRoundCornersWithRadius: 16)
        
        totalPriceLabel.textColor = .white
        priceLabel.textColor = .white
        currencyLabel.textColor = .white
        weakLabel.textColor = .white
        
        totalPriceLabel2.textColor = UIColor(rgb: 0x2A3038)
        priceLabel2.textColor = UIColor(rgb: 0x2A3038)
        currencyLabel2.textColor = UIColor(rgb: 0x2A3038)
        weakLabel2.textColor = UIColor(rgb: 0x2A3038)
        
        Adapty.makePurchase(product: product) { purchaserInfo, receipt, appleValidationResult, product, error in
            DispatchQueue.main.async {
                SwiftSpinner.hide()
            }
            
            if error != nil{
                DispatchQueue.main.async {self.message(text: "Something went wrong, try again")}
                AnalyticsManager.shared.reportEvent(vc: self, name: "purchaseCanceled", params: [:])
                return
            }
            
            AnalyticsManager.shared.logSubscribeEvent(orderId: UIDevice.getId(), currency: product?.currencyCode ?? "", price: product?.skProduct?.price.doubleValue ?? 0, contentId: product?.vendorProductId ?? "", duration: "week")
            User.shared.isPurchased = true

            DispatchQueue.main.async {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RELOAD_CONTROL"), object: nil)
                self.didPurchase?()
            }
        }
    }
    
    @objc private func priceView2Action(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "payPurchaseAction", params: [:])
        guard let product = PayManager.shared.getProduct(.month) else {return}
        
        SwiftSpinner.show("Just a second")
        callCNVibration(style: .soft)
        
        priceView.removeGradient()
        priceView.removeBorderGradient()
        priceView2.removeGradient()
        priceView2.removeBorderGradient()
        
        priceView.gradientBorder(width: 2, colors: [Color.green2.ui,Color.mainGreen2.ui], startPoint: .init(x: 0, y: 0.5), endPoint: .init(x: 1, y: 0.5), andRoundCornersWithRadius: 16)
        
        priceView2.applyGradient(colours: [UIColor(rgb: 0x00A469), UIColor(rgb: 0x67DD7F)], locations: [0,1], startPoint: CGPoint(x: 0, y: 0.5), endPoint: CGPoint(x: 1, y: 0.5), cornerRadius: 16)
        
        totalPriceLabel2.textColor = .white
        priceLabel2.textColor = .white
        currencyLabel2.textColor = .white
        weakLabel2.textColor = .white
        
        totalPriceLabel.textColor = UIColor(rgb: 0x2A3038)
        priceLabel.textColor = UIColor(rgb: 0x2A3038)
        currencyLabel.textColor = UIColor(rgb: 0x2A3038)
        weakLabel.textColor = UIColor(rgb: 0x2A3038)
        
        Adapty.makePurchase(product: product) { purchaserInfo, receipt, appleValidationResult, product, error in
            DispatchQueue.main.async {
                SwiftSpinner.hide()
            }
            
            if error != nil{
                DispatchQueue.main.async {self.message(text: "Something went wrong, try again")}
                AnalyticsManager.shared.reportEvent(vc: self, name: "purchaseCanceled", params: [:])
                return
            }
            
            AnalyticsManager.shared.logSubscribeEvent(orderId: UIDevice.getId(), currency: product?.currencyCode ?? "", price: product?.skProduct?.price.doubleValue ?? 0, contentId: product?.vendorProductId ?? "", duration: "month")
            User.shared.isPurchased = true

            DispatchQueue.main.async {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RELOAD_CONTROL"), object: nil)
                self.didPurchase?()
            }
        }
    }
    
    @objc private func restoreAction(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "restorePurchaseAction", params: [:])
        SwiftSpinner.show("Restoring purchases")
        Adapty.restorePurchases { purchaserInfo, receipt, appleValidationResult, error in
            DispatchQueue.main.async {
                SwiftSpinner.hide()
            }
            
            if let error = error {
                DispatchQueue.main.async {
                    self.message(text: error.localizedDescription)
                }
            }
            
            User.shared.isPurchased = true

            DispatchQueue.main.async {
                self.didPurchase?()
            }
        }
    }
    
    @objc private func policyAction(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "policyPurchaseAction", params: [:])
        if let url = URL(string: "https://clck.ru/amufY") {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
    }
    
    @objc private func termsAction(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "termsPurchaseAction", params: [:])
        if let url = URL(string: "https://clck.ru/amuez") {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
    }
    
    
    // MARK: - Funcionality
    private func loadPurchases(){
        PayManager.shared.loadProducts { _ in
            self.setAfterLoadPurchaseUI()
        }
    }
    
    private func setAfterLoadPurchaseUI(){
        guard let product1 = PayManager.shared.getProduct(.week) else {return}
        guard let product2 = PayManager.shared.getProduct(.month) else {return}
        
        totalPriceLabel.text = "Total price \(product1.localizedPrice ?? "")"
        priceLabel.text = product1.skProduct?.price.description
        currencyLabel.text = "\(product1.skProduct?.priceLocale.currencySymbol ?? "")/Weak"
        
        totalPriceLabel2.text = "Total price \(product2.localizedPrice ?? "")"
        priceLabel2.text = product2.skProduct?.price.description
        currencyLabel2.text = "\(product2.skProduct?.priceLocale.currencySymbol ?? "")/Month"
    }
    
    private func updateAfterScroll(){
        segmentsRef.enumerated().forEach { element in
            let isChoosed = choosedBoard == element.offset
            segmentsConstraintsRef[element.offset].constant = isChoosed ? 48 : 16
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn) {
                self.stackView.layoutIfNeeded()
                element.element.backgroundColor = isChoosed ? UIColor(red: 0.118, green: 0.839, blue: 0.579, alpha: 1) : UIColor(red: 0.165, green: 0.188, blue: 0.22, alpha: 0.6)
            } completion: { completed in }
        }
    }
    
    private func createItemView(item: PurchaseData.Item) -> UIView{
        let result = UIView()
        result.translatesAutoresizingMaskIntoConstraints = false
        priceView.addSubview(result)
        
        let imageView = UIImageView(image: item.image)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        result.addSubview(imageView)
        imageView.heightAnchor.constraint(equalToConstant: 48).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 48).isActive = true
        imageView.leadingAnchor.constraint(equalTo: result.leadingAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: result.topAnchor).isActive = true
        
        let titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.text = item.title
        titleLabel.font = Font.bold.font(size: 20)
        titleLabel.textColor = Color.title.ui
        titleLabel.adjustsFontSizeToFitWidth = true
        result.addSubview(titleLabel)
        titleLabel.heightAnchor.constraint(equalToConstant: 24).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 8).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: result.trailingAnchor, constant: -18).isActive = true
        titleLabel.topAnchor.constraint(equalTo: result.topAnchor).isActive = true
        
        let subtitleLabel = UILabel()
        subtitleLabel.translatesAutoresizingMaskIntoConstraints = false
        subtitleLabel.text = item.subtitle
        subtitleLabel.font = Font.regular.font(size: 16)
        subtitleLabel.textColor = Color.title.ui
        subtitleLabel.adjustsFontSizeToFitWidth = true
        result.addSubview(subtitleLabel)
        subtitleLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        subtitleLabel.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 8).isActive = true
        subtitleLabel.trailingAnchor.constraint(equalTo: result.trailingAnchor, constant: -18).isActive = true
        subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 6).isActive = true
        
        return result
    }
    
    
    // MARK: - UI
    private func setScrollView(){
        view.addSubview(scrollView)
        scrollView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    private func setCloseBtn(){
        scrollView.addSubview(closeBtn)
        closeBtn.setImage("close".img(), for: .normal)
        closeBtn.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        
        closeBtn.snp.makeConstraints { make in
            make.width.height.equalTo(44)
            make.leading.equalToSuperview().inset(UIDevice.width-44-8)
            make.top.equalToSuperview().inset(16)
        }
    }
    
    private func setTitleLabel(){
        scrollView.addSubview(titleLabel)
        titleLabel.modifyTitle(text: "Premium Protect")
        titleLabel.textColor = UIColor(red: 0.118, green: 0.839, blue: 0.579, alpha: 1)
        titleLabel.heightAnchor.constraint(equalToConstant: 29).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 18).isActive = true
        titleLabel.centerYAnchor.constraint(equalTo: closeBtn.centerYAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: closeBtn.leadingAnchor, constant: -8).isActive = true
    }
    
    private func setCollectionView(){
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView?.translatesAutoresizingMaskIntoConstraints = false
        collectionView?.backgroundColor = .clear
        collectionView?.showsVerticalScrollIndicator = false
        collectionView?.showsHorizontalScrollIndicator = false
        collectionView?.isPagingEnabled = true
        
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets.init(top: 0, left: 18, bottom: 0, right: 18)
        layout.itemSize = CGSize(width: UIDevice.width, height: 68)
        layout.minimumLineSpacing = 0
        collectionView?.setCollectionViewLayout(layout, animated: true)
        collectionView?.dataSource = self
        collectionView?.delegate = self
        
        self.scrollView.addSubview(collectionView!)
        collectionView?.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        collectionView?.widthAnchor.constraint(equalToConstant: UIDevice.width).isActive = true
        collectionView?.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 16).isActive = true
        collectionView?.heightAnchor.constraint(equalToConstant: 69).isActive = true
        
        // register cell
        collectionView?.register(PurchaseCell.self, forCellWithReuseIdentifier: PurchaseCell.key)
    }
    
    private func setStackView(){
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.spacing = 8
        scrollView.addSubview(stackView)
        stackView.heightAnchor.constraint(equalToConstant: 4).isActive = true
        stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 18).isActive = true
        stackView.topAnchor.constraint(equalTo: collectionView!.bottomAnchor, constant: 14).isActive = true
        
        for i in 0..<3{
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.layer.cornerRadius = 2
            stackView.addArrangedSubview(view)
            segmentsRef.append(view)
            
            if i == 0{
                let constraint = view.widthAnchor.constraint(equalToConstant: 48)
                segmentsConstraintsRef.append(constraint)
                constraint.isActive = true
                view.backgroundColor = UIColor(red: 0.118, green: 0.839, blue: 0.579, alpha: 1)
            }else{
                let constraint = view.widthAnchor.constraint(equalToConstant: 16)
                segmentsConstraintsRef.append(constraint)
                constraint.isActive = true
                view.backgroundColor = UIColor(red: 0.165, green: 0.188, blue: 0.22, alpha: 0.6)
            }
        }
    }
    
    private func setBottomStackView(){
        bottomStackView.translatesAutoresizingMaskIntoConstraints = false
        bottomStackView.axis = .horizontal
        bottomStackView.distribution = .fillProportionally
        bottomStackView.alignment = .fill
        bottomStackView.spacing = 3
        scrollView.addSubview(bottomStackView)
        bottomStackView.heightAnchor.constraint(equalToConstant: 18).isActive = true
        bottomStackView.topAnchor.constraint(equalTo: priceView2.bottomAnchor, constant: 24).isActive = true
        bottomStackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 18).isActive = true
        bottomStackView.widthAnchor.constraint(equalToConstant: UIDevice.width-36).isActive = true
        
        let privacyBtn = UIButton()
        privacyBtn.setTitle("Privacy Policy", for: .normal)
        privacyBtn.addTarget(self, action: #selector(policyAction), for: .touchUpInside)
        privacyBtn.setTitleColor(UIColor(red: 0.118, green: 0.839, blue: 0.579, alpha: 1), for: .normal)
        privacyBtn.titleLabel?.font = Font.regular.font(size: 12)
        privacyBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        privacyBtn.titleLabel?.underlineText(color: UIColor(red: 0.118, green: 0.839, blue: 0.579, alpha: 1))
        bottomStackView.addArrangedSubview(privacyBtn)
        
        let imageView1 = UIImageView(image: "dot".img())
        imageView1.contentMode = .scaleAspectFit
        bottomStackView.addArrangedSubview(imageView1)
        
        let termsBtn = UIButton()
        termsBtn.setTitle("Terms of use", for: .normal)
        termsBtn.addTarget(self, action: #selector(termsAction), for: .touchUpInside)
        termsBtn.setTitleColor(UIColor(red: 0.118, green: 0.839, blue: 0.579, alpha: 1), for: .normal)
        termsBtn.titleLabel?.font = Font.regular.font(size: 12)
        termsBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        termsBtn.titleLabel?.underlineText(color: UIColor(red: 0.118, green: 0.839, blue: 0.579, alpha: 1))
        bottomStackView.addArrangedSubview(termsBtn)
        
        let imageView2 = UIImageView(image: "dot".img())
        imageView2.contentMode = .scaleAspectFit
        bottomStackView.addArrangedSubview(imageView2)
        
        let restoreBtn = UIButton()
        restoreBtn.setTitle("Restore Purchases", for: .normal)
        restoreBtn.addTarget(self, action: #selector(restoreAction), for: .touchUpInside)
        restoreBtn.setTitleColor(UIColor(red: 0.118, green: 0.839, blue: 0.579, alpha: 1), for: .normal)
        restoreBtn.titleLabel?.font = Font.regular.font(size: 12)
        restoreBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        restoreBtn.titleLabel?.underlineText(color: UIColor(red: 0.118, green: 0.839, blue: 0.579, alpha: 1))
        bottomStackView.addArrangedSubview(restoreBtn)
    }
    
    private func setPriceView(){
        priceView.translatesAutoresizingMaskIntoConstraints = false
        priceView.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer(target: self, action: #selector(priceView1Action))
        priceView.addGestureRecognizer(gesture)
        scrollView.addSubview(priceView)
        priceView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 18).isActive = true
        priceView.widthAnchor.constraint(equalToConstant: UIDevice.width-36).isActive = true
        priceView.heightAnchor.constraint(equalToConstant: 65).isActive = true
        priceView.topAnchor.constraint(equalTo: itemsImageView.bottomAnchor, constant: 23).isActive = true
        
        weakLabel.translatesAutoresizingMaskIntoConstraints = false
        weakLabel.text = "1 Weak"
        weakLabel.textColor = Color.title.ui
        weakLabel.font = Font.bold.font(size: 18)
        priceView.addSubview(weakLabel)
        weakLabel.heightAnchor.constraint(equalToConstant: 28).isActive = true
        weakLabel.leadingAnchor.constraint(equalTo: priceView.leadingAnchor, constant: 16).isActive = true
        weakLabel.topAnchor.constraint(equalTo: priceView.topAnchor, constant: 10).isActive = true
        
        totalPriceLabel.translatesAutoresizingMaskIntoConstraints = false
        totalPriceLabel.text = "Total price"
        totalPriceLabel.textColor = UIColor(red: 0.165, green: 0.188, blue: 0.22, alpha: 0.7)
        totalPriceLabel.font = Font.regular.font(size: 14)
        priceView.addSubview(totalPriceLabel)
        totalPriceLabel.heightAnchor.constraint(equalToConstant: 21).isActive = true
        totalPriceLabel.leadingAnchor.constraint(equalTo: weakLabel.leadingAnchor).isActive = true
        totalPriceLabel.bottomAnchor.constraint(equalTo: priceView.bottomAnchor, constant: -10).isActive = true
        
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        priceLabel.textColor = Color.title.ui
        priceLabel.textAlignment = .right
        priceLabel.font = Font.bold.font(size: 30)
        priceView.addSubview(priceLabel)
        priceLabel.heightAnchor.constraint(equalToConstant: 38).isActive = true
        priceLabel.trailingAnchor.constraint(equalTo: priceView.trailingAnchor, constant: -16).isActive = true
        priceLabel.topAnchor.constraint(equalTo: priceView.topAnchor, constant: 7).isActive = true
        
        currencyLabel.translatesAutoresizingMaskIntoConstraints = false
        currencyLabel.text = "$/Week"
        currencyLabel.textColor = Color.title.ui
        currencyLabel.textAlignment = .right
        currencyLabel.font = Font.medium.font(size: 12)
        priceView.addSubview(currencyLabel)
        currencyLabel.heightAnchor.constraint(equalToConstant: 15).isActive = true
        currencyLabel.trailingAnchor.constraint(equalTo: priceView.trailingAnchor, constant: -21).isActive = true
        currencyLabel.bottomAnchor.constraint(equalTo: priceView.bottomAnchor, constant: -7).isActive = true
    }
    
    private func setPriceView2(){
        priceView2.translatesAutoresizingMaskIntoConstraints = false
        priceView2.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer(target: self, action: #selector(priceView2Action))
        priceView2.addGestureRecognizer(gesture)
        scrollView.addSubview(priceView2)
        priceView2.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 18).isActive = true
        priceView2.widthAnchor.constraint(equalToConstant: UIDevice.width-36).isActive = true
        priceView2.heightAnchor.constraint(equalToConstant: 65).isActive = true
        priceView2.topAnchor.constraint(equalTo: priceView.bottomAnchor, constant: 16).isActive = true
        
        weakLabel2.translatesAutoresizingMaskIntoConstraints = false
        weakLabel2.text = "1 Month"
        weakLabel2.textColor = Color.title.ui
        weakLabel2.font = Font.bold.font(size: 18)
        priceView2.addSubview(weakLabel2)
        weakLabel2.heightAnchor.constraint(equalToConstant: 28).isActive = true
        weakLabel2.leadingAnchor.constraint(equalTo: priceView2.leadingAnchor, constant: 16).isActive = true
        weakLabel2.topAnchor.constraint(equalTo: priceView2.topAnchor, constant: 10).isActive = true
        
        totalPriceLabel2.translatesAutoresizingMaskIntoConstraints = false
        totalPriceLabel2.text = "Total price"
        totalPriceLabel2.textColor = UIColor(red: 0.165, green: 0.188, blue: 0.22, alpha: 0.7)
        totalPriceLabel2.font = Font.regular.font(size: 14)
        priceView2.addSubview(totalPriceLabel2)
        totalPriceLabel2.heightAnchor.constraint(equalToConstant: 21).isActive = true
        totalPriceLabel2.leadingAnchor.constraint(equalTo: weakLabel2.leadingAnchor).isActive = true
        totalPriceLabel2.bottomAnchor.constraint(equalTo: priceView2.bottomAnchor, constant: -10).isActive = true
        
        priceLabel2.translatesAutoresizingMaskIntoConstraints = false
        priceLabel2.textColor = Color.title.ui
        priceLabel2.textAlignment = .right
        priceLabel2.font = Font.bold.font(size: 30)
        priceView2.addSubview(priceLabel2)
        priceLabel2.heightAnchor.constraint(equalToConstant: 38).isActive = true
        priceLabel2.trailingAnchor.constraint(equalTo: priceView2.trailingAnchor, constant: -16).isActive = true
        priceLabel2.topAnchor.constraint(equalTo: priceView2.topAnchor, constant: 7).isActive = true
        
        currencyLabel2.translatesAutoresizingMaskIntoConstraints = false
        currencyLabel2.text = "$/Month"
        currencyLabel2.textColor = Color.title.ui
        currencyLabel2.textAlignment = .right
        currencyLabel2.font = Font.medium.font(size: 12)
        priceView2.addSubview(currencyLabel2)
        currencyLabel2.heightAnchor.constraint(equalToConstant: 15).isActive = true
        currencyLabel2.trailingAnchor.constraint(equalTo: priceView2.trailingAnchor, constant: -21).isActive = true
        currencyLabel2.bottomAnchor.constraint(equalTo: priceView2.bottomAnchor, constant: -7).isActive = true
    }
    
    private func setItemsView(){
        scrollView.addSubview(itemsImageView)
        itemsImageView.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.width.equalTo(UIDevice.width)
            make.height.equalTo(UIDevice.width*0.664)
            make.top.equalTo(stackView.snp.bottom).inset(-32)
        }
    }
    
    private func setBottomLabel(){
        scrollView.addSubview(bottomLabel)
        bottomLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(bottomStackView)
            make.top.equalTo(bottomStackView.snp.bottom).inset(-16)
        }
    }
}


extension PurchaseVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        choosedBoard = Int(scrollView.contentOffset.x)/Int(scrollView.frame.width)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return PurchaseData.textArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PurchaseCell.key, for: indexPath as IndexPath) as! PurchaseCell
        let text = PurchaseData.textArray[indexPath.row]
        cell.configure(text: text)
        return cell
    }
}
