import UIKit
import SwiftSpinner

class SplashVC: UIViewController {
        
    private var timer: Timer?
    
    var didShowMyboard: (() -> ())?
    var didShowOnboard: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        SwiftSpinner.show("Just a second")
        NotificationCenter.default.addObserver(self, selector: #selector(setView(notification:)), name: Notification.Name.init("SplashVC"), object: nil)
        
        User.shared.isFirstOpen = false
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(getOnMainPage), userInfo: nil, repeats: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
                
        if User.shared.myboard?.subscription != ""{
            setView(notification: NSNotification())
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        NotificationCenter.default.removeObserver(self)
        timer?.invalidate()
    }
    
    @objc private func getOnMainPage(){
        SwiftSpinner.hide()
        AnalyticsManager.shared.reportEvent(vc: self, name: "showBoardFromSplashAction", params: [:])
        didShowOnboard?()
    }
    
    @objc private func setView(notification: NSNotification){
        SwiftSpinner.hide()
        if User.shared.myboard?.subscription == "week"{
            AnalyticsManager.shared.reportEvent(vc: self, name: "showMyboardFromSplashAction", params: [:])
            didShowMyboard?()
        }else{
            AnalyticsManager.shared.reportEvent(vc: self, name: "showBoardFromSplashAction", params: [:])
            didShowOnboard?()
        }
    }
    
}
