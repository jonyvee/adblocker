//
//  BoardVC.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 22.01.2022.
//

import UIKit
import Adapty
import SwiftSpinner
import SafariServices
import GoogleMobileAds

class BoardVC: UIViewController, JonyMessage{
    
    private var bannerViewHeightConstraint: NSLayoutConstraint?
    private let bannerView = UIView()
    private var banner: GADBannerView?
    private let closeBtn = UIButton()
    private var collectionView: UICollectionView?
    private let collectionCenteringView = UIView()
    private let bottomStackView = UIStackView()
    private var nextBtn = UIButton()
    private let stackView = UIStackView()
    private var segmentsRef: [UIView] = []
    private var segmentsConstraintsRef: [NSLayoutConstraint] = []
    
    private var productPrice: String = ""
    private var choosedBoard: Int = 0{
        didSet{
            updateAfterScroll()
        }
    }
    
    var didShowTutorial: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateGADUI), name: NSNotification.Name.init(rawValue: "RELOAD_CONTROL"), object: nil)
        
        // ui
        self.view.backgroundColor = .white
        setBottomStackView()
        setNextBtn()
        setStackView()
        setBannerView()
        setCollectionView()
        setCloseBtn()
        setGAD()
        
        Panela.fetch()
        
        // load purchases
        loadPurchases()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func updateGADUI(){
        if Panela.canShowMarketing && User.shared.isPurchased == false && UIDevice.hasNotch == true{
            if banner == nil{
                setGAD()
            }else{
                banner?.isHidden = false
                bannerViewHeightConstraint?.constant = 50 + UIDevice.notchHeight
            }
        }else{
            banner?.isHidden = true
            bannerViewHeightConstraint?.constant = 1
        }
    }
    
    private func setGAD(){
        if User.shared.isPurchased == false &&
                Panela.canShowMarketing &&
                UIDevice.hasNotch == true{
            bannerViewHeightConstraint?.constant = 50 + UIDevice.notchHeight
            self.banner = GADBannerView(adSize: GADAdSizeBanner)
            banner?.translatesAutoresizingMaskIntoConstraints = false
            banner?.adUnitID = AdManager.Ids.banner1.rawValue
            banner?.rootViewController = self
            banner?.isAutoloadEnabled = true
            self.bannerView.addSubview(banner!)
            banner?.heightAnchor.constraint(equalToConstant: 50).isActive = true
            banner?.leadingAnchor.constraint(equalTo: bannerView.leadingAnchor).isActive = true
            banner?.trailingAnchor.constraint(equalTo: bannerView.trailingAnchor).isActive = true
            banner?.bottomAnchor.constraint(equalTo: bannerView.bottomAnchor).isActive = true
        }
    }
    
    private func setBannerView(){
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        bannerViewHeightConstraint = bannerView.heightAnchor.constraint(equalToConstant: 1)
        bannerViewHeightConstraint?.isActive = true
        bannerView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        bannerView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        bannerView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    
    // MARK: - Actions
    @objc private func closeAction(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "boardCloseAction", params: [:])
        didShowTutorial?()
    }
    
    @objc private func nextAction(){
        if User.shared.isPurchased == true && choosedBoard == BoardData.items.count-2{
            AnalyticsManager.shared.reportEvent(vc: self, name: "boardShowTutorialAction", params: [:])
            didShowTutorial?()
            return
        }
        
        if choosedBoard < BoardData.items.count-1{
            choosedBoard+=1
            AnalyticsManager.shared.reportEvent(vc: self, name: "boardNext\(choosedBoard)Action", params: [:])
        }else{
            
            AnalyticsManager.shared.reportEvent(vc: self, name: "boardPayAction", params: [:])
            
            guard let product = PayManager.shared.getProduct(.week) else {return}
            SwiftSpinner.show("Just a second")
            Adapty.makePurchase(product: product) { purchaserInfo, receipt, appleValidationResult, product, error in
                DispatchQueue.main.async {
                    SwiftSpinner.hide()
                }
                
                if error != nil{
                    DispatchQueue.main.async {self.message(text: "Something went wrong, try again")}
                    AnalyticsManager.shared.reportEvent(vc: self, name: "purchaseCanceled", params: [:])
                    return
                }
                
                AnalyticsManager.shared.logSubscribeEvent(orderId: UIDevice.getId(), currency: product?.currencyCode ?? "", price: product?.skProduct?.price.doubleValue ?? 0, contentId: product?.vendorProductId ?? "", duration: "week")
                User.shared.isPurchased = true

                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RELOAD_CONTROL"), object: nil)
                    self.didShowTutorial?()
                }
            }
        }
    }
    
    @objc private func restoreAction(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "boardRestoreAction", params: [:])
        SwiftSpinner.show("Restoring purchases")
        
        Adapty.restorePurchases { purchaserInfo, receipt, appleValidationResult, error in
            DispatchQueue.main.async {
                SwiftSpinner.hide()
            }
            
            if let error = error {
                DispatchQueue.main.async {
                    self.message(text: error.localizedDescription)
                }
            }
            
            User.shared.isPurchased = true

            DispatchQueue.main.async {
                self.didShowTutorial?()
            }
        }
    }
    
    @objc private func policyAction(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "boardPolicyAction", params: [:])
        if let url = URL(string: "https://clck.ru/amufY") {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
    }
    
    @objc private func termsAction(){
        AnalyticsManager.shared.reportEvent(vc: self, name: "boardTermsAction", params: [:])
        if let url = URL(string: "https://clck.ru/amuez") {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
    }
    
    
    
    // MARK: - Funcionality
    private func loadPurchases(){
        PayManager.shared.loadProducts { _ in
            guard let product = PayManager.shared.getProduct(.week) else {return}
            self.productPrice = product.localizedPrice ?? ""
        }
    }
    
    private func updateAfterScroll(){
        self.collectionView?.scrollToItem(at: IndexPath(row: choosedBoard, section: 0), at: .centeredHorizontally, animated: true)
        
        if choosedBoard < BoardData.items.count-1{
            bottomStackView.isHidden = true
            bannerView.isHidden = false
            nextBtn.setTitle("Next", for: .normal)
            closeBtn.isHidden = true
        }else{
            bottomStackView.isHidden = false
            bannerView.isHidden = true
            nextBtn.setTitle("Try now", for: .normal)
            closeBtn.isHidden = false
        }
        
        segmentsRef.enumerated().forEach { element in
            let isChoosed = choosedBoard == element.offset
            segmentsConstraintsRef[element.offset].constant = isChoosed ? 48 : 16
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn) {
                self.stackView.layoutIfNeeded()
                element.element.backgroundColor = isChoosed ? UIColor(red: 0.118, green: 0.839, blue: 0.579, alpha: 1) : UIColor(red: 0.165, green: 0.188, blue: 0.22, alpha: 0.6)
            } completion: { completed in }
        }
    }
    
    
    // MARK: - UI
    private func setCloseBtn(){
        view.addSubview(closeBtn)
        closeBtn.isHidden = true
        closeBtn.translatesAutoresizingMaskIntoConstraints = false
        let image = "close".img()!.withRenderingMode(.alwaysTemplate)
        closeBtn.setImage(image, for: .normal)
        closeBtn.imageView?.tintColor = UIColor(red: 0.843, green: 0.843, blue: 0.843, alpha: 1)
        closeBtn.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        closeBtn.heightAnchor.constraint(equalToConstant: 44).isActive = true
        closeBtn.widthAnchor.constraint(equalToConstant: 44).isActive = true
        closeBtn.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16).isActive = true
        closeBtn.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8).isActive = true
    }
    
    private func setCollectionView(){
        collectionCenteringView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(collectionCenteringView)
        collectionCenteringView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        collectionCenteringView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        collectionCenteringView.topAnchor.constraint(equalTo: bannerView.bottomAnchor).isActive = true
        collectionCenteringView.bottomAnchor.constraint(equalTo: stackView.topAnchor).isActive = true
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView?.translatesAutoresizingMaskIntoConstraints = false
        collectionView?.backgroundColor = .clear
        collectionView?.showsVerticalScrollIndicator = false
        collectionView?.showsHorizontalScrollIndicator = false
        collectionView?.isPagingEnabled = true
        
        let width = UIDevice.width
        let height = UIDevice.width + (UIDevice.hasNotch ? 49 : 20) + 29 + 10 + 75
    
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: width, height: height)
        layout.minimumLineSpacing = 0
        collectionView?.setCollectionViewLayout(layout, animated: true)
        collectionView?.dataSource = self
        collectionView?.delegate = self
        
        collectionCenteringView.addSubview(collectionView!)
        collectionView?.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        collectionView?.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        collectionView?.centerYAnchor.constraint(equalTo: collectionCenteringView.centerYAnchor).isActive = true
        collectionView?.heightAnchor.constraint(equalToConstant: height+1).isActive = true
        
        // register cell
        collectionView?.register(BoardCell.self, forCellWithReuseIdentifier: BoardCell.key)
    }
    
    private func setBottomStackView(){
        bottomStackView.translatesAutoresizingMaskIntoConstraints = false
        bottomStackView.isHidden = true
        bottomStackView.axis = .horizontal
        bottomStackView.distribution = .fillProportionally
        bottomStackView.alignment = .fill
        bottomStackView.spacing = 3
        view.addSubview(bottomStackView)
        bottomStackView.heightAnchor.constraint(equalToConstant: 18).isActive = true
        bottomStackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -8).isActive = true
        bottomStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 18).isActive = true
        bottomStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -18).isActive = true
        
        let privacyBtn = UIButton()
        privacyBtn.setTitle("Privacy Policy", for: .normal)
        privacyBtn.addTarget(self, action: #selector(policyAction), for: .touchUpInside)
        privacyBtn.setTitleColor(.black, for: .normal)
        privacyBtn.titleLabel?.font = Font.regular.font(size: 12)
        privacyBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        privacyBtn.titleLabel?.underlineText(color: .black)
        bottomStackView.addArrangedSubview(privacyBtn)
        
        let imageView1 = UIImageView(image: "dot".img())
        imageView1.contentMode = .scaleAspectFit
        bottomStackView.addArrangedSubview(imageView1)
        
        let termsBtn = UIButton()
        termsBtn.setTitle("Terms of use", for: .normal)
        termsBtn.addTarget(self, action: #selector(termsAction), for: .touchUpInside)
        termsBtn.setTitleColor(.black, for: .normal)
        termsBtn.titleLabel?.font = Font.regular.font(size: 12)
        termsBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        termsBtn.titleLabel?.underlineText(color: .black)
        bottomStackView.addArrangedSubview(termsBtn)
        
        let imageView2 = UIImageView(image: "dot".img())
        imageView2.contentMode = .scaleAspectFit
        bottomStackView.addArrangedSubview(imageView2)
        
        let restoreBtn = UIButton()
        restoreBtn.setTitle("Restore Purchases", for: .normal)
        restoreBtn.addTarget(self, action: #selector(restoreAction), for: .touchUpInside)
        restoreBtn.setTitleColor(.black, for: .normal)
        restoreBtn.titleLabel?.font = Font.regular.font(size: 12)
        restoreBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        restoreBtn.titleLabel?.underlineText(color: .black)
        bottomStackView.addArrangedSubview(restoreBtn)
    }
    
    private func setNextBtn(){
        nextBtn.modify(title: "Next")
        nextBtn.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        view.addSubview(nextBtn)
        nextBtn.heightAnchor.constraint(equalToConstant: 52).isActive = true
        nextBtn.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 18).isActive = true
        nextBtn.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -18).isActive = true
        nextBtn.bottomAnchor.constraint(equalTo: bottomStackView.topAnchor, constant: UIDevice.hasNotch ? -16 : -8).isActive = true
    }
    
    private func setStackView(){
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.spacing = 8
        view.addSubview(stackView)
        stackView.heightAnchor.constraint(equalToConstant: 4).isActive = true
        stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: nextBtn.topAnchor, constant: -24).isActive = true
        
        let count = User.shared.isPurchased! ? BoardData.items.count-1 : BoardData.items.count
        for i in 0..<count{
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.layer.cornerRadius = 2
            stackView.addArrangedSubview(view)
            segmentsRef.append(view)
            
            if i == 0{
                let constraint = view.widthAnchor.constraint(equalToConstant: 48)
                segmentsConstraintsRef.append(constraint)
                constraint.isActive = true
                view.backgroundColor = UIColor(red: 0.118, green: 0.839, blue: 0.579, alpha: 1)
            }else{
                let constraint = view.widthAnchor.constraint(equalToConstant: 16)
                segmentsConstraintsRef.append(constraint)
                constraint.isActive = true
                view.backgroundColor = UIColor(red: 0.165, green: 0.188, blue: 0.22, alpha: 0.6)
            }
        }
    }
}


extension BoardVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        choosedBoard = Int(scrollView.contentOffset.x)/Int(scrollView.frame.width)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return User.shared.isPurchased! ? BoardData.items.count-1 : BoardData.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BoardCell.key, for: indexPath as IndexPath) as! BoardCell
        let item = BoardData.items[indexPath.row]
        let price = (indexPath.row == BoardData.items.count-1) ? productPrice : ""
        cell.configure(item: item, price: price)
        return cell
    }
}
