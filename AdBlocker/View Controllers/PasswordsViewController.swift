//
//  PasswordsViewController.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 26.03.2022.
//

import UIKit
import SnapKit
import RealmSwift

class PasswordsViewController: UIViewController, JonyMessage{
    
    // MARK: - Properties
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Password storage"
        label.textColor = UIColor(rgb: 0x2A3038)
        label.font = UIFont(name: "Montserrat-Bold", size: 24)
        return label
    }()
    
    private let selectView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 0.888, green: 0.992, blue: 0.967, alpha: 1)
        view.layer.cornerRadius = 16
        return view
    }()
    
    private lazy var allButton: UIButton = {
        let button = UIButton()
        button.setTitle("All", for: .normal)
        button.addTarget(self, action: #selector(allAction), for: .touchUpInside)
        button.titleLabel?.font = UIFont(name: "Montserrat-Bold", size: 16)
        button.layer.cornerRadius = 16
        return button
    }()
    
    private lazy var favoriteButton: UIButton = {
        let button = UIButton()
        button.setTitle("Favorite", for: .normal)
        button.addTarget(self, action: #selector(favoriteAction), for: .touchUpInside)
        button.titleLabel?.font = UIFont(name: "Montserrat-Bold", size: 16)
        button.layer.cornerRadius = 16
        return button
    }()
    
    private lazy var addBtn: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(addAction), for: .touchUpInside)
        button.setImage("addBtn".img(), for: .normal)
        return button
    }()
    
    private let emptyCentringView: UIView =  UIView()
    private let emptyImageView = UIImageView(image: "password_empty".img())
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.keyboardDismissMode = .onDrag
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets.init(top: 19, left: 16, bottom: 20, right: 16)
        layout.itemSize = CGSize(width: UIDevice.width-32, height: 100)
        layout.minimumLineSpacing = 16
        
        collectionView.setCollectionViewLayout(layout, animated: true)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(PasswordCell.self, forCellWithReuseIdentifier: PasswordCell.key)
        
        return collectionView
    }()
    
    // MARK: -
    private var isViewDidAppear = false
    private var isFavorite = false {
        didSet{
            if isFavorite {
                passwords = realm.objects(PasswordModel.self).where({$0.isFavorite == true})
            }else{
                passwords = realm.objects(PasswordModel.self)
            }
            
            self.updateEmptyViewUI()
            self.collectionView.reloadData()
        }
    }
    
    // MARK: -
    
    var didOpenPassword: ((PasswordModel?) -> ())?
    var didOpenPurchase: (() -> ())?
    
    // MARK: -
    
    private var passwords = realm.objects(PasswordModel.self)
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        setViews()
        setConstraints()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if isViewDidAppear == false{
            updateSelectionUI()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionView.reloadData()
        updateEmptyViewUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isViewDidAppear = true
    }
    
    // MARK: - Actions
    
    @objc private func allAction(){
        callCNVibration(style: .soft)
        isFavorite = false
        updateSelectionUI()
    }
    
    @objc private func favoriteAction(){
        callCNVibration(style: .soft)
        isFavorite = true
        updateSelectionUI()
    }
    
    @objc private func addAction(){
        if User.shared.isPurchased == false && passwords.count == 7{
            didOpenPurchase?()
        }else{
            didOpenPassword?(nil)
            
            if User.shared.isPurchased == false {
                if Panela.canShowMarketing == true && AdManager.shared.isReady(ad: .interstitial){
                    AdManager.shared.present(ad: .interstitial, root: self, callback: self)
                }
            }
        }
    }
    
    // MARK: - UI
    
    private func setViews(){
        view.addSubview(titleLabel)
        view.addSubview(selectView)
        selectView.addSubview(allButton)
        selectView.addSubview(favoriteButton)
        view.addSubview(emptyCentringView)
        emptyCentringView.addSubview(emptyImageView)
        view.addSubview(collectionView)
        view.addSubview(addBtn)
    }
    
    private func setConstraints(){
        titleLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(18)
            make.height.equalTo(29)
            make.top.equalTo(view.safeAreaLayoutGuide).inset(20)
        }
        
        selectView.snp.makeConstraints { make in
            make.height.equalTo(44)
            make.leading.trailing.equalToSuperview().inset(18)
            make.top.equalTo(titleLabel.snp.bottom).inset(-24)
        }

        allButton.snp.makeConstraints { make in
            make.leading.bottom.top.equalToSuperview().inset(2)
            make.width.equalTo(selectView).dividedBy(2.1)
        }

        favoriteButton.snp.makeConstraints { make in
            make.trailing.bottom.top.equalToSuperview().inset(2)
            make.width.equalTo(selectView).dividedBy(2.1)
        }
        
        emptyCentringView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide)
            make.top.equalTo(selectView.snp.bottom)
        }

        emptyImageView.snp.makeConstraints { make in
            make.width.equalTo(222)
            make.height.equalTo(267)
            make.center.equalToSuperview()
        }
        
        collectionView.snp.makeConstraints { make in
            make.leading.bottom.trailing.equalToSuperview()
            make.top.equalTo(selectView.snp.bottom).inset(-12)
        }
        
        addBtn.snp.makeConstraints { make in
            make.height.width.equalTo(52)
            make.trailing.equalToSuperview().inset(18)
            make.bottom.equalTo(view.safeAreaLayoutGuide).inset(24)
        }
    }
    
    // MARK: - Helper
    
    private func updateSelectionUI(){
        if isFavorite{
            allButton.removeGradient()
            favoriteButton.setTitleColor(.white, for: .normal)
            favoriteButton.applyGradient(colours: [UIColor(rgb: 0x1ED694),UIColor(rgb: 0x00A469)], locations: [0,1], startPoint: CGPoint(x: 0.5, y: 0), endPoint: CGPoint(x: 0.5, y: 1), cornerRadius: 16)
            allButton.backgroundColor = .clear
            allButton.setTitleColor(UIColor(rgb: 0x2A3038), for: .normal)
        }else{
            favoriteButton.removeGradient()
            allButton.setTitleColor(.white, for: .normal)
            allButton.applyGradient(colours: [UIColor(rgb: 0x1ED694),UIColor(rgb: 0x00A469)], locations: [0,1], startPoint: CGPoint(x: 0.5, y: 0), endPoint: CGPoint(x: 0.5, y: 1), cornerRadius: 16)
            favoriteButton.backgroundColor = .clear
            favoriteButton.setTitleColor(UIColor(rgb: 0x2A3038), for: .normal)
        }
    }
    
    private func updateEmptyViewUI(){
        emptyCentringView.isHidden = !passwords.isEmpty
    }
    
}

// MARK: - UICollectionViewDataSource

extension PasswordsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return passwords.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PasswordCell.key, for: indexPath as IndexPath) as! PasswordCell
        let password = self.passwords[indexPath.row]
        let presentable = PasswordCellPresentable(title: password.title, login: password.login, url: password.url, password: password.password, isPassHidden: false, isFavorite: password.isFavorite)
        
        cell.didCopy = {[weak self] in
            self?.message(text: "Password copied")
        }
        
        cell.configure(with: presentable)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let password = self.passwords[indexPath.row]
        guard let data = RealmHelper.DetachedCopy(of: password) else {return}
        didOpenPassword?(data)
    }
}

extension PasswordsViewController: AdManagerDelegate{
    func adDidDismissFullScreenContent(isEarn: Bool) {
    }
    
    func adDidPresentFullScreenContent() {
    }
    
    func didFailToPresentFullScreenContentWithError() {
    }
}
