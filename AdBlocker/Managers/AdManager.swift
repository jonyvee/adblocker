//
//  AdManager.swift
//  FairValue
//
//  Created by Nikola Cvetkovic on 11.11.2021.
//

import UIKit
import GoogleMobileAds
import AppTrackingTransparency



protocol AdManagerDelegate{
    func adDidDismissFullScreenContent(isEarn: Bool)
    func adDidPresentFullScreenContent()
    func didFailToPresentFullScreenContentWithError()
}


class AdManager: NSObject{
    
    static let shared = AdManager()
        
    private var interstitial: GADInterstitialAd?
    private var rewarded: GADRewardedAd?
    
    private var delegate: AdManagerDelegate?
    private (set) var isFullScreenAdVisible = false
    private var isEarn = false
    private var currentAdType: AdType?
    
    
    // MARK: - Inicialization
    func setupGAD(){
        if User.shared.isPurchased == true{return}
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = ["b8450761a66cea893362d3bd0f42fe5a/"]
        self.loadInterstitialAd()
    }
    
    

    func loadInterstitialAd(){
        self.interstitial = nil
        GADInterstitialAd.load(withAdUnitID: Ids.interstitial.rawValue, request: GADRequest()) { ad, error in
            if let error = error{
                print(error.localizedDescription)
            }else{
                self.interstitial = ad
                self.interstitial?.fullScreenContentDelegate = self
            }
        }
    }
    
    func isReady(ad type: AdType) -> Bool{
        if type == .interstitial{
            return interstitial != nil
        }else{
            return rewarded != nil
        }
    }
    
    
    func present(ad type: AdType, root: UIViewController, callback: AdManagerDelegate?){
        if isReady(ad: type) == false{return}
        isEarn = false
        currentAdType = type
        self.delegate = callback
        
        if type == .interstitial, let ad = self.interstitial{
            isEarn = true
            ad.present(fromRootViewController: root)
        }else if type == .reward, let ad = self.rewarded{
            ad.present(fromRootViewController: root) {
                self.isEarn = true
            }
        }
    }
}



// MARK: - GADFullScreenContentDelegate
extension AdManager: GADFullScreenContentDelegate{
    func adDidPresentFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        isFullScreenAdVisible = true
        delegate?.adDidPresentFullScreenContent()
    }

    func ad(_ ad: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error){
        print("Ad failed to present full screen content with error \(error.localizedDescription).")
        delegate?.didFailToPresentFullScreenContentWithError()
    }

    func adDidDismissFullScreenContent(_ ad: GADFullScreenPresentingAd) {
        if currentAdType == .interstitial{
            loadInterstitialAd()
        }
        
        isFullScreenAdVisible = false
        delegate?.adDidDismissFullScreenContent(isEarn: self.isEarn)
    }
}


// MARK: - Constraints
extension AdManager{
    
    enum Ids: String{
        case global = "ca-app-pub-5042957176547201~6210531032"
        case banner1 = "ca-app-pub-5042957176547201/3312904958"
        case banner2 = "ca-app-pub-5042957176547201/6565754253"
        case banner3 = "ca-app-pub-5042957176547201/3094764174"
        case banner4 = "ca-app-pub-5042957176547201/6650865800"
        case interstitial = "ca-app-pub-5042957176547201/2051794173"
    }
    
    enum AdType{
        case interstitial
        case reward
    }
}
