//
//  AnalyticManager.swift
//  secure
//
//  Created by Кирилл Блохин on 08.11.2021.
//

import UIKit
import AppsFlyerLib
import YandexMobileMetrica

class AnalyticsManager: NSObject{
    
    static let shared = AnalyticsManager()
    
    func reportEvent(vc: UIViewController, name: String, params: [String: Any]){
        var parameter = params
        parameter.updateValue(String(describing: type(of: vc)), forKey: "vc")
        AppsFlyerLib.shared().logEvent(name, withValues: params)
        reportEvent2(name: name, params: parameter)
    }
    
    func reportEvent2(name: String, params: [String: Any]){
        YMMYandexMetrica.reportEvent(name, parameters: params) { error in
            print(error.localizedDescription)
        }
    }
    
    func logSubscribeEvent(orderId: String, currency: String, price: Double, contentId: String, duration: String){
        AppsFlyerLib.shared().logEvent("purchase", withValues:[
            AFEventParamContentId: contentId,
            AFEventParamContentType : "week",
            AFEventParamContent : duration,
            AFEventParamReceiptId : orderId,
            AFEventParamRevenue: price,
            AFEventParamCurrency: currency
        ])
        
        reportEvent2(name: "purchaseEvent", params: ["orderId": orderId, "currency": currency, "price": price, "contentId": contentId, "duration": duration])
    }
}
