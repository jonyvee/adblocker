//
//  ImagePicker.swift
//  Raskraska2
//
//  Created by Nikola Cvetkovic on 25.06.2020.
//  Copyright © 2020 jonyvee. All rights reserved.
//

import UIKit

public protocol ImagePickerDelegate: AnyObject {
    func didSelect(image: UIImage?)
}

open class ImagePicker: NSObject {

    private var imagePicker = UIImagePickerController()
    
    private weak var presentationController: UIViewController?
    private weak var delegate: ImagePickerDelegate?

    public init(presentationController: UIViewController, delegate: ImagePickerDelegate) {
        self.imagePicker = UIImagePickerController()

        super.init()

        self.presentationController = presentationController
        self.delegate = delegate

        self.imagePicker.delegate = self
        self.imagePicker.allowsEditing = false
        self.imagePicker.mediaTypes = ["public.image"]
    }

    private func action(for type: UIImagePickerController.SourceType, title: String) -> UIAlertAction? {
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            return nil
        }

        return UIAlertAction(title: title, style: .default) { [unowned self] _ in
            self.imagePicker.sourceType = type
            self.presentationController?.present(self.imagePicker, animated: true)
        }
    }
    
    public func present(type: UIImagePickerController.SourceType){        
        guard UIImagePickerController.isSourceTypeAvailable(type) else {return}
        
        self.imagePicker.sourceType = type
        self.presentationController?.present(self.imagePicker, animated: true)
    }
    

    private func pickerController(_ controller: UIImagePickerController, didSelect image: UIImage?) {
        controller.dismiss(animated: true, completion: {
            self.delegate?.didSelect(image: image)
        })
    }
}

extension ImagePicker: UIImagePickerControllerDelegate {

    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.pickerController(picker, didSelect: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        
        guard let image = info[.originalImage] as? UIImage else {
            return self.pickerController(picker, didSelect: nil)
        }
        self.pickerController(picker, didSelect: image)
    }
    

}

extension ImagePicker: UINavigationControllerDelegate {}
