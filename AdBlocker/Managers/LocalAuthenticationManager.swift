//
//  LocalAuthenticationManager.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 05.03.2022.
//

import UIKit
import LocalAuthentication


protocol LocalAuthenticationManager{
    func hasFaceId() -> Bool
    func loginWithFaceId(completion: ((Bool) -> ())?)
}

extension LocalAuthenticationManager{
    
    func hasFaceId() -> Bool{
        let context = LAContext()
        var error: NSError?
        return context.canEvaluatePolicy(
            .deviceOwnerAuthenticationWithBiometrics,
            error: &error
        )
    }
    
    func loginWithFaceId(completion: ((Bool) -> ())?){
        let context = LAContext()
        let reason = "Log in with Face ID"
        context.evaluatePolicy(
            .deviceOwnerAuthenticationWithBiometrics,
            localizedReason: reason
        ) { success, error in
            if success {
                completion?(true)
            } else {
                completion?(false)
                DispatchQueue.main.async {
                    Vibration.error.vibrate()
                }
            }
        }
    }
    
}
