//
//  SubscriptionData.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 15.03.2022.
//

import UIKit
import Adapty
import SwiftSpinner

enum Identifiers: String, CaseIterable{
    case week
    case month
}

class PayManager{
    
    // MARK: - Static
    
    static let shared = PayManager()
    
    // MARK: - Properties
    
    private(set) var products: [ProductModel] = []

    
    // MARK: - Methods
    
    func initialiaze(){
        Adapty.logLevel = .all
        Adapty.activate("public_live_HMnTxJoy.vV24EcK42jPs0OO2f1J6", customerUserId: UIDevice.getId())
    }
    
    func loadProducts(spinner: Bool = true, completion: @escaping (([ProductModel]?) -> ())){
        if products.isEmpty{
            if spinner{
                SwiftSpinner.show("Loading offerings")
            }
            
            Adapty.getPaywalls(forceUpdate: false) { (paywalls, products, error) in
                DispatchQueue.main.async {
                    if spinner{
                        SwiftSpinner.hide()
                    }
                   
                    if error == nil, let paywall = paywalls?.first {
                        self.products = paywall.products
                        completion(self.products)
                    }
                }
            }
        }else{
            completion(self.products)
        }
    }

    func getProduct(_ identifier: Identifiers) -> ProductModel?{
        return products.first(where: {$0.vendorProductId == identifier.rawValue})
    }
    
    func getPrice(for product: ProductModel, devide: Float = 0) -> String? {
        guard let skProduct = product.skProduct else {return nil}
        let price = Float(truncating: skProduct.price) / devide
        let price2 = skProduct.price
    
        let numberFormatter = NumberFormatter()
        let locale = skProduct.priceLocale
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = locale
        
        if devide != 0{
            return numberFormatter.string(from: NSNumber(value: price))
        }else{
            return numberFormatter.string(from: price2)
        }
    }
    
    func getPurchaserInfo(completion: @escaping ((_ isSuccess: Bool) -> ())){
        Adapty.getPurchaserInfo { purchaser, error in
            if error == nil {
                completion(false)
                return
            }
            
            if let access = purchaser?.accessLevels["premium"]{
                User.shared.isPurchased = access.isLifetime ? true : access.isActive
            }
            completion(true)
        }
    }

}
