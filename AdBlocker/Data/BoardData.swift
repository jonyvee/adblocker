//
//  BoardData.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 22.01.2022.
//

import UIKit

class BoardData{
    
    static let items: [Item] = [
        .init(image: "board1".img(), title: "Blocks all ads", subtitle: "Forget about viral pop-ups,\nannoying commercials and\ndangerous websites"),
        .init(image: "board2".img(), title: "Smart Lock Filters", subtitle: "Save thousands of megabytes by\ndisabling advertising content and\nvirus windows!"),
        .init(image: "board3".img(), title: "Protect your data", subtitle: "Tracking blocking prevents\ncompanies from tracking your\nactivity while you are online"),
        .init(image: "board4".img(), title: "Secure Access", subtitle: "Keep your photos and passwords\nunder reliable protection, secure\nyour personal data"),
        .init(image: "board5".img(), title: "Open all functions", subtitle: "By purchasing a subscription, you support\nthe application and open all of its features.\n")
    ]
    
    struct Item{
        let image: UIImage?
        let title: String
        let subtitle: String
    }
}
