//
//  PurchaseData.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 22.01.2022.
//

import UIKit


class PurchaseData{
        
    static let textArray = [
        "Tracking blocking prevents\ncompanies from tracking your activity\nwhile you are online",
        "Block phishing, malware and other\nsuspicious content",
        "Improved methods of blocking\ncomplex ads, such as 18+ ads"
    ]
    
    static let items: [Item] = [
        .init(image: "purchase1".img(), title: "Blocks all ads", subtitle: "We'll block all unpleasant ads!"),
        .init(image: "purchase2".img(), title: "Speed", subtitle: "Increases page loading speed"),
        .init(image: "purchase3".img(), title: "High-quality ad blocker", subtitle: "Ad blocker, just for you"),
        .init(image: "purchase4".img(), title: "Protect your data", subtitle: "Protect your data from fraudsters!")
    ]
    
    struct Item{
        let image: UIImage?
        let title: String
        let subtitle: String
    }
}
