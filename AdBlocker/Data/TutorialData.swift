//
//  TutorialData.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 21.01.2022.
//

import UIKit

class TutorialData{

    static let tutorials: [Item] = [
        Item.init(title: "Open the settings", description: "Exit the app and go to your device\nsettings", image: "tutorial1".img()),
        Item.init(title: "Safari section", description: "Select Safari application\nin the settings of your device", image: "tutorial2".img()),
        Item.init(title: "Extension section", description: "Select the extensions\nsection and click on it", image: "tutorial3".img()),
        Item.init(title: "Active extension", description: "Find our application, and\ngive us permission ", image: "tutorial4".img()),
        Item.init(title: "Toggle on app", description: "Great! Complete all the points\nand come back to the app!", image: "tutorial5".img())
    ]
    
    struct Item{
        let title: String
        let description: String
        let image: UIImage?
    }
}
