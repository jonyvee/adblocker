//
//  UserData.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 23.01.2022.
//

import UIKit


class User{
    static var shared = User()
    private let defaults = UserDefaults.standard
    
    var isPurchased: Bool?{
        get {
            var id = false
            if defaults.value(forKey: "isPurchased") != nil{
                id = defaults.value(forKey: "isPurchased") as? Bool ?? false
            }
            return id
        }
        set {
            defaults.setValue(newValue, forKey: "isPurchased")
        }
    }
    
    var isFirstOpen: Bool?{
        get {
            var id = true
            if defaults.value(forKey: "isFirstOpen") != nil{
                id = defaults.value(forKey: "isFirstOpen") as? Bool ?? true
            }
            return id
        }
        set {
            defaults.setValue(newValue, forKey: "isFirstOpen")
        }
    }
    
    var isFinishedOnboard: Bool?{
        get {
            var id = false
            if defaults.value(forKey: "isFinishedOnboard") != nil{
                id = defaults.value(forKey: "isFinishedOnboard") as? Bool ?? false
            }
            return id
        }
        set {
            defaults.setValue(newValue, forKey: "isFinishedOnboard")
        }
    }
    
    var passcode: String?{
        get {
            var id = ""
            if defaults.value(forKey: "passcode") != nil{
                id = defaults.value(forKey: "passcode") as? String ?? ""
            }
            return id
        }
        set {
            defaults.setValue(newValue, forKey: "passcode")
        }
    }
    
    var hasPasscode: Bool?{
        get {
            var id = false
            if defaults.value(forKey: "hasPasscode") != nil{
                id = defaults.value(forKey: "hasPasscode") as? Bool ?? false
            }
            return id
        }
        set {
            defaults.setValue(newValue, forKey: "hasPasscode")
        }
    }
    
    var hasFaceid: Bool?{
        get {
            var id = false
            if defaults.value(forKey: "hasFaceid") != nil{
                id = defaults.value(forKey: "hasFaceid") as? Bool ?? false
            }
            return id
        }
        set {
            defaults.setValue(newValue, forKey: "hasFaceid")
        }
    }
    
    var summMinutes: Int?{
        get {
            var id = 0
            if defaults.value(forKey: "summMinutes") != nil{
                id = defaults.value(forKey: "summMinutes") as? Int ?? 0
            }
            return id
        }
        set {
            defaults.setValue(newValue, forKey: "summMinutes")
        }
    }
    
    var lastWorkingDate: Date?{
        get {
            var id = Date()
            if defaults.value(forKey: "lastWorkingDate") != nil{
                id = defaults.value(forKey: "lastWorkingDate") as? Date ?? Date()
            }
            return id
        }
        set {
            defaults.setValue(newValue, forKey: "lastWorkingDate")
        }
    }
    
    var status: Status?{
        get {
            return defaults.getObject(key: "status", type: Status()) ?? Status()
        }
        set {
            defaults.setObject(newValue, key: "status")
        }
    }
        
    var myboard: MyBoardModel?{
        get {
            return defaults.getObject(key: "myboard", type: MyBoardModel()) ?? MyBoardModel()
        }
        set {
            defaults.setObject(newValue, key: "myboard")
        }
    }

}
