//
//  SettingsData.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 20.01.2022.
//

import UIKit


class SettingsData{
    static let titles = [
        "How to use",
        "Passcode settings",
        "Support",
        "Share the App",
        "Rate the app",
        "Privacy Policy",
        "Terms of use"
    ]
    
    static func image(index: Int) -> UIImage?{
        return "settings_\(index+1)".img()
    }
}
