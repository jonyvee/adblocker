//
//  Constants.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 23.01.2022.
//

import UIKit

public struct Constants{
    static let groupIdentifier = "group.jonyvee.blocker"

    enum Host: String{
        case ad = "jonyvee.AdBlockerrr.AdContent"
        case adult = "jonyvee.AdBlockerrr.AdultContent"
        case script = "jonyvee.AdBlockerrr.ScriptContent"
        case malvare = "jonyvee.AdBlockerrr.MalvareContent"
        case cookie = "jonyvee.AdBlockerrr.CookieContent"
        
        var path: String{
            switch self {
            case .ad:
                return "Ad.json"
            case .adult:
                return "Adult.json"
            case .script:
                return "Script.json"
            case .malvare:
                return "Malvare.json"
            case .cookie:
                return "Cookie.json"
            }
        }
        
        var json: String{
            switch self {
            case .ad:
                return "https://gitlab.com/jonyvee/adblockerhost/-/raw/main/adsCSS.txt"
            case .adult:
                return "https://gitlab.com/jonyvee/adblockerhost/-/raw/main/adult.txt"
            case .script:
                return "https://gitlab.com/jonyvee/adblockerhost/-/raw/main/trackers.txt"
            case .malvare:
                return "https://gitlab.com/jonyvee/adblockerhost/-/raw/main/malvare.txt"
            case .cookie:
                return "https://gitlab.com/jonyvee/adblockerhost/-/blob/main/cookie.txt"
            }
        }
        
    }
}
