//
//  AppDelegate.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 20.01.2022.
//

import UIKit
import AppsFlyerLib
import Adapty
import AppTrackingTransparency
import OneSignal
import Appodeal
import YandexMobileMetrica


@main
class AppDelegate: UIResponder, UIApplicationDelegate, AppsFlyerLibDelegate, DeepLinkDelegate, AdBlocker {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // qonversion purchase
        PayManager.shared.initialiaze()
        PayManager.shared.loadProducts(spinner: false, completion: { tt in})
        PayManager.shared.getPurchaserInfo { isSuccess in}
        
        // appsflayer analitics
        AppsFlyerLib.shared().appsFlyerDevKey = "DhPhPamMUudLdzYtW8vHuC"
        AppsFlyerLib.shared().appleAppID = "1606941362"
        AppsFlyerLib.shared().delegate = self
        AppsFlyerLib.shared().isDebug = true
        AppsFlyerLib.shared().customerUserID = UIDevice.getId()
        AppsFlyerLib.shared().deepLinkDelegate = self
        
        // one signal
        OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)
        OneSignal.initWithLaunchOptions(launchOptions)
        OneSignal.setAppId("b838df21-4643-4c75-b562-190a3c0b04e7")
        OneSignal.setExternalUserId(UIDevice.getId())
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
            // send analitica about push accept
        })
        
        
        // appodeal
        Appodeal.setAutocache(true, types: .banner)
        Appodeal.setTestingEnabled(true)
        

        // ATT
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization { status in
                switch status {
                case .authorized:
                    Appodeal.initialize(withApiKey: "5807947eef06d0bcaaf2023b352aef0cf2217de5f8482292", types: [.banner], hasConsent: true)
                    AdManager.shared.setupGAD()
                default:
                    Appodeal.initialize(withApiKey: "5807947eef06d0bcaaf2023b352aef0cf2217de5f8482292", types: [.banner], hasConsent: false)
                    AdManager.shared.setupGAD()
                    break;
                }
            }
        }else{
            Appodeal.initialize(withApiKey: "5807947eef06d0bcaaf2023b352aef0cf2217de5f8482292", types: [.banner], hasConsent: true)
            AdManager.shared.setupGAD()
        }
        
        
        // yandex mobile metrica
        let configuration = YMMYandexMetricaConfiguration.init(apiKey: "6cff18d1-9b08-4c7f-ad82-f6722f0d6122")
        configuration?.userProfileID = UIDevice.getId()
        configuration?.locationTracking = true
        configuration?.crashReporting = true
        YMMYandexMetrica.activate(with: configuration!)
        
        
        // observer
        NotificationCenter.default.addObserver(self, selector: #selector(sendLaunch), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        return true
    }

    
    // MARK: - AppsFlyer
    @objc private func sendLaunch(){
        AppsFlyerLib.shared().start()
    }
    
    func onConversionDataSuccess(_ conversionInfo: [AnyHashable : Any]) {
        checkAndPost(data: conversionInfo)
    }
    
    func onConversionDataFail(_ error: Error) {
        debugPrint(error.localizedDescription)
    }

    func onAppOpenAttribution(_ attributionData: [AnyHashable : Any]) {
        checkAndPost(data: attributionData)
    }
    
    func onAppOpenAttributionFailure(_ error: Error) {
        print(error)
    }
    
    private func checkAndPost(data: [AnyHashable : Any]){
        if data["af_status"] as? String == "Non-organic"{
            post()
        }
        
        func post(){
            if User.shared.myboard?.subscription == ""{
                User.shared.myboard = MyBoardModel(data)
                NotificationCenter.default.post(name: Notification.Name("SplashVC"), object: nil, userInfo: nil)
            }
        }
    }
    
    
    // MARK: UISceneSession Lifecycle
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

}

