//
//  TrackerListParser.swift
//  TrackerBlocker
//
//  Created by Nikolay Derkach on 9/17/18.
//  Copyright © 2018 Nikolay Derkach. All rights reserved.
//

import Alamofire

class TrackerListParser {

	typealias BlockerList = [[String: [String: String]]]

    
    
    // MARK: - Public
    static func fetchAndParseTrackerList(host: Constants.Host, completion: @escaping (BlockerList) -> Void ) {
        AF.request(host.json).responseData(queue: .global()) { response in
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                switch host{
                case .ad:
                    return completion(TrackerListParser.convertAdFormat(text: utf8Text))
                case .adult:
                    return completion(TrackerListParser.convertBlockFormat(text: utf8Text))
                case .script:
                    return completion(TrackerListParser.convertBlockFormat(text: utf8Text))
                case .malvare:
                    return completion(TrackerListParser.convertBlockFormat(text: utf8Text))
                case .cookie:
                    return completion(TrackerListParser.convertCookieFormat(text: utf8Text))
                }
            }
        }
    }
    
    
    
    
    
    
    
	// MARK: - Private
    private static func generateAdJSON(from trackerList: [String]) -> BlockerList {
        var blacklist: BlockerList = []
        for tracker in trackerList {
            blacklist.append([
                "action": ["type": "css-display-none",
                           "selector": tracker],
                "trigger": ["url-filter": ".*"]
            ])
        }

        return blacklist
    }
    
    private static func generateCookieJSON(from trackerList: [String]) -> BlockerList {
        var blacklist: BlockerList = []
        blacklist.append([
            "action": ["type": "block-cookies"],
            "trigger": ["url-filter": ".*"]
        ])
        for tracker in trackerList {
            blacklist.append([
                "action": ["type": "css-display-none",
                           "selector": tracker],
                "trigger": ["url-filter": ".*"]
            ])
        }
        return blacklist
    }
    
	private static func generateBlacklistJSON(from trackerList: [String]) -> BlockerList {
		var blacklist: BlockerList = []
		for tracker in trackerList {
			blacklist.append([
				"action": ["type": "block"],
				"trigger": ["url-filter": String(format: "https?://(www.)?%@.*", tracker)]
			])
		}

		return blacklist
	}
    
	
    
    
    // MARK: - Ad
    static func convertAdFormat(text: String) -> BlockerList{
        let lines = text.components(separatedBy: .newlines)
        var parsedLines: [String] = []
        
        for line in lines {
            if line.isEmpty {
                continue
            }

            if let domainName = line.components(separatedBy: .whitespaces).first {
                parsedLines.append(domainName)
            }
        }
        
        return TrackerListParser.generateAdJSON(from: parsedLines)
    }
    
    
    // MARK: - Cookie
    static func convertCookieFormat(text: String) -> BlockerList{
        let lines = text.components(separatedBy: .newlines)
        var parsedLines: [String] = []
        
        for line in lines {
            if line.isEmpty {
                continue
            }

            if let domainName = line.components(separatedBy: .whitespaces).first {
                parsedLines.append(domainName)
            }
        }
        
        return TrackerListParser.generateCookieJSON(from: parsedLines)
    }
    
    
        
    // MARK: - Script
    static func convertBlockFormat(text: String) -> BlockerList{
        let lines = text.components(separatedBy: .newlines)
        var parsedLines: [String] = []
        
        for line in lines {
            if line.starts(with: "#") || line.isEmpty {
                continue
            }

            if let domainName = line.components(separatedBy: .whitespaces).first {
                parsedLines.append(domainName)
            }
        }
        
        return TrackerListParser.generateBlacklistJSON(from: parsedLines)
    }
}
