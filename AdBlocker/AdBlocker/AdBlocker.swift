//
//  AdBlocker.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 23.01.2022.
//

import UIKit
import SafariServices


protocol AdBlocker{
    func reload(host: Constants.Host)
    func write(host: Constants.Host, object: TrackerListParser.BlockerList?)
    func getState(host: Constants.Host, completion: ((Bool) -> ())?)
}


extension AdBlocker{
    func reload(host: Constants.Host){
        SFContentBlockerManager.reloadContentBlocker(withIdentifier: host.rawValue) { error in
            if let error = error{
                print("jonyvee error ", error.localizedDescription)
            }else{
                print("Reloaded \(host.rawValue)")
            }
        }
    }
    
    func write(host: Constants.Host, object: TrackerListParser.BlockerList?){
        let emptyData: TrackerListParser.BlockerList = [[
            "action": ["type": "block"],
             "trigger": ["url-filter":"webkit.svg"]
        ]]

        guard let jsonData = try? JSONSerialization.data(withJSONObject: object ?? emptyData, options: .prettyPrinted) else {return}
        let documentFolder = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: Constants.groupIdentifier)
        guard let jsonURL = documentFolder?.appendingPathComponent(host.path) else {return}
        do {
            if FileManager.default.fileExists(atPath: jsonURL.path){
                try FileManager.default.removeItem(at: jsonURL)
            }
            try jsonData.write(to: jsonURL)
        } catch {
            print("jonyvee = ", error.localizedDescription)
        }        
    }
    
    
    
    func getState(host: Constants.Host, completion: ((Bool) -> ())?){
        SFContentBlockerManager.getStateOfContentBlocker(withIdentifier: host.rawValue) { state, error in
            if let error = error {
                completion?(false)
                print(error.localizedDescription)
            }
            if let state = state {
                completion?(state.isEnabled)
            }
        }
    }
    
    
}
