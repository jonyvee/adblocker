//
//  PasswordsCell.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 20.02.2022.
//

import UIKit
import SnapKit


struct PasswordCellPresentable{
    var title: String
    var login: String
    var url: String
    var password: String
    var isPassHidden: Bool
    var isFavorite: Bool 
}

class PasswordCell: UICollectionViewCell{
    
    // MARK: - Properties
    
    static let key = "PasswordCell"
    
    // MARK: -
    
    private lazy var backView: UIView = {
        let view = UIView()
        view.isUserInteractionEnabled = true
        view.backgroundColor = UIColor(red: 0.118, green: 0.839, blue: 0.58, alpha: 0.2)
        view.layer.cornerRadius = 16
        return view
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 0.165, green: 0.188, blue: 0.22, alpha: 1)
        label.font = UIFont(name: "Montserrat-SemiBold", size: 14)
        return label
    }()
    
    private let loginLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 0.165, green: 0.188, blue: 0.22, alpha: 1)
        label.font = UIFont(name: "Montserrat-Regular", size: 12)
        return label
    }()
    
    private let passwordLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 0.165, green: 0.188, blue: 0.22, alpha: 1)
        label.font = UIFont(name: "Montserrat-Medium", size: 16)
        return label
    }()
    
    private lazy var copyBtn: UIButton = {
        let button = UIButton()
        button.setImage("copyy".img(), for: .normal)
        button.addTarget(self, action: #selector(copyPasswordAction), for: .touchUpInside)
        return button
    }()
    
    private let heartImageView = UIImageView(image: "heartfill".img())
    
    // MARK: -
    private var password = ""
    
    // MARK: -
    
    var didCopy: (() -> ())?
                
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.backgroundColor = .white
        setupViews()
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backView.layer.shadowPath = UIBezierPath(rect:  backView.bounds).cgPath
    }
    
    // MARK: - Configuration
    
    func configure(with presentable: PasswordCellPresentable){
        heartImageView.isHidden = !presentable.isFavorite
        
        self.titleLabel.text = presentable.title
        loginLabel.text = presentable.login
        passwordLabel.text = presentable.password
        self.password = presentable.password
        self.contentView.layoutIfNeeded()
    }
    
    // MARK: - Actions
    
    @objc private func copyPasswordAction(){
        Vibration.soft.vibrate()
        didCopy?()
        UIPasteboard.general.string = self.password
    }
    
    // MARK: - UI
    
    private func setupViews(){
        contentView.addSubview(backView)
        backView.addSubview(titleLabel)
        backView.addSubview(loginLabel)
        backView.addSubview(passwordLabel)
        backView.addSubview(copyBtn)
        backView.addSubview(heartImageView)
    }
    
    private func setConstraints(){
        backView.snp.makeConstraints { make in
            make.leading.trailing.top.bottom.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview().inset(16)
            make.height.equalTo(20)
        }
        
        loginLabel.snp.makeConstraints { make in
            make.leading.equalTo(titleLabel)
            make.top.equalTo(titleLabel.snp.bottom)
            make.height.equalTo(20)
            make.trailing.equalTo(copyBtn.snp.leading)
        }
        
        passwordLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(titleLabel)
            make.top.equalTo(loginLabel.snp.bottom).inset(-4)
        }
        
        copyBtn.snp.makeConstraints { make in
            make.height.width.equalTo(44)
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview()
        }

        heartImageView.snp.makeConstraints { make in
            make.width.height.equalTo(24)
            make.top.trailing.equalToSuperview().inset(8)
        }
    }
    
}
