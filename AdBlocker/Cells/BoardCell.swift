//
//  BoardCell.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 22.01.2022.
//

import UIKit

class BoardCell: UICollectionViewCell{
    static let key = "BoardCell"
    
    private let imageView = UIImageView()
    private let titleLabel = UILabel()
    private let descriptionLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setImageView()
        setTitleLabel()
        setDescriptionLabel()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Configuration
    func configure(item: BoardData.Item, price: String){
        imageView.image = item.image
        titleLabel.text = item.title
        descriptionLabel.text = item.subtitle
        
        if price != ""{
            let text = "Subscription costs "
            descriptionLabel.attributedText = NSMutableAttributedString().normal(item.subtitle).normal(text).bold("\(price) per weak")
        }
    }
    
    
    // MARK: - UI
    private func setImageView(){
        imageView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(imageView)
        imageView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor).isActive = true
    }
    
    private func setTitleLabel(){
        titleLabel.modifyTitle(text: "")
        titleLabel.textAlignment = .center
        contentView.addSubview(titleLabel)
        titleLabel.heightAnchor.constraint(equalToConstant: 29).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 24).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -24).isActive = true
        titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: UIDevice.hasNotch ? 49 : 20).isActive = true
    }
    
    private func setDescriptionLabel(){
        descriptionLabel.modifySubTitle(text: "")
        descriptionLabel.numberOfLines = 3
        descriptionLabel.textAlignment = .center
        contentView.addSubview(descriptionLabel)
        descriptionLabel.heightAnchor.constraint(equalToConstant: 75).isActive = true
        descriptionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20).isActive = true
        descriptionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20).isActive = true
        descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10).isActive = true
    }
}
