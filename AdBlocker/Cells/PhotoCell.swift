//
//  PhotoCell.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 28.02.2022.
//

import UIKit
import SnapKit

struct PhotoCellPresentable{
    var image: UIImage?
    var isSelected: Bool
}

class PhotoCell: UICollectionViewCell{
    
    // MARK: - Static
    
    static let key = "PhotoCell"
    
    // MARK: - Properties

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private let checkyImageView = UIImageView(image: "checky".img())
        
    // MARK: - Initialization

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Configuration
    
    func configure(presentable: PhotoCellPresentable){
        checkyImageView.isHidden = !presentable.isSelected
        
        if let image = presentable.image{
            imageView.image = image
        }
    }
    
    // MARK: - UI
    
    private func setupViews(){
        contentView.addSubview(imageView)
        imageView.addSubview(checkyImageView)
    }
    
    private func setConstraints(){
        imageView.snp.makeConstraints { make in
            make.leading.top.trailing.bottom.equalToSuperview()
        }
        
        checkyImageView.snp.makeConstraints { make in
            make.width.height.equalTo(24)
            make.bottom.trailing.equalToSuperview().inset(8)
        }
    }

}
