//
//  SettingCell.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 20.01.2022.
//

import UIKit

class SettingCell: UICollectionViewCell{
    static let key = "SettingCell"
    
    private let backView = UIView()
    private let imageView = UIImageView()
    private let forwardImageView = UIImageView(image: "forward".img())
    private let textLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setBackView()
        setImageView()
        setTextLabel()
        setForwardImageView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    
    // MARK: - Configuration
    
    func configure(index: Int){
        self.imageView.image = SettingsData.image(index: index)
        self.textLabel.text = SettingsData.titles[index]
    }
    
    
    
    // MARK: - UI
    private func setBackView(){
        contentView.addSubview(backView)
        backView.pinToSuperViewEdges()
        backView.layer.cornerRadius = 16
        backView.backgroundColor = UIColor(red: 0.761, green: 0.804, blue: 0.863, alpha: 0.1)
    }
    
    private func setImageView(){
        imageView.translatesAutoresizingMaskIntoConstraints = false
        backView.addSubview(imageView)
        imageView.heightAnchor.constraint(equalToConstant: 48).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 48).isActive = true
        imageView.centerYAnchor.constraint(equalTo: backView.centerYAnchor).isActive = true
        imageView.leadingAnchor.constraint(equalTo: backView.leadingAnchor, constant: 8).isActive = true
    }
    
    private func setTextLabel(){
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.font = Font.bold.font(size: 18)
        textLabel.textColor = Color.title.ui
        backView.addSubview(textLabel)
        textLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        textLabel.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 14).isActive = true
        textLabel.centerYAnchor.constraint(equalTo: backView.centerYAnchor).isActive = true
    }
    
    private func setForwardImageView(){
        forwardImageView.translatesAutoresizingMaskIntoConstraints = false
        backView.addSubview(forwardImageView)
        forwardImageView.heightAnchor.constraint(equalToConstant: 24).isActive = true
        forwardImageView.widthAnchor.constraint(equalToConstant: 24).isActive = true
        forwardImageView.trailingAnchor.constraint(equalTo: backView.trailingAnchor, constant: -8).isActive = true
        forwardImageView.centerYAnchor.constraint(equalTo: backView.centerYAnchor).isActive = true
    }
}
