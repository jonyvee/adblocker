//
//  TutorialCell.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 21.01.2022.
//

import UIKit

class TutorialCell: UICollectionViewCell{
    static let key = "TutorialCell"
    
    private let imageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setImageView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Configuration
    func configure(image: UIImage?){
        imageView.image = image
    }
    
    // MARK: - UI
    private func setImageView(){
        imageView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(imageView)
        imageView.pinToSuperViewEdges()
    }
}
