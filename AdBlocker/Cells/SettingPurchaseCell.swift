


import UIKit

class SettingPurchaseCell: UICollectionViewCell{
    static let key = "SettingPurchaseCell"
    
    private let backView = UIView()
    private let imageView = UIImageView(image: "settings_purchase".img())
    private let titleLabel = UILabel()
    private let subTitleLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setBackView()
        setImageView()
        setTitleLabel()
        setSubTitleLabel()
        
        backView.applyGradient(colours: [Color.green1.ui, Color.green2.ui], locations: [0,1], startPoint: CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 1, y: 1), cornerRadius: 16)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        backView.applyGradient(colours: [Color.green1.ui, Color.green2.ui], locations: [0,1], startPoint: CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 1, y: 1), cornerRadius: 16)
    }
    
    
    // MARK: - Configuration
    func configure(){
        titleLabel.text = "Get premium today"
        subTitleLabel.text = "Unlock all the benefits of the app"
        backView.applyGradient(colours: [Color.green1.ui, Color.green2.ui], locations: [0,1], startPoint: CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 1, y: 1), cornerRadius: 16)
        self.layoutIfNeeded()
    }
    
    
    // MARK: - UI
    private func setBackView(){
        backView.translatesAutoresizingMaskIntoConstraints = false
        backView.layer.cornerRadius = 16
        contentView.addSubview(backView)
        backView.pinToSuperViewEdges()
    }
    
    private func setImageView(){
        backView.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.heightAnchor.constraint(equalToConstant: 48).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 48).isActive = true
        imageView.leadingAnchor.constraint(equalTo: backView.leadingAnchor, constant: 16).isActive = true
        imageView.centerYAnchor.constraint(equalTo: backView.centerYAnchor).isActive = true
    }
    
    private func setTitleLabel(){
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.textColor = .white
        titleLabel.font = Font.bold.font(size: 18)
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.numberOfLines = 1
        backView.addSubview(titleLabel)
        titleLabel.heightAnchor.constraint(equalToConstant: 27).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 18).isActive = true
        titleLabel.topAnchor.constraint(equalTo: backView.topAnchor, constant: 19).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: backView.trailingAnchor, constant: -8).isActive = true
    }
    
    private func setSubTitleLabel(){
        subTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        subTitleLabel.textColor = .white.withAlphaComponent(0.7)
        subTitleLabel.font = Font.medium.font(size: 12)
        subTitleLabel.adjustsFontSizeToFitWidth = true
        backView.addSubview(subTitleLabel)
        subTitleLabel.heightAnchor.constraint(equalToConstant: 15).isActive = true
        subTitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: -1).isActive = true
        subTitleLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor).isActive = true
        subTitleLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor).isActive = true
    }
}
