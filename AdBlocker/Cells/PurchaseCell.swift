//
//  PurchaseCell.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 22.01.2022.
//

import UIKit

class PurchaseCell: UICollectionViewCell{
    static let key = "PurchaseCell"
    
    private let textLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setTextLabel()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Configuration
    func configure(text: String){
        textLabel.text = text
    }
    
    // MARK: - UI
    private func setTextLabel(){
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(textLabel)
        textLabel.font = Font.regular.font(size: 15)
        textLabel.numberOfLines = 3
        textLabel.adjustsFontSizeToFitWidth = true
        textLabel.textColor = Color.title.ui
        textLabel.pinToSuperViewEdges()
    }
}
