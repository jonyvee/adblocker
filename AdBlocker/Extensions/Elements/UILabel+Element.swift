//
//  UILabel+Element.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 20.01.2022.
//

import UIKit

extension UILabel{
    
    func modifyTitle(text: String){
        translatesAutoresizingMaskIntoConstraints = false
        self.text = text
        textColor = Color.title.ui
        font = Font.bold.font(size: 24)
        adjustsFontSizeToFitWidth = true
    }
    
    func modifySubTitle(text: String){
        translatesAutoresizingMaskIntoConstraints = false
        self.text = text
        textColor = Color.subtitle.ui
        font = Font.regular.font(size: 16)
        adjustsFontSizeToFitWidth = true
        numberOfLines = 2
    }
}
