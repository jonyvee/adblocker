//
//  UIButton.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 20.01.2022.
//

import UIKit


extension UIButton{
    func modify(title: String = ""){
        self.translatesAutoresizingMaskIntoConstraints = false
        self.setTitleColor(.white, for: .normal)
        self.setTitle(title, for: .normal)
        self.titleLabel?.font = Font.bold.font(size: 16)
        self.backgroundColor = Color.button.ui
        self.layer.cornerRadius = 16
    }
}
