//
//  UserDefaults+Extension.swift
//  Raskraska2
//
//  Created by Nikola Cvetkovic on 24.11.2020.
//  Copyright © 2020 jonyvee. All rights reserved.
//

import UIKit


extension UserDefaults {
    func imageForKey(key: String) -> UIImage? {
        let imageData = object(forKey: key) as? Data
        var image: UIImage? = nil
        if let imageData = imageData {
            image = UIImage(data: imageData)
        }
        return image
    }
    func setImage(image: UIImage, forKey key: String) {
        set(image.pngData(), forKey: key)
    }
    
    
    func getObject<T: Decodable>(key: String, type: T) -> T?{
        guard let decoded  = data(forKey: key) else{return nil}
        return try? JSONDecoder().decode(T.self, from: decoded)
    }

    func setObject<T: Encodable>(_ data: T, key: String){
        if let object = try? JSONEncoder().encode(data) {
            set(object, forKey: key)
        }
    }
}
