//
//  String.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 20.01.2022.
//

import UIKit


extension String{
    func height(font: UIFont, width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)

        return boundingBox.height
    }
    
    func img() -> UIImage?{
        return UIImage(named: self)
    }
    
    func normalize() -> String{
        var result = self.replacingOccurrences(of: "||", with: "\n")
        result = result.replacingOccurrences(of: "|", with: "!")
        return result
    }
}
