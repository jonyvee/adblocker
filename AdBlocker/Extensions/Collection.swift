//
//  Collection.swift
//  Raskraska2
//
//  Created by Nikola Cvetkovic on 15.05.2021.
//  Copyright © 2021 jonyvee. All rights reserved.
//

import UIKit


extension Collection {
    public subscript (safe index: Self.Index) -> Iterator.Element? {
        (startIndex ..< endIndex).contains(index) ? self[index] : nil
    }    
}
