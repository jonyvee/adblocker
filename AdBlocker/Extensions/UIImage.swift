//
//  UIImage.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 26.03.2022.
//

import UIKit

extension UIImage {
    func aspectFitted(_ newWidth: CGFloat) -> UIImage {
        let scale = newWidth / self.size.width
        let newHeight = self.size.height * scale
        let newSize = CGSize(width: newWidth, height: newHeight)
        let renderer = UIGraphicsImageRenderer(size: newSize)

        return renderer.image { _ in
            self.draw(in: CGRect(origin: .zero, size: newSize))
        }
    }
}

