//
//  UIView.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 20.01.2022.
//

import UIKit


extension UIView {
    @discardableResult
    func pinToSuperViewEdges(left: CGFloat = 0, top: CGFloat = 0, right: CGFloat = 0, bottom: CGFloat = 0) -> [NSLayoutConstraint] {
        guard let superview = superview else { return [] }
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(equalTo: superview.topAnchor, constant: top).isActive = true
        bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: bottom).isActive = true
        leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: left).isActive = true
        trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: right).isActive = true
        return constraints
    }
}
