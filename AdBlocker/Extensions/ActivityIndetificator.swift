////
////  ActivityInditificator.swift
////  Quarta Panel
////
////  Created by Nikola on 12/06/2019.
////  Copyright © 2019 serbionare. All rights reserved.
////
//
//import UIKit
//import Lottie
//
//
//private var activityView:UIView!
//
//func showActivityIndetificator(superView : UIView, isTableView: Bool = false){
//
//    if (activityView?.superview) != nil{
//        return
//    }
//
//    let size:CGFloat = 150
//
//    var activityViewMinusYPosition: CGFloat = size
//
//    if isTableView == false {
//        activityViewMinusYPosition = size/2
//    }
//
//    activityView = UIView(frame: CGRect(x: superView.frame.size.width/2-size/2, y: superView.frame.size.height/2-activityViewMinusYPosition, width: size, height: size))
//    activityView.backgroundColor = .clear
//
//
//    let lottieAnimation = AnimationView(name: "loading")
//    lottieAnimation.contentMode = .scaleAspectFit
//    lottieAnimation.layer.zPosition = 2
//    lottieAnimation.animationSpeed = 1
//    lottieAnimation.loopMode = .loop
//    lottieAnimation.translatesAutoresizingMaskIntoConstraints = false
//    activityView.addSubview(lottieAnimation)
//    lottieAnimation.pinToSuperViewEdges()
//
//    DispatchQueue.main.async {
//        lottieAnimation.play()
//        superView.addSubview(activityView)
//    }
//}
//
//func hideActivityIndetificator(){
//    if (activityView?.superview) == nil{
//        return
//    }
//
//    activityView.removeFromSuperview()
//}
//
//
//
//
//
//
//
//
//
//extension UIViewController{
//
//    func showCNIndicator(touchDissabled: Bool = false){
//        let activityView = UIView()
//        activityView.translatesAutoresizingMaskIntoConstraints = false
//        activityView.tag = 9329395
//        activityView.isUserInteractionEnabled = touchDissabled
//        self.view.addSubview(activityView)
//        activityView.pinToSuperViewEdges()
//
//
//        let lottieAnimation = AnimationView(name: "loading")
//        lottieAnimation.contentMode = .scaleAspectFit
//        lottieAnimation.layer.zPosition = 2
//        lottieAnimation.animationSpeed = 1
//        lottieAnimation.loopMode = .loop
//        lottieAnimation.translatesAutoresizingMaskIntoConstraints = false
//        activityView.addSubview(lottieAnimation)
//        lottieAnimation.heightAnchor.constraint(equalToConstant: 150).isActive = true
//        lottieAnimation.widthAnchor.constraint(equalToConstant: 150).isActive = true
//        lottieAnimation.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
//        lottieAnimation.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
//
//        DispatchQueue.main.async {
//            lottieAnimation.play()
//        }
//    }
//
//
//
//    func removeCNIndicator(){
//        DispatchQueue.main.async {
//            let activityView = self.view.viewWithTag(9329395)
//            activityView?.removeFromSuperview()
//        }
//    }
//
//}
//
//
