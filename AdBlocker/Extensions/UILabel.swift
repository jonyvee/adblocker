//
//  UILabel.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 22.01.2022.
//

import UIKit

extension UILabel{
    func underlineText(color: UIColor){
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: self.text ?? "")
        attributeString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSMakeRange(0, attributeString.length))
        attributeString.addAttribute(NSAttributedString.Key.underlineColor, value: color, range: NSMakeRange(0, attributeString.length))
        self.attributedText = attributeString
    }
}
