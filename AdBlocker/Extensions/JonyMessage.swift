//
//  Message.swift
//  message
//
//  Created by Nikola Cvetkovic on 07.12.2021.
//

import UIKit

protocol JonyMessage{
    func message(text: String)
}

extension JonyMessage where Self: UIViewController{
    
    func message(text: String){
        let image = UIImage(named: "board1")
        let jonyView = JonyMessageView(frame: getFrame(text: text), image: image, text: text)
        view.addSubview(jonyView)
        animate(messageView: jonyView)
    }
    
    private func getFrame(text: String) -> CGRect{
        let minHeight: CGFloat = 52
        let messageWidth = UIScreen.main.bounds.width-26
        let verticalPaddingConst: CGFloat = 30
        var messageHeight: CGFloat = minHeight
        let textHeight = text.height(font: JonyMessageView.messageFont, width: messageWidth) + verticalPaddingConst
        messageHeight = textHeight > minHeight ? textHeight : minHeight
        return CGRect(x: 13, y: -messageHeight, width: messageWidth, height: messageHeight)
    }
    
    private func animate(messageView: JonyMessageView){
        // Время отображения сообщения
        let alertTimeInterval: TimeInterval = 0.4
        
        // Плавное отображение сообщения
        UIView.animate(withDuration: alertTimeInterval, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            messageView.frame.origin.y = UIDevice.hasNotch ? UIDevice.notchHeight : 30
        }, completion: { (true) in
            UIView.animate(withDuration: alertTimeInterval, delay: 2, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                messageView.frame.origin.y = -messageView.frame.height
            }, completion: { (true) in
                messageView.removeFromSuperview()
            })
        })
    }
}


private final class JonyMessageView: UIView{
    
    static let messageFont = UIFont.systemFont(ofSize: 17, weight: .regular)
    
    private let imageView = UIImageView()
    private let textLabel = UILabel()
    
    private var image: UIImage?
    private var text: String?
    
    
    
    
    init(frame: CGRect, image: UIImage?, text: String) {
        self.image = image
        self.text = text
        super.init(frame: frame)
        
        setView()
        setBlurView()
        setImageView()
        setTextLabel()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    // MARK: - UI
    private func setView(){
        self.layer.cornerRadius = 8
        self.layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        self.layer.shadowColor = UIColor(red: 0.424, green: 0.258, blue: 0.554, alpha: 0.5).cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 20
        self.layer.shadowOffset = CGSize(width: 0, height: 4)
        self.backgroundColor = .clear
        self.gradientBorder(width: 2, colors: [#colorLiteral(red: 0.7568627451, green: 0.431372549, blue: 0.5921568627, alpha: 1),#colorLiteral(red: 0.4823529412, green: 0.368627451, blue: 0.8588235294, alpha: 1)], startPoint: CGPoint(x: 0, y: 0), endPoint: CGPoint(x: 1, y: 1), andRoundCornersWithRadius: 8)
    }
    
    private func setImageView(){
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = self.image
        imageView.contentMode = .scaleAspectFit
        self.addSubview(imageView)
        imageView.heightAnchor.constraint(equalToConstant: 24).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 24).isActive = true
        imageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 11).isActive = true
        imageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    
    private func setTextLabel(){
        textLabel.text = text
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.font = JonyMessageView.messageFont
        textLabel.textColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
        textLabel.numberOfLines = 0
        self.addSubview(textLabel)
        
        textLabel.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 12).isActive = true
        textLabel.topAnchor.constraint(equalTo: topAnchor, constant: 3).isActive = true
        textLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -3).isActive = true
        textLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10).isActive = true
    }
    
    private func setBlurView(){
        let backView = UIView()
        backView.layer.cornerRadius = 8
        backView.translatesAutoresizingMaskIntoConstraints = false
        backView.clipsToBounds = true
        self.addSubview(backView)
        backView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 1).isActive = true
        backView.topAnchor.constraint(equalTo: topAnchor, constant: 1).isActive = true
        backView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -1).isActive = true
        backView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -1).isActive = true
        
        // 1. create container view
        let containerView = UIView()
        containerView.layer.cornerRadius = 8
        // 2. create custom blur view
        let blurEffect = UIBlurEffect(style: .systemMaterialLight)
        let customBlurEffectView = JonyVisualEffectView(effect: blurEffect, intensity: 0.1)
        customBlurEffectView.frame = self.bounds
        // 3. create semi-transparent black view
        let dimmedView = UIView()
        dimmedView.backgroundColor = .white.withAlphaComponent(0.3)
        dimmedView.frame = self.bounds
        // 4. add both as subviews
        containerView.addSubview(customBlurEffectView)
        containerView.addSubview(dimmedView)
        // 5.
        backView.addSubview(containerView)
        backView.sendSubviewToBack(containerView)
    }
    
}



