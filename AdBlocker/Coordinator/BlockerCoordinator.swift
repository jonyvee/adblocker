//
//  BlockerCoordinator.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 26.03.2022.
//

import UIKit
import Combine
import Photos
import SafariServices
import MessageUI

class BlockerCoordinator: Coordinator{
    
    // MARK: - Properties
    
    private var navigationController: UINavigationController = {
        let navigation = UINavigationController()
        navigation.isNavigationBarHidden = true
        navigation.tabBarItem.title = ""
        navigation.tabBarItem.image = "blockerP".img()
        navigation.tabBarItem.selectedImage = "blockerA".img()
        return navigation
    }()
    
    var rootViewController: UIViewController {
        return navigationController
    }
    
    private var settingsNavigation = UINavigationController()
    
    // MARK: - Initialization
    
    override init(){
        super.init()
    }
    
    // MARK: - Overrides
    
    override func start() {
        let controller = ViewController.instantiate()
        controller.didShowPurchase = { [weak self] in
            self?.presentPurchase()
        }
        controller.didShowSettings = { [weak self] in
            self?.showSettings()
        }
        navigationController.pushViewController(controller, animated: true)
    }
    
    private func showSettings() {
        let controller = SettingsVC()
        controller.modalPresentationStyle = .fullScreen
        controller.hidesBottomBarWhenPushed = true
                
        controller.didShowPurchase = { [weak self] in
            self?.presentPurchase()
        }
        controller.didClose = { [weak self] in
            self?.navigationController.popViewController(animated: true)
        }
        controller.didShowTutorial = { [weak self] in
            self?.showTutorialFromSettings()
        }
        controller.didShowPassSettings = {[weak self] in
            self?.showPasscodeSettings()
        }
        
        navigationController.pushViewController(controller, animated: true)
    }
    
    private func showTutorialFromSettings() {
        let controller = TutorialVC()
        controller.didClose = { [weak self] in
            self?.navigationController.popViewController(animated: true)
        }
        navigationController.pushViewController(controller, animated: true)
    }
    
    private func presentPurchase(){
        let controller = PurchaseVC()
        controller.hidesBottomBarWhenPushed = true
        controller.didClose = { [weak self] in
            self?.navigationController.popViewController(animated: true)
        }
        controller.didPurchase = { [weak self] in
            self?.navigationController.popViewController(animated: true)
        }
        navigationController.pushViewController(controller, animated: true)
    }
    
    private func showPasscodeSettings(){
        let controller = PasscodeSettingsViewController()
        controller.modalPresentationStyle = .overFullScreen
        
        settingsNavigation.modalPresentationStyle = .overFullScreen
        settingsNavigation.isNavigationBarHidden = true
        settingsNavigation.pushViewController(controller, animated: false)
        
        controller.didClose = {[weak self] in
            self?.settingsNavigation.dismiss(animated: true)
        }
        controller.didOpenChange = {[weak self] in
            self?.showEnterViewController(isCreatePasscode: true)
        }
        navigationController.present(settingsNavigation, animated: false)
    }
    
    private func showEnterViewController(isCreatePasscode: Bool){
        let controller = EnterViewController()
        controller.isCreatePasscode = isCreatePasscode
        controller.isBackBtnHidden = !isCreatePasscode
        
        controller.didBack = {[weak self] in
            self?.settingsNavigation.popViewController(animated: true)
        }
        
        controller.didClose = {[weak self] in
            self?.settingsNavigation.popViewController(animated: true)
        }
        
        settingsNavigation.pushViewController(controller, animated: true)
    }
}
