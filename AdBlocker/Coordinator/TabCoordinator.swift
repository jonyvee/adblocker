//
//  AppCoordinator.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 18.02.2022.
//

import UIKit


class TabCoordinator: Coordinator{
    
    // MARK: - Properties
    
    private let tabBarController = TabBarController()
    
    var rootViewController: UIViewController {
        return tabBarController
    }
    
    private var navigationController: UINavigationController = {
        let navigation = UINavigationController()
        navigation.isNavigationBarHidden = true
        navigation.modalPresentationStyle = .overFullScreen
        return navigation
    }()
    
    var hasEnterCoordinator: Bool = true
    
    // MARK: - Initialization

    override init() {
        super.init()
        
        let galleryCoordinator = GalleryCoordinator()
        let blockerCoordinator = BlockerCoordinator()
        let passwordsCoordinator = PasswordsCoordinator()

        // Update View Controllers
        tabBarController.viewControllers = [
            galleryCoordinator.rootViewController,
            blockerCoordinator.rootViewController,
            passwordsCoordinator.rootViewController,
        ]

        // Append to Child Coordinators
        childCoordinators.append(galleryCoordinator)
        childCoordinators.append(blockerCoordinator)
        childCoordinators.append(passwordsCoordinator)
    }

    // MARK: - Overrides
    
    override func start() {
        childCoordinators.forEach { (childCoordinator) in
            // Start Child Coordinator
            childCoordinator.start()
        }
        
        
        if User.shared.isFirstOpen!{
            showSplash()
        }else{
            self.rootViewController.present(navigationController, animated: false)
            
            if User.shared.myboard?.subscription != ""{
                if User.shared.isPurchased!{
                    if User.shared.isFinishedOnboard! == false{
                        showTutorial()
                    }else{
                        showEnterViewController(isCreatePasscode: false)
                    }
                }else{
                    showMyBoard()
                }
            }else{
                if User.shared.isFinishedOnboard! == false{
                    showOnboard()
                }else{
                    showEnterViewController(isCreatePasscode: false)
                }
            }
        }
    }
    
    private func showSplash(){
        let controller = SplashVC()
        controller.didShowMyboard = { [weak self] in
            self?.showMyBoard()
        }
        controller.didShowOnboard = { [weak self] in
            self?.showOnboard()
        }
        navigationController.pushViewController(controller, animated: false)
        self.rootViewController.present(navigationController, animated: false)
    }
    
    private func showOnboard() {
        let controller = BoardVC()
        controller.didShowTutorial = { [weak self] in
            self?.showTutorial()
        }
        navigationController.pushViewController(controller, animated: true)
    }
    
    private func showTutorial() {
        let controller = TutorialVC()
        controller.didClose = { [weak self] in
            if User.shared.isPurchased == true{
                self?.showMethodViewController()
            }else{
                self?.showPurchase()
            }
        }
        navigationController.pushViewController(controller, animated: true)
    }
    
    private func showMethodViewController(){
        let controller = MethodViewController()
        controller.didUserPassword = {[weak self] in
            self?.showEnterViewController(isCreatePasscode: true)
        }
        controller.didClose = {[weak self] in
            self?.navigationController.dismiss(animated: true, completion: nil)
        }
        navigationController.pushViewController(controller, animated: true)
    }
    
    private func showEnterViewController(isCreatePasscode: Bool){
        if User.shared.hasPasscode == false && isCreatePasscode == false {
            self.navigationController.dismiss(animated: true)
            return
        }
        
        let controller = EnterViewController()
        controller.isCreatePasscode = isCreatePasscode
        controller.isBackBtnHidden = !isCreatePasscode
        
        controller.didBack = {[weak self] in
            self?.navigationController.popViewController(animated: true)
        }
        
        controller.didClose = {[weak self] in
            self?.navigationController.dismiss(animated: true)
        }
        
        navigationController.pushViewController(controller, animated: true)
    }
    
    private func showPurchase(){
        let controller = PurchaseVC()
        controller.hidesBottomBarWhenPushed = true
        controller.didClose = { [weak self] in
            self?.showMethodViewController()
        }
        controller.didPurchase = { [weak self] in
            self?.showMethodViewController()
        }
        navigationController.pushViewController(controller, animated: true)
    }
    
    
    private func showMyBoard(){
        let controller = MyBoardVC()
        controller.didShowBoard2 = { [weak self] in
            self?.showMyBoard2()
        }
        navigationController.pushViewController(controller, animated: true)
    }
    
    private func showMyBoard2(){
        let controller = MyBoardVC2()
        controller.didShowPurchase = { [weak self] in
//            self?.showPurchase(afterMyboard: true)
        }
        controller.didShowOnboard = { [weak self] in
            self?.showTutorial()
        }
        // did show onboard
        navigationController.pushViewController(controller, animated: true)
    }
    
    
    
}
