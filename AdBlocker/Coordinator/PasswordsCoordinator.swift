//
//  PasswordsCoordinator.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 19.02.2022.
//

import UIKit
import Combine
import SafariServices
import MessageUI

class PasswordsCoordinator: Coordinator{
    
    // MARK: - Properties

    private var navigationController: UINavigationController = {
        let navigation = UINavigationController()
        navigation.isNavigationBarHidden = true
        navigation.tabBarItem.title = ""
        navigation.tabBarItem.image = "passwordP".img()
        navigation.tabBarItem.selectedImage = "passwordA".img()
        return navigation
    }()

    var rootViewController: UIViewController {
        return navigationController
    }

    
    // MARK: - Overrides

    override func start() {
        let controller = PasswordsViewController()
        
        controller.didOpenPassword = { [weak self] (password) in
            self?.openPasswordViewController(data: password)
        }
        
        controller.didOpenPurchase = {[weak self] in
            self?.presentPurchase()
        }
        
        self.navigationController.pushViewController(controller, animated: false)
    }

    
    private func openPasswordViewController(data: PasswordModel?){
        let controller = PasswordViewController(data: data)
        controller.didBack = {[weak self] in
            self?.navigationController.popViewController(animated: true)
        }
        self.navigationController.pushViewController(controller, animated: true)
    }
    
    private func presentPurchase(){
        let controller = PurchaseVC()
        controller.hidesBottomBarWhenPushed = true
        controller.didClose = { [weak self] in
            self?.navigationController.popViewController(animated: true)
        }
        controller.didPurchase = { [weak self] in
            self?.navigationController.popViewController(animated: true)
        }
        navigationController.pushViewController(controller, animated: true)
    }
}
