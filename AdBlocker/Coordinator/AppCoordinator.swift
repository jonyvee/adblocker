////
////  AppCoordinator.swift
////  AdBlocker
////
////  Created by Nikola Cvetkovic on 24.01.2022.
////
//
//import UIKit
//import Foundation
//
//class AppCoordinator {
//    
//    // MARK: - Properties
//    private let navigationController = UINavigationController()
//    
//    // MARK: - Public API
//    var rootViewController: UIViewController {
//        navigationController.isNavigationBarHidden = true
//        return navigationController
//    }
//
//    // MARK: -
//    func start() {
////        if User.shared.isFirstOpen!{
////            showSplash()
////        }else{
////            if User.shared.myboard?.subscription != ""{
////                if User.shared.isPurchased!{
////                    if User.shared.isFinishedOnboard!{
////                        showViewController()
////                    }else{
////                        showOnboard()
////                    }
////                }else{
////                    showMyBoard()
////                }
////            }else{
////                if User.shared.isFinishedOnboard!{
////                    showViewController()
////                }else{
////                    showOnboard()
////                }
////            }
////        }
//    }
//    
//    // MARK: - Helper Methods
//    private func showSplash(){
//        let controller = SplashVC()
//        controller.didShowMyboard = { [weak self] in
//            self?.showMyBoard()
//        }
//        controller.didShowOnboard = { [weak self] in
//            self?.showOnboard()
//        }
//        navigationController.pushViewController(controller, animated: false)
//    }
//    
//    private func showMyBoard(){
//        let controller = MyBoardVC()
//        controller.didShowBoard2 = { [weak self] in
//            self?.showMyBoard2()
//        }
//        navigationController.pushViewController(controller, animated: true)
//    }
//    
//    private func showMyBoard2(){
//        let controller = MyBoardVC2()
//        controller.didShowPurchase = { [weak self] in
//            self?.showPurchase(afterMyboard: true)
//        }
//        controller.didShowOnboard = { [weak self] in
//            self?.showOnboard()
//        }
//        // did show onboard
//        navigationController.pushViewController(controller, animated: true)
//    }
//    
//    private func showOnboard() {
//        let controller = BoardVC()
//        controller.didShowTutorial = { [weak self] in
//            self?.showTutorial()
//        }
//        navigationController.pushViewController(controller, animated: true)
//    }
//
//    private func showTutorial() {
//        let controller = TutorialVC()
//        controller.didClose = { [weak self] in
//            if User.shared.isPurchased == true{
//                self?.showViewController()
//            }else{
//                self?.showPurchase(afterMyboard: false)
//            }
//        }
//        navigationController.pushViewController(controller, animated: true)
//    }
//
//    private func showPurchase(afterMyboard: Bool){
//        let controller = PurchaseVC()
//        controller.didClose = { [weak self] in
//            if afterMyboard{
//                self?.navigationController.popViewController(animated: true)
//            }else{
//                self?.showViewController()
//            }
//        }
//        controller.didPurchase = { [weak self] in
//            if afterMyboard{
//                self?.showOnboard()
//            }else{
//                self?.showViewController()
//            }
//        }
//        navigationController.pushViewController(controller, animated: true)
//    }
//  
//
//}
//
