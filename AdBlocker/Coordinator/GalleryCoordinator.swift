//
//  GalleryCoordinator.swift
//  Vault
//
//  Created by Nikola Cvetkovic on 19.02.2022.
//

import UIKit
import Combine
import Photos
import SafariServices
import MessageUI

class GalleryCoordinator: Coordinator{
    
    // MARK: - Properties
    
    private var navigationController: UINavigationController = {
        let navigation = UINavigationController()
        navigation.isNavigationBarHidden = true
        navigation.tabBarItem.title = ""
        navigation.tabBarItem.image = "galleryP".img()
        navigation.tabBarItem.selectedImage = "galleryA".img()
        return navigation
    }()
    
    var rootViewController: UIViewController {
        return navigationController
    }
    
    // MARK: - Initialization
    
    override init(){
        super.init()
    }
    
    // MARK: - Overrides
    
    override func start() {
        let controller = AlbumsViewController()
        
        controller.didSelect = { [weak self] (model) in
            self?.openPhotoViewController(model: model)
        }
        
        controller.didShowPurchase = {[weak self] in
            self?.presentPurchase()
        }
        
        self.navigationController.pushViewController(controller, animated: false)
    }
    
    private func openPhotoViewController(model: PictureModel){
        let controller = PhotoViewController(model: model)
        controller.hidesBottomBarWhenPushed = true
        
        controller.didBack = {[weak self] in
            self?.navigationController.popViewController(animated: true)
        }
        
        self.navigationController.pushViewController(controller, animated: true)
    }
    
    private func presentPurchase(){
        let controller = PurchaseVC()
        controller.hidesBottomBarWhenPushed = true
        controller.didClose = { [weak self] in
            self?.navigationController.popViewController(animated: true)
        }
        controller.didPurchase = { [weak self] in
            self?.navigationController.popViewController(animated: true)
        }
        navigationController.pushViewController(controller, animated: true)
    }
}
