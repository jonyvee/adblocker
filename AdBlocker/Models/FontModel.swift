//
//  FontModel.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 20.01.2022.
//

import UIKit


enum Font: String{
    case regular = "Montserrat-Regular"
    case medium = "Montserrat-Medium"
    case bold = "Montserrat-Bold"
    case semibold = "Montserrat-SemiBold"
    
    func font(size: CGFloat) -> UIFont{
        return UIFont(name: self.rawValue, size: size)!
    }
}
