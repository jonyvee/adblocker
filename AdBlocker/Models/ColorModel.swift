//
//  ColorModel.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 20.01.2022.
//

import Foundation
import UIKit


enum Color{
    case button
    case title
    case subtitle
    case green1
    case green2
    case littleTitle
    case red1
    case red2
    case mainGreen2
    case lightGreen1
    case lightGreen2
        
    var ui: UIColor{
        switch self {
        case .button:
            return UIColor(red: 0, green: 0.642, blue: 0.412, alpha: 1)
        case .title:
            return UIColor(red: 0.165, green: 0.188, blue: 0.22, alpha: 1)
        case .subtitle:
            return UIColor(red: 0.518, green: 0.549, blue: 0.604, alpha: 1)
        case .green1:
            return UIColor(red: 0.024, green: 0.675, blue: 0.443, alpha: 1)
        case .green2:
            return UIColor(red: 0.118, green: 0.839, blue: 0.58, alpha: 1)
        case .littleTitle:
            return UIColor(red: 0.102, green: 0.114, blue: 0.122, alpha: 1)
        case .red1:
            return UIColor(red: 1, green: 0.337, blue: 0.337, alpha: 1)
        case .red2:
            return UIColor(red: 0.792, green: 0.228, blue: 0.228, alpha: 1)
        case .mainGreen2:
            return UIColor(red: 0, green: 0.642, blue: 0.412, alpha: 1)
        case .lightGreen1:
            return UIColor(red: 0.888, green: 0.992, blue: 0.967, alpha: 1)
        case .lightGreen2:
            return UIColor(red: 0.597, green: 0.925, blue: 0.807, alpha: 1)
        }
    }
}
