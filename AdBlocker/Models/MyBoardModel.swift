//
//  MyBoardModel.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 25.01.2022.
//

import Foundation


struct MyBoardModel: Codable{
    var subscription: String
    var title: String
    var subtitle: String
    var title2: String
    var banner1: String
    var banner2: String
    var banner3: String
    var button: String
    var duration: Int
    var showPaywall: Bool
    
    init(_ data: [AnyHashable : Any] = [:]){
        self.subscription = (data["subscription"] as? String ?? "").normalize()
        self.title = (data["title"] as? String ?? "").normalize()
        self.subtitle = (data["subtitle"] as? String ?? "").normalize()
        self.title2 = (data["title2"] as? String ?? "").normalize()
        self.banner1 = (data["banner1"] as? String ?? "").normalize()
        self.banner2 = (data["banner2"] as? String ?? "").normalize()
        self.banner3 = (data["banner3"] as? String ?? "").normalize()
        self.button = (data["button"] as? String ?? "").normalize()
        let showPaywall = data["showPaywall"] as? String ?? ""
        self.showPaywall = showPaywall == "true"
        let duration = (data["banner3"] as? String ?? "5")
        self.duration = Int(duration) ?? 5
    }
}
