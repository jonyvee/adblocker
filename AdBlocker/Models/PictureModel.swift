//
//  PictureModel.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 26.03.2022.
//


import UIKit
import Realm
import RealmSwift


class PictureModel: Object, Codable{
    @objc dynamic var id: String = NSUUID().uuidString
    @objc dynamic var isFavorite: Bool = false
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
