//
//  PasswordsModel.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 01.04.2022.
//

import UIKit
import Realm

class PasswordModel: RealmSwiftObject, Codable{
    @objc dynamic var id: String = NSUUID().uuidString
    @objc dynamic var title: String = ""
    @objc dynamic var login: String = ""
    @objc dynamic var url: String = ""
    @objc dynamic var password: String = ""
    @objc dynamic var isFavorite: Bool = false
    @objc dynamic var isPassHidden: Bool = true
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
