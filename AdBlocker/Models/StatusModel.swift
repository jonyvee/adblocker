//
//  StatusModel.swift
//  AdBlocker
//
//  Created by Nikola Cvetkovic on 25.01.2022.
//

import Foundation


struct Status: Codable{
    var ad: Bool = false
    var adult: Bool = false
    var script: Bool = false
    var malvare: Bool = false
    var cookies: Bool = false
}
