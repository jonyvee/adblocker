//
//  ContentBlockerRequestHandler.swift
//  MalvareContent
//
//  Created by Nikola Cvetkovic on 26.01.2022.
//

import UIKit
import MobileCoreServices

class ContentBlockerRequestHandler: NSObject, NSExtensionRequestHandling {

    func beginRequest(with context: NSExtensionContext) {
        let documentFolder = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.jonyvee.blocker")
        guard let jsonURL = documentFolder?.appendingPathComponent("Malvare.json") else {print("Error jonyvee");return}
        guard let attachment = NSItemProvider(contentsOf: jsonURL) else {print("Error jonyvee");return}
        let item = NSExtensionItem()
        item.attachments = [attachment]
        context.completeRequest(returningItems: [item], completionHandler: nil)
    }
    
}
